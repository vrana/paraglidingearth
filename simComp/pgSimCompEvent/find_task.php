<?php
include "../connexion.php";
session_start();

// TIME CONVERTER
function secondsToHoursMinutesSeconds($time){
if ($time < 86400 AND $time>=3600)  // si le nombre de secondes ne contient pas de jours mais contient des heures
{  // on refait la m�me op�ration sans calculer les jours
$heure = floor($time/3600);
$reste = $time%3600;
$minute = floor($reste/60);
$seconde = $reste%60;
$result = $heure.'h.'.$minute.'min.'.$seconde.'s.';
}
elseif ($time<3600 AND $time>=60)
{  // si le nombre de secondes ne contient pas d'heures mais contient des minutes
$minute = floor($time/60);
$seconde = $time%60;
$result = $minute.'min.'.$seconde.'s.';
}
elseif ($time < 60)
// si le nombre de secondes ne contient aucune minutes
{ $result = $time.'s.'; }
return $result;
}
//ENDOF TIME CONVERTER

$queryByCountries = "select distinct id_pays, nom_pays_en, iso from pays
left join site on site.pays=id_pays
left join pgsim_comp_tasks on pgsim_comp_tasks.site_id = id_site
where pgsim_comp_tasks.id order by continent, nom_pays_en";
$resultByCountries = mysql_query($queryByCountries);
$queryTasks = "select count(id) as numTasks from pgsim_comp_tasks";
$resultTasks = mysql_query($queryTasks);
$valTasks = mysql_fetch_array($resultTasks);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<link rel=stylesheet href='../pgearth/style/style.css' type='text/css' />

<script type="text/javascript" src="../scripts/overlib.js"></script>
<script type="text/javascript" src="../scripts/sorttable.js"></script>

<script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAAloePAlEicH6IDTkMBLcJrhRnw2erUWlj7HEtSeImHxR-M2N57BQP1GQhxXeEjH8bjapkkuVaZ8Y7Ng" type="text/javascript"></script>

<title>Welcome to Comp module !</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
</head>
<body>

<h1>Sortable tasks list</h1>

<?php
$here = "tasks_lists"; 
include "tabs_header.php";
?>
<em>Click the first line to sort the table</em>
	<table class="sortable">
		<thead><tr align="center">
			<th>Task name</th>
			<th>Country</th>
			<th>Length (in km)</th>
			<th>Flights<br />made</th>
			<th>Completed</th>
			<th>Completed %<br />(percent)</th>
			<th>Interest</th>
			<th>Status</th>
			<th>Author</th>
		</thead></tr>
		<tbody>
<?php
$resultTasks = mysql_query("select * from pgsim_comp_tasks");
while ($vTasks=mysql_fetch_array($resultTasks)){
		
		if ($vTasks['task_name']=="") $taskName = "no name...";
		else $taskName = $vTasks['task_name'];
		
		$qSite = "select nom, id_site, iso from site left join pays on site.pays=id_pays where id_site = ".$vTasks['site_id'];
		$vSite = mysql_fetch_array(mysql_query($qSite));
		
		$qAuthor = "select pseudo from auteur where id_auteur = ".$vTasks['organised_by'];
		$vAuthor = mysql_fetch_array(mysql_query($qAuthor));
		
		$qResults = "select * from pgsim_comp_results where task_id = ".$vTasks['id'];
		$vResults = mysql_fetch_array(mysql_query($qResults));
		$nResults = mysql_num_rows(mysql_query($qResults));
		$qCompleted = "select * from pgsim_comp_results where task_id = ".$vTasks['id']." and distance = 999999 order by flight_duration asc";
		$vCompleted = mysql_fetch_array(mysql_query($qCompleted));
		$nCompleted = mysql_num_rows(mysql_query($qCompleted));
		
		$rInterest = mysql_query("SELECT count(id) as count, sum(rating_num) as total  FROM `pgsim_comp_tasks_ratings` WHERE `rating_id` = ".$vTasks['id']." Group BY rating_id");
		$vInterest = mysql_fetch_array($rInterest);
?>
	<tr>
		<td align="center"><a title="Site : <?php echo stripslashes($vSite['nom']);?>" href="start_task.php?task_id=<?php echo $vTasks['id'];?>"><?php echo $taskName;?></a></td>
		<td align="center"><?php echo  $vSite['iso'] ?></td>
		<td align="center" sorttable_customkey="<?php echo  $vTasks['task_length']?>"><?php echo  round($vTasks['task_length']/1000) ?> km</td>
		<td align="center"><?php echo  $nResults ?></td>
		<td align="center"><?php echo  $nCompleted ?></td>
		<td align="center"><?php echo  round(100*$nCompleted/$nResults) ?></td>
		<td align="center" sorttable_customkey="<?php if ($vInterest['count']>0) {echo $vInterest['total']/$vInterest['count'];} else echo "0";?>"><?php if ($vInterest['count']>0) {$interest=round(10*$vInterest['total']/$vInterest['count'])/10; echo $interest."/5 (".$vInterest['count']." votes)";} else echo "0 (no votes)";?></td>
		<td align="center" style="color:white"><?php if ($vTasks['testing']) {$flag ="orange"; $title="task in testing mode"; $sortText="0";} else {$flag="green"; $title="task in definitive version"; $sortText="1";}  ?>
<img src="../pgearth/images/famfamfamicons/flag_<?php echo $flag;?>.png" title="<?php echo $title;?>" /><?php echo $sortText;?></td>		
		<td align="center"><?php echo  $vAuthor['pseudo'] ?></td>
	</tr>
<?php
	}
?>
		</tboby>
	</table>

</body>

<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-1083947-1";
urchinTracker();
olLoaded=1;
</script>

</html>
