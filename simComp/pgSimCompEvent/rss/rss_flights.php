<?php
include"../../connexion.php";
header("Content-Type: text/xml");

function secondsToHoursMinutesSeconds($time){
if ($time < 86400 AND $time>=3600)
// si le nombre de secondes ne contient pas de jours mais contient des heures
{
// on refait la même opération sans calculer les jours
$heure = floor($time/3600);
$reste = $time%3600;
$minute = floor($reste/60);
$seconde = $reste%60;
$result = $heure.'h.'.$minute.'min.'.$seconde.'s.';
}
elseif ($time<3600 AND $time>=60)
{
// si le nombre de secondes ne contient pas d'heures mais contient des minutes
$minute = floor($time/60);
$seconde = $time%60;
$result = $minute.'min.'.$seconde.'s.';
}
elseif ($time < 60)
// si le nombre de secondes ne contient aucune minutes
{
$result = $time.'s.';
}
return $result;
}



echo "
<rss version=\"2.0\"  xmlns:atom=\"http://www.w3.org/2005/Atom\">
    <channel>
   
        <title>PGE PgSim Comp : latest flights</title>
        <link>http://www.paraglidingearth.com/pgSimComp/</link>
        <description>Latest flights made on the online competion on PgE</description>
	<language>en</language>";

$result = mysql_query("select * from pgsim_comp_results left join pgsim_comp_tasks on task_id = pgsim_comp_tasks.id order by pgsim_comp_results.date desc limit 10");

while ($val=mysql_fetch_array($result)){

$pubdate = $val['date'];
$val_pseudo = mysql_fetch_array(mysql_query("select pseudo from auteur where id_auteur = ".$val['pilot_id'].";"));

$val_country= mysql_fetch_array(mysql_query("select iso from site left join pays on site.pays=id_pays where id_site = ".$val['site_id']));

if ($val['distance']==999999) $distance = round($val['task_length']/1000) ." km (completed task)";
else $distance = round($val['distance']/1000) ." km";
$time = secondsToHoursMinutesSeconds($val['flight_duration']);

echo "
        <item>
            <title>".$val_pseudo['pseudo']." : $distance and ".$val['score']." points on \"".$val['task_name']."\" (".$val_country['iso'].")</title>
            <link>http://www.paraglidingearth.com/pgSimComp/start_task.php?task_id=".$val['task_id']."</link>
            <description>".$val_pseudo['pseudo']." flew ".$distance." in $time, that brought him ".$val['score']." points</description>
	    <pubDate>".$pubdate."</pubDate>
        </item>";
}       

echo "
    <atom:link href=\"http://www.paraglidingearth.com/pgSimComp/rss/rss_flights.php\" rel=\"self\" type=\"application/rss+xml\" />
    </channel>
</rss>";


?>
