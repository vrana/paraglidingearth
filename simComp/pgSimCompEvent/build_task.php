<?php
session_start();
include "../connexion.php";
include "../pgearth/secure.php";

$site_id = $_GET['site_id'];
$member_id = $_SESSION['id_membre'];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

<link
    href="external_js/carpe_slider/default.css"
    rel="stylesheet"
    type="text/css" />

<script
    language="JavaScript"
    src="external_js/carpe_slider/slider.js" >
</script>

<script type="text/javascript" src="calendar_form.js"></script>

<script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAAloePAlEicH6IDTkMBLcJrhQ5O_AQOV-t-OYn1cRAcVTwH8AssxSaNjYyjfTXYy8UNvlgWybY6A3fug" type="text/javascript"></script>
<script type="text/javascript">
var marker= new Array();
var lastActiveWaypoint = 1 ;
var taskLength = 0;

function addWaypoint(){
	var waypointToAdd = lastActiveWaypoint + 1;
	if (lastActiveWaypoint == 6) {
		alert('not more than 6 waypoints per task !');
	} else {
		document.getElementById('waypointForm'+waypointToAdd).style.visibility="visible";
		marker[waypointToAdd].show();
		lastActiveWaypoint++;
		document.getElementById('last_active_waypoint').value = lastActiveWaypoint;
		dragendUpdate();
	}
}

function removeWaypoint(){
	if (lastActiveWaypoint == 1) {
		alert('You need at least one waypoint on the task !');
	} else {
		document.getElementById('waypointForm'+lastActiveWaypoint).style.visibility="hidden";
		marker[lastActiveWaypoint].hide();
		document.getElementById('distance_b'+lastActiveWaypoint).value = 0;
		lastActiveWaypoint--;
		document.getElementById('last_active_waypoint').value = lastActiveWaypoint;
		dragendUpdate() ;
	}
}

function dragendUpdate() {
	taskLength = 0 ;
	for (p=1; p <= lastActiveWaypoint; p++) {
		var pPlusOne = p+1;
		var legLength = Math.round(marker[p-1].getLatLng().distanceFrom(marker[p].getLatLng()));
		document.getElementById('distance_b'+p).value = legLength;
		document.getElementById('dist_b'+p).innerHTML = legLength / 1000;
		taskLength = taskLength + legLength;
		document.getElementById('lng_b'+p).value = marker[p].getLatLng().lng() ;
		document.getElementById('lat_b'+p).value = marker[p].getLatLng().lat() ;
	}
	document.getElementById('taskLength').innerHTML = '<b>'+ taskLength/1000 +' km</b>';
	document.getElementById('task_length').value = taskLength;
}

</script>
<link rel=stylesheet href='../pgearth/style/style.css' type='text/css' />
<title>Comp !</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta name="generator" content="HAPedit 3.1">
</head>

<?php
$querySite = "select latd, longd, nom from site where id_site = ".$site_id;
$resultSite= mysql_query($querySite);
$numSite = mysql_num_rows($resultSite);
if ($numSite ==1) {
$valSite = mysql_fetch_array($resultSite);

$query12 = "select count(id) as tasks_count from pgsim_comp_tasks where site_id = ".$site_id;
$result12= mysql_query($query12);
$val12 = mysql_fetch_array($result12);
$task_num = $val12['tasks_count']+1 ;
?>
<body  onunload="GUnload()">

  <h1>Build a new task from <?php echo stripslashes($valSite['nom']);?> in 3 steps</h1>

<?php
$here = "build_task"; 
include "tabs_header.php";
?>

  <form name="build_task" method="post" action="save_new_task.php">

<input type="hidden" name="event_id" value="<?php echo $_GET['event_id']; ?>" />

<div class="rubriqueSite">
  <div class="titreMenu">1 - Task description in a few words</div>
	<p>Task name : <input name="task_name" value="<?php echo stripslashes($valSite['nom'])." - task #".$task_num;?>" /></p>
	<p>Task description :
	<textarea name="task_description"></textarea></p>
	<p>Open this tak in :<br/>
	<input type="radio" name="test_mode" value="1" checked /> Test mode : you will be able to edit and modify the task, but every change on it will delete the saved results for the previous version. Use this if you want to test the parameters (turnpoints and weather).<br />
	<input type="radio" name="test_mode" value="0" /> Definitive version : no further changes can be done to the task, and results will remain definitive.
	</p>
</div>

<div class="rubriqueSite">
	<p>Opening date of the task :<br/>
<input id="starts" name="starts" type="text" size="25" />
<a href="javascript:NewCssCal('starts','yyyymmdd','dropdown',true,24,false)">
<img src="images/cal.gif" width="16" height="16" alt="Pick a date" border="0" /></a>
<br />
        Ending date of the task :<br/>
<input id="expires" name="expires" type="text" size="25" />
<a href="javascript:NewCssCal('expires','yyyymmdd','dropdown',true,24,false)">
<img src="images/cal.gif" width="16" height="16" alt="Pick a date" border="0" /></a>
	</p>
</div>

<div class="rubriqueSite">
  <div class="titreMenu">2 - Set the task turnpoints</div>
  <table>
    <tr>
	  <td>
	    <div id='map' style='width: 570px; height: 420px; ' ></div>
	  </td>
	  <td valign="top">
	  <div>
		<span><b>Task length : </b></span> <span id="taskLength"><b>0 km</b></span>
	  </div>
	  <hr />
	  Drag the waypoint icons on the map to build the task.<br />
	  <a href="javascript:void(0)" onclick="addWaypoint();"><img src="../pgearth/images/famfamfamicons/add.png" border="0" /> Add a waypoint</a> (it will appear at the takeoff location on the map)<br />
	  <a href="javascript:void(0)" onclick="removeWaypoint();"><img src="../pgearth/images/famfamfamicons/delete.png" border="0" /> Remove last waypoint</a><hr />
<?php
   for ($i=1; $i<7; $i++){ 
?>
	   <div id="waypointForm<?php echo $i?>">
	   <span>Waypoint&nbsp;<?php echo $i; ?> - leg length : </span><span id="dist_b<?php echo $i?>">0 km</span>
	   <input name="distance_b<?php echo $i?>" id="distance_b<?php echo $i?>" type="hidden" />
  	   <input type="hidden" name="lat_b<?php echo $i?>" id="lat_b<?php echo $i?>" value="0" />
  	   <input type="hidden" name="lng_b<?php echo $i?>" id="lng_b<?php echo $i?>" value="0" />
	   <hr />
	   </div>
<?php
    }
?>
	  </td>
	</tr>
     </table>
</div>

<div class="rubriqueSite">
  <div class="titreMenu">3 - Set the weather conditions for the task</div>
	  
<table>
<tr>
  <td colspan="3">
    <b>Wind : </b>
  </td>
</tr>
 <tr>
  <td align="right">
    Wind direction (coming from) :
  </td>
  <td>
	<table style="background-position: center center; background-repeat: no-repeat;" background="../pgearth/rose/compass.png">
	<tbody><tr>
	<td valign="bottom" width="35" align="right" height="35"><input name="wind_direction" value="315" type="radio"></td>

        <td valign="top" align="center"><input name="wind_direction" value="0" type="radio"></td>
	<td valign="bottom" width="35" align="left"><input name="wind_direction" value="45" type="radio"></td>
	</tr>
	<tr>
	<td align="left"><input name="wind_direction" value="270" type="radio"></td>
        <td width="30" align="center" height="30">&nbsp;</td>
	<td align="right"> <input name="wind_direction" value="90" type="radio"></td>
	</tr>

	<tr>
	<td valign="top" align="right" height="35"><input name="wind_direction" value="225" type="radio"></td>
        <td valign="bottom" align="center"><input name="wind_direction" value="180" type="radio"></td>
	<td valign="top" align="left"><input name="wind_direction" value="135" type="radio"></td>
	</tr>
	</tbody></table>
    </td>
    <td>   &nbsp;
    </td>
  </tr>
<tr>
 <td align="right">
  Wind speed :
 </td>
 <td>
<div class="carpe_horizontal_slider_display_combo">
  <div class="carpe_horizontal_slider_track"  style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <div class="carpe_slider_slit" style="background-color: #300; border-color: #b99 #fdd #fdd #b99;">&nbsp;</div>
    <!-- Default position: 80px -->
    <div
      class="carpe_slider"
      id="sliderWindSpeed"
      display="displayWindSpeed"
      style="left: 40px; background-color: #369; border-color: #69c #036 #036 #69c;">&nbsp;</div>
  </div>
  <div class="carpe_slider_display_holder" style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <!-- Default value: 180 -->
    <input
      class="carpe_slider_display"
      name="wind_speed"
      id="displayWindSpeed"
      style="background-color: #fff; color: #258;"
      type="text"
      from="0"
      to="10"
      value="4" />
  </div>
</div>
  </td>
  <td>  &nbsp;meters per second
  </td>
 </tr>
<tr>
  <td colspan="3">
    <b>Thermals : </b>
  </td>
</tr>
        <tr>
          <td align="right"> Thermals density :</td><td>

<div class="carpe_horizontal_slider_display_combo">
  <div class="carpe_horizontal_slider_track"  style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <div class="carpe_slider_slit" style="background-color: #300; border-color: #b99 #fdd #fdd #b99;">&nbsp;</div>
    <!-- Default position: 80px -->
    <div
      class="carpe_slider"
      id="sliderThermalsDensity"
      display="displayThermalsDensity"
      style="left: 50px; background-color: #369; border-color: #69c #036 #036 #69c;">&nbsp;</div>
  </div>
  <div class="carpe_slider_display_holder" style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <!-- Default value: 180 -->
    <input
      class="carpe_slider_display"
      name="thermal_density" 
      id="displayThermalsDensity"
      style="background-color: #fff; color: #258;"
      type="text"
      from="0"
      to="4"
      value="2" />
  </div>
</div>
          </td>
          <td>  &nbsp;thermals&nbsp;per&nbsp;km&nbsp;square
          </td>
        </tr>
        <tr>
          <td align="right">
            Thermals max height : </td>
          <td>

<div class="carpe_horizontal_slider_display_combo">
  <div class="carpe_horizontal_slider_track"  style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <div class="carpe_slider_slit" style="background-color: #300; border-color: #b99 #fdd #fdd #b99;">&nbsp;</div>
    <!-- Default position: 80px -->
    <div
      class="carpe_slider"
      id="sliderThermalsHeight"
      display="displayThermalsHeight"
      style="left: 50px; background-color: #369; border-color: #69c #036 #036 #69c;">&nbsp;</div>
  </div>
  <div class="carpe_slider_display_holder" style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <!-- Default value: 180 -->
    <input
      class="carpe_slider_display"
      name="thermal_height" 
      id="displayThermalsHeight"
      style="background-color: #fff; color: #258;"
      type="text"
      from="0"
      to="4200"
      value="2100" />
  </div>
</div>

          </td>
          <td>&nbsp;meters&nbsp;ASL
          </td>
        </tr>
        <tr>
          <td align="right">
            Thermals average max uplift speed :  </td>
            <td>

<div class="carpe_horizontal_slider_display_combo">
  <div class="carpe_horizontal_slider_track"  style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <div class="carpe_slider_slit" style="background-color: #300; border-color: #b99 #fdd #fdd #b99;">&nbsp;</div>
    <!-- Default position: 80px -->
    <div
      class="carpe_slider"
      id="sliderThermalsSpeed"
      display="displayThermalsSpeed"
      style="left: 50px; background-color: #369; border-color: #69c #036 #036 #69c;">&nbsp;</div>
  </div>
  <div class="carpe_slider_display_holder" style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <!-- Default value: 180 -->
    <input
      class="carpe_slider_display"
      name="thermal_speed"
      id="displayThermalsSpeed"
      style="background-color: #fff; color: #258;"
      type="text"
      from="2"
      to="10"
      value="6" />
  </div>
</div>

          </td>
          <td> meters per second
          </td>
        </tr>
        <tr>
          <td align="right">
            Thermals size :</td><td>
            <input type="radio" name="thermal_size" id="thermal_size_small" value="40"><label for="thermal_size_small"> small </label>
            <br />
            <input type="radio" name="thermal_size" id="thermal_size_medium" value="60"  checked><label for="thermal_size_medium"> medium </label>
            <br />
            <input type="radio" name="thermal_size" id="thermal_size_big" value="80"><label for="thermal_size_big"> large </label>
          </td>
        </tr>
      </table>
</div>

  <input type="hidden" name="site_id" value="<?php echo $site_id;?>" />
  <input type="hidden" name="member_id" value="<?php echo $member_id;?>" />
  <input type="hidden" name="task_length" id="task_length" value="0" />
  <input type="hidden" name="last_active_waypoint" id="last_active_waypoint" value="1" />

<div class="rubriqueSite">
<div class="titreMenu">4 - (ok, we said three steps, but...) Check the information above and validate</div>
  <p>Please double check all the information above, since no modifications are possible after that</p>
  <p>&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" /></p>
  &nbsp;
  </form>
</div>

</body>
<script type="text/javascript">
// hide the 5 last waypoints at start
for (i=2; i<7; i++){
	document.getElementById('waypointForm'+i).style.visibility="hidden";
}

//   ---- Mouse wheel zoom
function GoogleMapWheelZoom(a) {
    if(a.cancelable)    a.preventDefault();
    if ((a.detail || -a.wheelDelta) < 0) map.zoomIn();
    else map.zoomOut();        
    return false; 
}
//   ---- end of mouse wheel zoom

// Display the map, with some controls and set the initial location 
      var mapobj = document.getElementById("map");
      var map = new GMap2(mapobj);
      map.addControl(new GSmallMapControl());
 //     map.addControl(new GMapTypeControl());
	  map.addMapType(G_SATELLITE_3D_MAP);
      map.addMapType(G_PHYSICAL_MAP);
      map.addControl(new GHierarchicalMapTypeControl());
      map.setMapType(G_PHYSICAL_MAP);
      map.setCenter(new GLatLng(<?php echo $valSite['latd'].",".$valSite['longd']; ?>), 12, G_NORMAL_MAP);

	  map.enableDoubleClickZoom();
	  GEvent.addDomListener(mapobj, "DOMMouseScroll", GoogleMapWheelZoom);
	  GEvent.addDomListener(mapobj, "mousewheel", GoogleMapWheelZoom);

// Display markers
        var baseIcon = new GIcon(G_DEFAULT_ICON);
        baseIcon.shadow = "http://www.google.com/mapfiles/shadow50.png";
        baseIcon.iconSize = new GSize(20, 34);
        baseIcon.shadowSize = new GSize(37, 34);
        baseIcon.iconAnchor = new GPoint(9, 34);
        baseIcon.infoWindowAnchor = new GPoint(9, 2);
		
        function createMarker(point, number) {
          // Create a lettered icon for this point using our icon class
          var numberedIcon = new GIcon(baseIcon);
          numberedIcon.image = "img/markers/number" + number + ".png";
          if (number==0) markerOptions = { icon:numberedIcon };
		  else markerOptions = { icon:numberedIcon, draggable: true };
		 
          var marker = new GMarker(point, markerOptions);
		 
          GEvent.addListener(marker, "click", function() {
            if (number==0) marker.openInfoWindowHtml("Waypoint <b>" + number + " : Start  point</b>");
			else marker.openInfoWindowHtml("Waypoint <b>" + number + "</b>");
          });
		  
		  if (number>0){
			GEvent.addListener(marker, "dragstart", function() {
				map.closeInfoWindow();
			});
		 
			GEvent.addListener(marker, "dragend", function() {
				dragendUpdate();
			});
		  }
		return marker;
        }

	marker[0] = createMarker(new GLatLng(<?php echo $valSite['latd'].", ".$valSite['longd']?>), 0) ;
        map.addOverlay(marker[0]);
<?php
   for ($i=6; $i>0; $i--){ 
           $j=$i-1;    
	   echo "
       marker[".$i."] = createMarker(new GLatLng(".$valSite['latd'].", ".$valSite['longd']."), ".$i.");
       map.addOverlay(marker[".$i."]);";
   }
  for ($i=6; $i>0; $i--){ 
           $j=$i-1;    
           if ($i>1) echo "
		   marker[$i].hide();";
    }
?>

</script>
<?php
} else {
?>
<body>
You need to specify a valid id for the site you want your task to start from.<br />
<a href="build_task_how_to.php">Please see here</a>
</body>	
<?php
}
?>
</html>