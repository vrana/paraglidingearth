<?php
session_start();

include "../connexion.php";

$task_id = $_GET['task_id'];
$queryTask = "select * from pgsim_comp_tasks where id = $task_id";
//echo $queryTask;
$resultTask = mysql_query($queryTask);
$numTask = mysql_num_rows($resultTask);
if ($numTask<>1){
   echo "There seems to be a problem with the task you are trying to access : the task does not seem to be present in our database... :(";
} else {

$valTask = mysql_fetch_array($resultTask);

$glider = "4e01a5a1f374d9d3a426bad1ca64772";
if (isset($_POST['glider'])) $glider = $_POST['glider'];

$pseudo = "Anonymous";  $id_pilot = 0;
if (isset ($val['id_membre'])){
    $auteur = mysql_fetch_array(mysql_query("select id_auteur, pseudo from auteur where id_auteur=".$_SESSION['id_membre'].";"));
    $pseudo = $auteur['pseudo']; $id_pilot = $auteur['id_auteur'];
}
if (isset ($valTask['site_id'])){
  $result = mysql_query("select latd, longd, nom, winch from site where id_site=".$valTask['site_id'].";");
  $val = mysql_fetch_array($result);
    $site_name = $val['nom'];
    $site_lat = $val['latd'];
    $site_lng = $val['longd'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title>PG sim competion task in progress... ;)</title>
  <link rel=stylesheet href='../pgearth/style/style.css' type='text/css' />

<?php
  if (substr_count(strtolower($_SERVER['SERVER_NAME']), 'paraglidingearth.com') > 0) $googlemapskey="ABQIAAAAloePAlEicH6IDTkMBLcJrhQXwnL9FxgcTH4vf2kk7gAE15haUxSJ7gKW1yxt4vCGL_lEpmkZonN36w";
  if (substr_count(strtolower($_SERVER['SERVER_NAME']), 'thenet.gr') > 0) $googlemapskey="ABQIAAAAloePAlEicH6IDTkMBLcJrhTXr2IV_XJIZ-YXf_AXZXGLuo8U8xS848FWl_V9yyPvaFAkTEaCpDp1Qw";  
?>
  <script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAAloePAlEicH6IDTkMBLcJrhTS12zr6RxXI0CRobmUzd_COOhmZBS24fuq3aTp-9cry77ZmapyoiPvYA" type="text/javascript"></script>
  <script src="http://www.google.com/jsapi?key=ABQIAAAAloePAlEicH6IDTkMBLcJrhTS12zr6RxXI0CRobmUzd_COOhmZBS24fuq3aTp-9cry77ZmapyoiPvYA" type="text/javascript"></script>

  <script type="text/javascript">
// FLIGHT CONDITIONS INITIALISED HERE FOR THE WHOLE APP AS 'GLOBAL' VARIABLES //

var PILOT_NAME = '<?php echo $pseudo; ?>';
var SITE_NAME = '<?php echo $site_name; ?>';

var TAKEOFF_LAT = <?php echo $site_lat; ?>;
var TAKEOFF_LNG = <?php echo $site_lng; ?>;
var TAKEOFF_HEADING = <?php echo $valTask['wind_direction']; ?>;
var WINCH_ALTITUDE = 0;

var WIND_SPEED = <?php echo $valTask['wind_speed']; ?>;
var WIND_DIRECTION = <?php echo $valTask['wind_direction']; ?>;
 
var THERMAL_VZ_MAX = <?php echo $valTask['thermals_speed']; ?>;
var THERMAL_TOP = <?php echo $valTask['thermals_height']; ?>;
var THERMAL_DENSITY = <?php echo $valTask['thermals_density']; ?>;
var THERMAL_RADIUS = <?php echo $valTask['thermals_width']; ?>;

var temperatureInit = 21;

var GLIDER = '4e01a5a1f374d9d3a426bad1ca64772';
  
// LETS PUT THE TASK WAYPOINTS IN AN ARRAY
var WAYPOINT = new Array();
var LEG_LENGTH = new Array();
<?php
   $iJs = 1;
   for ($i=1; $i<7; $i++){
       if ($valTask['lat_b'.$i] and $valTask['lng_b'.$i]){
           echo "WAYPOINT[$iJs]=[".$valTask['lat_b'.$iJs].", ".$valTask['lng_b'.$iJs]."];\n";
           echo "LEG_LENGTH[$iJs] =".$valTask['distance_b'.$iJs].";\n";
           $iJs++;
       }
   }
?>
var LAST_WAYPOINT = <?php echo $iJs;?>;
var WAYPOINT_RADIUS = 400;

google.load("earth", "1");
google.load("maps", "2.99");

var ge = null;
var geocoder;
var truck;

function el(e) { return document.getElementById(e); }

function Sample(description, url) {
  this.description = description;
  this.url = url;
  return this;
}

var samples = [];

function init() {
  geocoder = new GClientGeocoder();

  var map2d = new GMap2(document.getElementById("map2d"));
  map2d.setCenter(new GLatLng(<?php echo $site_lat;?>, <?php echo $site_lng;?>), 13);

  init3D();

}

function initCallback(object) {
  ge = object;
  ge.getWindow().setVisibility(true);
  ge.getLayerRoot().enableLayerById(ge.LAYER_BUILDINGS, true);
  ge.getOptions().setFlyToSpeed(ge.SPEED_TELEPORT);

  truck = new Truck();
}

function failureCallback(err) {
  /***
   * This function will be called if plugin fails to load, in case
   * you need to handle that error condition.
   ***/
}

function init3D() {
  google.earth.createInstance("map3d", initCallback, failureCallback);
}

function submitLocation() {
  doGeocode(el('address').value);
}

function doGeocode(address) {
  geocoder.getLatLng(address, function(point) {
    if (point) {
      if (ge != null && truck != null) {
        truck.teleportTo(point.y, point.x);
      }
    }
  });
}

  </script>

  <script type="text/javascript" src="math3d.js"></script>
  <script type="text/javascript" src="paraglideComp.js"></script>
  <script type="text/javascript" src="sound/script/soundmanager2.js"></script>
  <script type="text/javascript">
// VARIO SOUND
soundManager.url = 'sound/soundmanager2.swf'; // override default SWF url
soundManager.debugMode = false;
soundManager.consoleOnly = false;
soundManager.onload = function() {
  soundManager.createSound({
    id: 'beep',
    url: 'sound/beep.mp3',
    autoLoad: true
  });  
}
// VARIO SOUND
  </script>

</head>
<body onload='init();' onunload="GUnload()"; onKeyDown="return keyDown(event);" onKeyUp="return keyUp(event);" >
<center>
<table>
 <tr>
  <td valign="top">
    <div id="GPS" style="background:url('img/gpsBackground.jpg'); width:276px; height:520px">
       <div id='GPSscreen' style="width: 170px; height: 220px; border: solid 1px gray; position:relative; left:52px; top:136px">
         <div id='mapGPS' style='width: 170px; height: 220px; ' >
         </div>
         <div style='position:relative; bottom:118px; width:170px; height:16px'><center><img src="img/arrow_down.png" name="headingImg" width="16" height="16" /></center>
     </div>
         <div id='GPSinfo' style='position:relative; bottom:80px'>
         </div>
       </div>
    </div>
    <div id='waypoint'>waypoints info here
    </div>
  </td>
  <td valign="top">
  <div id='map3d_container' style='width: 762px; height: 467px;border: solid 1px black'>
    <div id='map3d' style='width: 760px; height: 465px;border: solid 1px gray'></div>
    <div id='endFlight'></div>
    <div id='igc' style='display:none'>&nbsp;<br />
      <form name="igcForm" action="save_task.php" method="post">
        <textarea name='igcFile' style='width: 650px; height: 180px; display:none'></textarea>
        <input name='member_id' type="hidden" value="<?php echo $_SESSION['id_membre'];?>" />
        <input name='task_id' type="hidden" value="<?php echo $task_id;?>" />
        <input name='task_last_waypoint' type="hidden" value="0" />
        <input name='last_waypoint_reached' type="hidden" value="0" />
        <input name='distance_to_next_waypoint' type="hidden" value="999999" />
        <input name='flight_duration' type="hidden" value="0" />
        <input name='flight_distance' type="hidden" value="0" />
        <input type="submit" value="Save your result !" />
      </form>
    </div>

  </div>
    <center>
      <div id="takeOffButton">
        &nbsp;<br />
        <input type=button class="button medw" onmousedown="leftButtonDown=true;" onmouseup="leftButtonDown=false;" value="left">
        <input type=button class="button medw" onmousedown="takeoffButtonDown=true;" onmouseup="takeoffButtonDown=false;" value="Run !">
         <input type=button class="button medw" onmousedown="rightButtonDown=true;" onmouseup="rightButtonDown=false;" value="right">
      </div>
      <div id="pilotButtons" style="display:none">
    <table>
      <tr>
        <td></td>
        <td><input type=button class="button medw" onmousedown="gasButtonDown=true;" onmouseup="gasButtonDown = false;" value="speed bar"></td>
        <td></td>
      </tr>
      <tr>
        <td>
        <input type=button class="button medw" onmousedown="hardLeftButtonDown=true;" onmouseup="hardLeftButtonDown=false;" value="hard left">
        <input type=button class="button medw" onmousedown="leftButtonDown=true;" onmouseup="leftButtonDown=false;" value="left"></td>
        <td></td>
        <td><input type=button class="button medw" onmousedown="rightButtonDown=true;" onmouseup="rightButtonDown=false;" value="right">
        <input type=button class="button medw" onmousedown="hardRightButtonDown=true;" onmouseup="hardRightButtonDown=false;" value="hard right">
        </td>
      </tr>
      <tr>
        <td></td>
        <td align="center"><input type=button class="button medw" onmousedown="reverseButtonDown=true;" onmouseup="reverseButtonDown=false;" value="brake"></td>
        <td></td>
      </tr>
    </table>
      </div>

    </center>
  </td>
  
  <td  valign="top" align="center">
      <MAP name="map">
        <AREA shape="circle" coords="122,272,12" href="#" onmousedown="javascript:varioSoundOnOff=switchTrueFalse(varioSoundOnOff);">
      </MAP>
   <div id="vario"  style="position:relative; width: 240px; height: 400px; background:url(img/varioScreenBackground.jpg); overflow:hidden; margin:0px; padding:0px">
      <div id="varioBarSpacer" style="height:150px;  margin:0px; padding:0px"></div>
      <div id="varioBar" style="background:black; height:1px;  margin:0px; padding:0px"></div>
      <div id="varioImg" style="position:absolute; top:0px; left:0px">
        <img src="img/vario.png" USEMAP="#map" border="0" />
        <div id="varioClimbingRate" style="position:absolute; width:60px; top:75px; right:65px;  background:url(img/varioScreenBackground.jpg); text-align:right">
             <font face="Comic Sans MS" color="#000000" size=4> ____ </font>
        </div>
        <div id="varioAltitude" style="position:absolute; width:50px; top:124px; right:58px;  background:url(img/varioScreenBackground.jpg); text-align:right">
             <font face="Comic Sans MS" color="#000000" size=5> ____ </font>
        </div>
        <div id="varioTemperature" style="position:absolute; top:156px; right:92px;  background:url(img/varioScreenBackground.jpg); text-align:right">
             <font face="Comic Sans MS" color="#000000" size=5> __ </font>
        </div>
      </div>
    </div>

   <div id="jsInfo3" ></div>
    <div>
      <div id="compass_container" style="position:relative;top:0px;right:0px;height:40px;width:200px;overflow:hidden;border: solid 1px gray">
        <div id="compass_image" style="height:40px;width:1080px;background:url(img/compass_ruler.jpg);position:relative;top:0px;right:<?php echo 2*$valTask['wind_direction']+110;?>px">
        </div>
      </div>
    </div>
    
    <div>
      <a href="how_to.html" target="_blank"><img src="../pgearth/images/famfamfamicons/help.png" border="0" /><em>pg sim how-to</em></a>
    </div>
  </td>
 </tr>
</table>

</center>


<script type="text/javascript">
// Display the map, with some controls and set the initial location 
      var mapobj = document.getElementById("mapGPS");
      var mapGPS = new GMap2(mapobj);
      mapGPS.addControl(new GSmallMapControl());
      mapGPS.addControl(new GMapTypeControl());
      //mapGPS.addControl(new GScaleControl() ) ;
      mapGPS.setCenter(new GLatLng(<?php echo $site_lat.",".$site_lng; ?>), 13, G_NORMAL_MAP);
</script>


 <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
 </script>
 <script type="text/javascript">
  _uacct = "UA-1083947-1";
  urchinTracker();
  olLoaded=1;
 </script>

<script type="text/javascript" src="wz_rotateimg.js"></script>
<script type="text/javascript">
<!--
SET_ROTATABLE("headingImg",<?php echo $valTask['wind_direction']?>);
//-->
</script> 


<?php
//end of if task not in the db, else...
}
?>
</body>
</html>