<?php 
  include ("../connexion.php");
?>

<html>
  <head>
    <title>Testing</title>
    <script type="text/javascript" src="ajax.js"></script>

    <script>
  
    var pilotsPositions = [];
    var lastCall = 0;
    var start = (new Date()).getTime();
    var ajaxFrequency = 60;

    if (!Array.prototype.forEach) {
      Array.prototype.forEach = function(fun /*, thisp*/) {
        var len = this.length;
        if (typeof fun != "function")
          throw new TypeError();
        var thisp = arguments[1];
        for (var i = 0; i < len; i++) {
          if (i in this)
            fun.call(thisp, this[i], i, this);
        }
      };
    }

    function printBr(element, index, array) {
      document.getElementById('here').innerHTML += "<br />[" + index + "] is " + element ; 
    }

    function ajaxGetPilotsPositions(pilot_id, task_id, time) {
      alert ('ajax call');
      var req =  createAjaxInstance();
      var data = "pilot_id=" + pilot_id + "&task_id=" + task_id + "&time=" + time  ;
      var req.onreadystatechange = function() { 
		if(req.readyState == 4) {
			if(req.status == 200) {
				// document.getElementById('there').innerHTML = req.responseText;
				pilotsPositions = req.responseText;
			}	
			else {
				alert("Error: returned status code " + req.status + " " + req.statusText);
			}	
		}
      }; 
      req.open("POST", "pilots_temp_positions.php", true); 
      req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      req.send(data); 
    }

    function loop(){
      var now = (new Date()).getTime();
      var timeSec = (now-start)/1000;
      if (lastCall == 0 || now-start > lastCall-5000){
        document.getElementById('there').innerHTML = "";
        ajaxGetPilotsPositions(1, 741, timeSec);
        alert ("pilot_id=" + 1 + "&task_id=" + 741 + "&time=" + timeSec);
        lastCall += 60000;
        // printArray(pilotsPositions);
        document.getElementById('there').innerHTML = pilotsPositions ;
      }
      setTimeout("loop()",100);
      document.getElementById('here').innerHTML = "timeSec = " + timeSec;
      document.getElementById('here').innerHTML += "<br />lastCall = " + lastCall;
    }

    function printArray(array){
      for(var i=0;i<array.length;i++){
//        for (var j=0; j<5; j++){
          document.getElementById('there').innerHTML += "<b>array["+i+"][0] is </b>=>"+array[i][0]+" /// ";
//        }
        document.getElementById('there').innerHTML += "<br />";
      }
    }

    </script>

  </head>
  <body onload = "loop()";>
    <div id="here"></div>
    <div id="there"></div>
  </body>
</html>
