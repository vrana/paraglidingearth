<?php


///// Tasks library //////


function isTaskFlyable($taskId) {

  $return = array('flyable'=>false,
                  'registered'=>false,
                  'firstTryNotDone'=>false,
                  'hasStarted'=>false,
                  'hasNotExpired'=>false,
                  'reason'=>'- '
                  );
  if ($_SESSION['id_membre']>0) $return['registered']=true;
  else $return['reason'].=' you must be a registered user -';


  $q = "select id from pgsim_comp_results where task_id = $taskId and pilot_id = ".$_SESSION['id_membre'];
  $r = mysql_query($q);
  $n = mysql_num_rows($r);
  if ($n == 0) $return['firstTryNotDone']=true;
  else $return['reason'].=' you already flew this task -';

  $q="select timediff(now(), starts) as hasStarted, timediff(expires, now()) as hasNotExpired  from pgsim_comp_tasks where id = $taskId";
  $r = mysql_query($q);
  $v = mysql_fetch_array($r);
  if ($v['hasStarted']>0) $return['hasStarted']=true;
  else $return['reason'].=' the task is not open yet -';
  if ($v['hasNotExpired']>0) $return['hasNotExpired']=true;
  else $return['reason'].=' the task is closed now -';

  if ($return['registered'] and $return['firstTryNotDone'] and $return['hasStarted'] and $return['hasNotExpired']) {
    $return['flyable'] = true ;
    $return['reason'] = 'you can fly the task';
  }

// print_r($return);

   return $return;
} 
?>