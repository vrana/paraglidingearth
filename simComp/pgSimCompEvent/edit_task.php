<?php
session_start();
include "../connexion.php";
include "../pgearth/secure.php";

$task_id = $_GET['task_id'];
$queryTask = "select * from pgsim_comp_tasks where id = ".$task_id;
$resultTask= mysql_query($queryTask);
$valTask = mysql_fetch_array($resultTask);

$lastActiveWaypoint = 0;
$lastActiveWaypointWhile = 0;
while ($valTask['lat_b'.$lastActiveWaypointWhile]<>0) {
	$lastActiveWaypoint = $lastActiveWaypointWhile;
	$lastActiveWaypointWhile++;
}

$member_id = $_SESSION['id_membre'];

echo "memberId = ".$member_id." /// task set up by : ".$valTask['organised_by']." /// testing ? : ".$valTask['testing']."<br />";
echo "edit condition : ".($member_id == $valTask['organised_by'] or $member_id == 1)."<br />";

if (($member_id == $valTask['organised_by'] or $member_id == 1) and $valTask['testing'] == 1) {

$querySite = "select latd, longd, nom from site where id_site = ".$valTask['site_id'];
$resultSite= mysql_query($querySite);
$valSite = mysql_fetch_array($resultSite);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

<link
    href="external_js/carpe_slider/default.css"
    rel="stylesheet"
    type="text/css" />

<script
    language="JavaScript"
    src="external_js/carpe_slider/slider.js" >
</script>

<script
    language="JavaScript"
    src="calendar_form.js" >
</script>

<script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAAloePAlEicH6IDTkMBLcJrhQ5O_AQOV-t-OYn1cRAcVTwH8AssxSaNjYyjfTXYy8UNvlgWybY6A3fug"
 type="text/javascript"></script>

<script type="text/javascript">
var marker= new Array();
var lastActiveWaypoint = <?php echo $lastActiveWaypoint;?> ;
var taskLength = 0;

function addWaypoint(){
	var waypointToAdd = lastActiveWaypoint + 1;
	if (lastActiveWaypoint == 6) {
		alert('not more than 6 waypoints per task !');
	} else {
		document.getElementById('waypointForm'+waypointToAdd).style.visibility="visible";
		marker[waypointToAdd].show();
		lastActiveWaypoint++;
		document.getElementById('last_active_waypoint').value = lastActiveWaypoint;
		dragendUpdate();
	}
}

function removeWaypoint(){
	if (lastActiveWaypoint == 1) {
		alert('You need at least one waypoint on the task !');
	} else {
		document.getElementById('waypointForm'+lastActiveWaypoint).style.visibility="hidden";
		marker[lastActiveWaypoint].hide();
		document.getElementById('distance_b'+lastActiveWaypoint).value = 0;
		lastActiveWaypoint--;
		document.getElementById('last_active_waypoint').value = lastActiveWaypoint;
		dragendUpdate() ;
	}
}

function dragendUpdate() {
	taskLength = 0 ;
	for (p=1; p <= lastActiveWaypoint; p++) {
		var pPlusOne = p+1;
		var legLength = Math.round(marker[p-1].getLatLng().distanceFrom(marker[p].getLatLng()));
		document.getElementById('distance_b'+p).value = legLength;
		document.getElementById('dist_b'+p).innerHTML = legLength / 1000;
		taskLength = taskLength + legLength;
		document.getElementById('lng_b'+p).value = marker[p].getLatLng().lng() ;
		document.getElementById('lat_b'+p).value = marker[p].getLatLng().lat() ;
	}
	document.getElementById('taskLength').innerHTML = '<b>'+ taskLength/1000 +' km</b>';
	document.getElementById('task_length').value = taskLength;
}

</script>
<link rel=stylesheet href='../pgearth/style/style.css' type='text/css' />
<title>Comp module : edit task</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta name="generator" content="HAPedit 3.1">


</head>
<body  onunload="GUnload()">

  <h1>Edit an existing task from <?php echo stripslashes($valSite['nom']);?></h1>
 
<?php
$here="my_tasks"; 
include "tabs_header.php";
?>

  <form name="build_task" method="post" action="save_updated_task.php">
  
<div class="rubriqueSite">
  <div class="titreMenu">1 - Task description in a few words</div>
  <p>Task name : <input name="task_name" value="<?php echo $valTask['task_name']?>"/></p>
  <p>Task description :
  <textarea name="task_description"><?php echo stripslashes($valTask['task_description'])?></textarea></p>
<!--
  <img src="../pgearth/images/famfamfamicons/flag_green.png" /><a href="toggle_testing_task.php?task_id=<?php echo $valTask['id']?>"><b>Set the task state as definitive</b></a>
-->
</div>


<div class="rubriqueSite">
<p>Opening date of the task :<br />
<input id="starts" name="starts" size="25" type="text" value="<?php echo $valTask['starts']?>">
<a href="javascript:NewCssCal('starts','yyyymmdd','dropdown',true,24,false)">
<img src="images/cal.gif" alt="Pick a date" height="16" width="16" border="0"></a>
<br />
Ending date of the task :<br />
<input id="expires" name="expires" size="25" type="text" value="<?php echo $valTask['expires']?>">
<a href="javascript:NewCssCal('expires','yyyymmdd','dropdown',true,24,false)">
<img src="images/cal.gif" alt="Pick a date" height="16" width="16" border="0"></a>
</p>
</div>

<div class="rubriqueSite">
<p>Is task number :<br />
<input name="task_number" size="2" value="<?php echo $valTask['is_pge_challenge']?>"/>
</p>
</div>

<div class="rubriqueSite">
  <div class="titreMenu">2 - Set the task turnpoints</div>
  <table>
    <tr>
	  <td>
	    <div id='map' style='width: 570px; height: 420px; ' ></div>
	  </td>
	  <td valign="top">
	  <div>
		Task length :<br />
<?php //                <input name="task_length"  id="task_length" readonly /> ?>
		<div id="taskLength"><b><?php echo $valTask['task_length']/1000;?> km</b></div>
	  </div>
	  <hr />
	  Drag the waypoint icons on the map to build the task.<br />
	  <a href="javascript:void(0)" onclick="addWaypoint();"><img src="../pgearth/images/famfamfamicons/add.png" border="0" /> Add a waypoint</a><br />
	  <a href="javascript:void(0)" onclick="removeWaypoint();"><img src="../pgearth/images/famfamfamicons/delete.png" border="0" /> Remove last waypoint</a><hr />
<?php
   for ($i=1; $i<7; $i++){ 
?>
	   <div id="waypointForm<?php echo $i?>">
	    * Waypoint&nbsp;<?php echo $i; ?> - 
	   <span>leg length : </span><span id="dist_b<?php echo $i?>"></span>
	   <input type="hidden" name="distance_b<?php echo $i?>" id="distance_b<?php echo $i?>" value="<?php echo $valTask['distance_b'.$i]; ?>" />
  	   <input type="hidden" name="lat_b<?php echo $i?>" id="lat_b<?php echo $i?>" value="<?php echo $valTask['lat_b'.$i]; ?>" />
  	   <input type="hidden" name="lng_b<?php echo $i?>" id="lng_b<?php echo $i?>" value="<?php echo $valTask['lng_b'.$i]; ?>" />
	   </div>
<?php
    }
?>
	  </td>
	</tr>
     </table>
</div>

<div class="rubriqueSite">
  <div class="titreMenu">3 - Set the weather conditions for the task</div>
	  
<table>
<tr>
  <td colspan="3">
    <b>Wind : </b>
  </td>
</tr>
 <tr>
  <td align="right">
    Wind direction (coming from) :
  </td>
  <td>
	<table style="background-position: center center; background-repeat: no-repeat;" background="../pgearth/rose/compass.png">
	<tbody><tr>
	<td valign="bottom" width="35" align="right" height="35"><input name="wind_direction" value="315" <?if ($valTask['wind_direction']==315)echo " checked";?> type="radio"></td>
    <td valign="top" align="center"><input name="wind_direction" value="0" <?if ($valTask['wind_direction']==0)echo " checked";?> type="radio"></td>
	<td valign="bottom" width="35" align="left"><input name="wind_direction" value="45" <?if ($valTask['wind_direction']==45)echo " checked";?> type="radio"></td>
	</tr>
	<tr>
	<td align="left"><input name="wind_direction" value="270" <?if ($valTask['wind_direction']==270)echo " checked";?> type="radio"></td>
        <td width="30" align="center" height="30">&nbsp;</td>
	<td align="right"> <input name="wind_direction" value="90" <?if ($valTask['wind_direction']==90)echo " checked";?> type="radio"></td>
	</tr>

	<tr>
	<td valign="top" align="right" height="35"><input name="wind_direction" value="225" <?if ($valTask['wind_direction']==225)echo " checked";?> type="radio"></td>
        <td valign="bottom" align="center"><input name="wind_direction" value="180" <?if ($valTask['wind_direction']==180)echo " checked";?> type="radio"></td>
	<td valign="top" align="left"><input name="wind_direction" value="135" <?if ($valTask['wind_direction']==135)echo " checked";?> type="radio"></td>
	</tr>
	</tbody></table>
    </td>
    <td>   &nbsp;
    </td>
  </tr>
<tr>
 <td align="right">
  Wind speed :
 </td>
 <td>
<div class="carpe_horizontal_slider_display_combo">
  <div class="carpe_horizontal_slider_track"  style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <div class="carpe_slider_slit" style="background-color: #300; border-color: #b99 #fdd #fdd #b99;">&nbsp;</div>
    <!-- Default position: 80px -->
    <div
      class="carpe_slider"
      id="sliderWindSpeed"
      display="displayWindSpeed"
      style="left: <?echo $valTask['wind_speed']*10;?>px; background-color: #369; border-color: #69c #036 #036 #69c;">&nbsp;</div>
  </div>
  <div class="carpe_slider_display_holder" style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <!-- Default value: 180 -->
    <input
      class="carpe_slider_display"
      name="wind_speed"
      id="displayWindSpeed"
      style="background-color: #fff; color: #258;"
      type="text"
      from="0"
      to="10"
      value="<?echo $valTask['wind_speed'];?>" />
  </div>
</div>
  </td>
  <td>  &nbsp;meters per second
  </td>
 </tr>
<tr>
  <td colspan="3">
    <b>Thermals : </b>
  </td>
</tr>
        <tr>
          <td align="right"> Thermals density :</td><td>

<div class="carpe_horizontal_slider_display_combo">
  <div class="carpe_horizontal_slider_track"  style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <div class="carpe_slider_slit" style="background-color: #300; border-color: #b99 #fdd #fdd #b99;">&nbsp;</div>
    <!-- Default position: 80px -->
    <div
      class="carpe_slider"
      id="sliderThermalsDensity"
      display="displayThermalsDensity"
      style="left: <?echo $valTask['thermals_density']*25;?>px; background-color: #369; border-color: #69c #036 #036 #69c;">&nbsp;</div>
  </div>
  <div class="carpe_slider_display_holder" style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <!-- Default value: 180 -->
    <input
      class="carpe_slider_display"
      name="thermal_density" 
      id="displayThermalsDensity"
      style="background-color: #fff; color: #258;"
      type="text"
      from="0"
      to="4"
      value="<?echo $valTask['thermals_density'];?>" />
  </div>
</div>
          </td>
          <td>  &nbsp;thermals&nbsp;per&nbsp;km&nbsp;square
          </td>
        </tr>
        <tr>
          <td align="right">
            Thermals max height : </td>
          <td>

<div class="carpe_horizontal_slider_display_combo">
  <div class="carpe_horizontal_slider_track"  style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <div class="carpe_slider_slit" style="background-color: #300; border-color: #b99 #fdd #fdd #b99;">&nbsp;</div>
    <!-- Default position: 80px -->
    <div
      class="carpe_slider"
      id="sliderThermalsHeight"
      display="displayThermalsHeight"
      style="left: <?echo $valTask['thermals_height']/42;?>px; background-color: #369; border-color: #69c #036 #036 #69c;">&nbsp;</div>
  </div>
  <div class="carpe_slider_display_holder" style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <!-- Default value: 180 -->
    <input
      class="carpe_slider_display"
      name="thermal_height" 
      id="displayThermalsHeight"
      style="background-color: #fff; color: #258;"
      type="text"
      from="0"
      to="4200"
      value="<?echo $valTask['thermals_height'];?>" />
  </div>
</div>

          </td>
          <td>&nbsp;meters&nbsp;ASL
          </td>
        </tr>
        <tr>
          <td align="right">
            Thermals average max uplift speed :  </td>
            <td>

<div class="carpe_horizontal_slider_display_combo">
  <div class="carpe_horizontal_slider_track"  style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <div class="carpe_slider_slit" style="background-color: #300; border-color: #b99 #fdd #fdd #b99;">&nbsp;</div>
    <!-- Default position: 80px -->
    <div
      class="carpe_slider"
      id="sliderThermalsSpeed"
      display="displayThermalsSpeed"
      style="left: <?echo $valTask['thermals_speed']*10;?>px; background-color: #369; border-color: #69c #036 #036 #69c;">&nbsp;</div>
  </div>
  <div class="carpe_slider_display_holder" style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <!-- Default value: 180 -->
    <input
      class="carpe_slider_display"
      name="thermal_speed"
      id="displayThermalsSpeed"
      style="background-color: #fff; color: #258;"
      type="text"
      from="2"
      to="10"
      value="<?echo $valTask['thermals_speed'];?>" />
  </div>
</div>

          </td>
          <td> meters per second
          </td>
        </tr>
        <tr>
          <td align="right">
            Thermals size :</td><td>
            <input type="radio" name="thermal_size" id="thermal_size_small" <?if ($valTask['thermals_width']==40)echo " checked";?> value="40"><label for="thermal_size_small"> small </label></input>
            <br />
            <input type="radio" name="thermal_size" id="thermal_size_medium" <?if ($valTask['thermals_width']==60)echo " checked";?>  value="60"><label for="thermal_size_medium"> medium </label></input>
            <br />
            <input type="radio" name="thermal_size" id="thermal_size_big" <?if ($valTask['thermals_width']==80) echo " checked";?>  value="80"><label for="thermal_size_big"> large </label></input>
          </td>
        </tr>
      </table>
</div>

  <input type="hidden" name="site_id" value="<?php echo $site_id;?>" />
  <input type="hidden" name="task_id" value="<?php echo $task_id;?>" />
  <input type="hidden" name="member_id" value="<?php echo $member_id;?>" />
  <input type="hidden" name="task_length" id="task_length" value="<?php echo $valTask['task_length'];?>" />
  <input type="hidden" name="last_active_waypoint" id="last_active_waypoint" value="<?php echo $lastActiveWaypoint;?>" />
  <input type="hidden" name="task_id" id="task_id" value="<?php echo $valTask['id'] ?>" />

  <div class="rubriqueSite">
  <div class="titreMenu">4 - Validate</div>
  All actual results on this task will be deleted when you submit this new modified version of the task !<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" />
  </div>
  
  </form>
  
</body>
<script type="text/javascript">
// hide the 5 last waypoints at start
for (i=2; i<7; i++){
	document.getElementById('waypointForm'+i).style.visibility="hidden";
}

//   ---- Mouse wheel zoom
function GoogleMapWheelZoom(a) {
    if(a.cancelable)    a.preventDefault();
    if ((a.detail || -a.wheelDelta) < 0) map.zoomIn();
    else map.zoomOut();        
    return false; 
}
//   ---- end of mouse wheel zoom

// Display the map, with some controls and set the initial location 
      var mapobj = document.getElementById("map");
      var map = new GMap2(mapobj);
      map.addMapType(G_PHYSICAL_MAP); 
      map.addControl(new GSmallMapControl());
      map.addControl(new GMapTypeControl());
      map.setCenter(new GLatLng(<?php echo $valSite['latd'].",".$valSite['longd']; ?>), 13, G_NORMAL_MAP);

	  map.enableDoubleClickZoom();
	  GEvent.addDomListener(mapobj, "DOMMouseScroll", GoogleMapWheelZoom);
	  GEvent.addDomListener(mapobj, "mousewheel", GoogleMapWheelZoom);

// Display markers
        var baseIcon = new GIcon(G_DEFAULT_ICON);
        baseIcon.shadow = "http://www.google.com/mapfiles/shadow50.png";
        baseIcon.iconSize = new GSize(20, 34);
        baseIcon.shadowSize = new GSize(37, 34);
        baseIcon.iconAnchor = new GPoint(9, 34);
        baseIcon.infoWindowAnchor = new GPoint(9, 2);
		
        function createMarker(point, number) {
          // Create a lettered icon for this point using our icon class
          var numberedIcon = new GIcon(baseIcon);
          numberedIcon.image = "img/markers/number" + number + ".png";
          if (number==0) markerOptions = { icon:numberedIcon };
		  else markerOptions = { icon:numberedIcon, draggable: true };
		 
          var marker = new GMarker(point, markerOptions);
		 
          GEvent.addListener(marker, "click", function() {
            if (number==0) marker.openInfoWindowHtml("Waypoint <b>" + number + " : Start  point</b>");
			else marker.openInfoWindowHtml("Waypoint <b>" + number + "</b>");
          });
		  
		  if (number>0){
			GEvent.addListener(marker, "dragstart", function() {
				map.closeInfoWindow();
			});
		 
			GEvent.addListener(marker, "dragend", function() {
				dragendUpdate();
			});
		  }
		return marker;
        }

	var bounds = new GLatLngBounds();
	bounds.extend(new GLatLng(<?php echo $valSite['latd'].", ".$valSite['longd']?>));

	marker[0] = createMarker(new GLatLng(<?php echo $valSite['latd'].", ".$valSite['longd']?>), 0) ;
        map.addOverlay(marker[0]);

<?php
	$numberOfWaypoints= 0;
   for ($i=0; $i<7; $i++){ 
           $j=$i-1; 
		   if ($valTask['lat_b'.$i]<>0 and $valTask['lng_b'.$i]<>0) {
			   	$numberOfWaypoints=$i;
				echo "
				marker[".$i."] = createMarker(new GLatLng(".$valTask['lat_b'.$i].", ".$valTask['lng_b'.$i]."), ".$i.");
				map.addOverlay(marker[".$i."]);";
		    } else {
				echo "
				marker[".$i."] = createMarker(new GLatLng(".$valSite['latd'].", ".$valSite['longd']."), ".$i.");
				map.addOverlay(marker[".$i."]);";
		    }
   }
  for ($i=6; $i>$numberOfWaypoints; $i--){    
         /*  if ($i>1) */ echo "
		   marker[$i].hide();";
    }
  for ($i=1; $i<=$numberOfWaypoints; $i++){    
           echo "
		   document.getElementById('waypointForm".$i."').style.visibility='visible';
		   document.getElementById('distance_b".$i."').value= ".$valTask['distance_b'.$i].";
		   document.getElementById('dist_b".$i."').innerHTML = \"". $valTask['distance_b'.$i]/1000 ." km\";
		   document.getElementById('lat_b".$i."').value=".$valTask['lat_b'.$i].";
		   document.getElementById('lng_b".$i."').value=".$valTask['lng_b'.$i].";
		   bounds.extend(new GLatLng(".$valTask['lat_b'.$i].", ".$valTask['lng_b'.$i]."));";
    }
  if ($numberOfWaypoints == 0) { echo "addWaypoint(); "; $numberOfWaypoints =1; }
  echo "
        lastActiveWaypoint = ".$numberOfWaypoints." ;" ;
?>

		var zoomLevel  = map.getBoundsZoomLevel(bounds);
		map.setZoom(zoomLevel);
		map.panTo(bounds.getCenter());

</script>
</html>
<?php
} else {
   if ($valTask['testing'] == 1){
	echo "sorry, only the person who build the task can modify it.<br />If you are that person, please log in first on paraglidingearth.com";
   } else {
	echo "this task can not be modified : it was set by its author as in its definitive version !";
   }
}
?>
