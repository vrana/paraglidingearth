<?php
session_start();
include "../connexion.php";
include "../pgearth/secure.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<link rel=stylesheet href='../pgearth/style/style.css' type='text/css' />
<title>build a task</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
</head>
<body>
<h1>Build your own task and submit it to the whole world</h1>

<?php
$here = "build_task"; 
include "tabs_header.php";

if (/*isset($_SESSION['id_membre'])*/true){

?>


<div class="rubriqueSite">
<div class="titreMenu">What to know before you start</div>
As this is in beta, read this before you can start :
<ul class="menu">
	<li>task can only start from a pg site known in our database with gps coordinates also known : select the country below and click on the site where you want your task to start</li>
	<li>when building a task, you can set it to 'testing' or 'definitive' status : 'testing' will allow you to modify the settings for the task (turnpoints and/or weather), but after each modification
	the saved results will be cancelled for scoring as the task competed has changed, so take care not to leave your task in 'testing' mode during a too long time...</li>
</ul>
	
</div>
&nbsp;
<div class="rubriqueSite">
<div class="titreMenu">Select country and event below :</div>
<form name="country" action="" method="post">
	<select name="country_id"  <?php if (isset($_POST['country_id'])) echo "disabled";?> >
		<option value='0'> --- Select a country ---</option>
			<?
			$query  = " SELECT DISTINCT id_pays, nom_pays_en, iso
FROM pays
LEFT JOIN site ON site.pays = id_pays
WHERE id_site
ORDER BY iso ";
			$result = mysql_query($query);
			while ($val = mysql_fetch_array($result)){
			?>
			<option value='<?=$val['id_pays']?>' <?php if ($_POST['country_id']==$val['id_pays']) echo 'selected';?> ><?php echo $val['iso'];?> - <?=$val['nom_pays_en']?>
			</option>
			<?
			}
			?>      
	</select>

<div class="rubriqueSite">
  Select event : 
  <select name="event_id" <?php if (isset($_POST['event_id'])) echo "disabled";?> >
      <option value="0">Select an event</option>

<?php $qEvent="select id, event_name from pgsim_comp_events where organised_by = ".$_SESSION['id_membre'];
      if ($_SESSION['id_membre']==1) $qEvent="select id, event_name from pgsim_comp_events" ;
      $rEvent=mysql_query($qEvent);
      while ($vEvent=mysql_fetch_array($rEvent)) {
?>
      <option value="<?php echo $vEvent['id'];?>" <?php if ($_POST['event_id']==$vEvent['id']) echo "selected";?> ><?php echo $vEvent['event_name'];?></option>
<?php }  ?>

  </select>
<br />

<?php if (! isset($_POST['country_id']) or ! isset($_POST['event_id'])) echo '<input type="submit" value="continue" />';?>
</div>

</form>
<?php
if (isset($_POST['country_id']) and ($_POST['event_id']>0)) {
	$queryArea = "select id_region, nom_region_en from region where pays = ".$_POST['country_id']." order by nom_region_en";
	$resultArea = mysql_query($queryArea);
	while ($valArea = mysql_fetch_array($resultArea)){
		echo "
		<b>".$valArea['nom_region_en']."</b><br />";
		$querySites = "select id_site, nom from site where region = ".$valArea['id_region']." and longd !=0 and latd !=0 order by nom";
		$resultSites = mysql_query($querySites);
		while ($valSites = mysql_fetch_array($resultSites)){
			echo "
			&nbsp;&nbsp;&nbsp;-&nbsp;<a href=\"build_task.php?site_id=".$valSites['id_site']."&event_id=".$_POST['event_id']."\">".stripslashes($valSites['nom'])."</a><br />";
		}
	}
}
?>


<?php
} else { echo "you are not allowed here..."; 
}
?>

</div>

</html>
