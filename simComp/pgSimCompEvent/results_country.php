<?php
session_start();
include "../connexion.php";
$yearNow = date('Y');
$year = date('Y');
if (isset($_GET['year'])) $year = $_GET['year'];
$orderItem = 'total';
if (isset($_GET['order'])) $orderItem = $_GET['order'];
//default :
$count_mode="first_flight";
if (isset($_GET['count_mode'])) $count_mode = $_GET['count_mode'];

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<script type="text/javascript" src="../scripts/overlib.js"></script>
<link rel=stylesheet href='../pgearth/style/style.css' type='text/css' />
<title>Overall comp results</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
</head>
<body>

<h1>Overall competion results</h1> 
 
<?php
$here = "results_overall"; 
include "tabs_header.php";
?>

<div class="rubriqueSite">
<div class="titreMenu">Overall results beta page  <a href="rss/rss_flights.php" title="rss of the results"><img src="../pgearth/images/famfamfamicons/rss.png" border="0" /></a></div>
... still to come : ranking by country, by year (season?), group tasks in events?, etc, blah blah..
<br />&nbsp;
  <link href="../pgearth/style/style_tabs.css" rel="stylesheet" type="text/css">

  <div id="tabs">
      <ul>
<?php
for ($i=2008; $i<=$yearNow; $i++){
?>		<li <?php if($i==$year) echo "class=\"here\"";?>><a href="?year=<?php echo $i?>"><?php echo $i?></a></li>
<?php
}
?>
      </ul>
  </div>
  <div id="content">

<div id="tabs">
    <ul>
      <li <?php if($count_mode=="first_flight") echo "class=\"here\"";?>><a href="?count_mode=first_flight&year=<?php echo $year;?>">Individual "First Attempt" ranking</a></li>
      <li <?php if($count_mode=="all_flights") echo "class=\"here\"";?>><a href="?count_mode=all_flights&year=<?php echo $year;?>">Individual "Best Attempt" ranking</a></li>
      <li <?php if($count_mode=="by_country") echo "class=\"here\"";?>><a href="?count_mode=by_country&year=<?php echo $year;?>">"By country" ranking</a></li>
      <li <?php if($count_mode=="national") echo "class=\"here\"";?>>"National" rankings</li>
    </ul>
</div>
<div id="content">

<?php




if ($count_mode=="first_flight") {
$sql = 'SELECT sum(first_try_score) as total , count( id ) as flights , count( distinct task_id ) as tasks , pilot_id , pseudo, iso, nom_pays_en, 
round(sum(first_try_score)/count(distinct task_id)) as average_per_task,
round(sum(first_try_score)/count(id)) as average_per_flight
FROM `pgsim_comp_results` left join `auteur` on id_auteur = pilot_id left join pays on id_pays = pays_auteur
where year(pgsim_comp_results.date) = '.$year.'
and not cancelled group by pilot_id
order by '.$orderItem.' desc ';
$que = mysql_query($sql);
?>
<h3>Ranking based on the consideration of the results of the <u>first attempt</u> made by pilot on each task.</h3>
    <table border="1">
	<tr align="center">
		<td> Pilot </td>
		<td> Rank </td>
		<td> <a href="?count_mode=<?php echo $count_mode;?>&order=total&year=<?php echo $year;?>">Total scoring</a></td>
		<td> Tasks flown </td>
		<td> Total attempts</td>
		<td> <a href="?count_mode=<?php echo $count_mode;?>&order=average_per_task&year=<?php echo $year;?>">Points per task</a></td>
		<td> <a href="?count_mode=<?php echo $count_mode;?>&order=average_per_flight&year=<?php echo $year;?>">Points per flight</a></td>
		<td> View pilot results </td>
	</tr>
<?php
$i=0;
while ($val = mysql_fetch_array($que)){
$i++;
?>
	<tr align="center">
		<td><a href="results_pilot.php?pilot_id=<?php echo $val['pilot_id'];?>"><?php echo $val['pseudo'];?></a> - <img src="img/flags/_tn_<?php echo strtolower($val['iso']);?>.png" /></td>
		<td><?php echo $i;?></td>
		<td><?php echo $val['total'];?></td>
		<td><?php echo $val['tasks'];?></td>
		<td><?php echo $val['flights'];?></td>
		<td><?php echo $val['average_per_task'];?></td>
		<td><?php echo $val['average_per_flight'];?></td>
		<td><a href="results_pilot.php?pilot_id=<?php echo $val['pilot_id'];?>"><img src="../pgearth/images/famfamfamicons/chart_line.png" border="0"></a></td>
	</tr>
<?php
 }
echo "</table>";

}  
// end of if first_flights


if ($count_mode=="all_flights") {
$sql = 'SELECT sum(score) as total , count( id ) as flights , count( distinct task_id ) as tasks , pilot_id , pseudo, iso, nom_pays_en, 
round(sum(score)/count(distinct task_id)) as average_per_task,
round(sum(score)/count(id)) as average_per_flight
FROM `pgsim_comp_results` left join `auteur` on id_auteur = pilot_id left join pays on id_pays = pays_auteur
where year(pgsim_comp_results.date) = '.$year.'
and not cancelled group by pilot_id
order by '.$orderItem.' desc ';
$que = mysql_query($sql);
?>
<h3>Ranking based on the consideration of the results of the <u>best flight</u> made by pilot on each task.</h3>
    <table border="1">
	<tr align="center">
		<td> Pilot </td>
		<td> Rank </td>
		<td> <a href="?count_mode=<?php echo $count_mode;?>&order=total&year=<?php echo $year;?>">Total scoring</a></td>
		<td> Tasks flown </td>
		<td> Total attempts</td>
		<td> <a href="?count_mode=<?php echo $count_mode;?>&order=average_per_task&year=<?php echo $year;?>">Points per task</a></td>
		<td> <a href="?count_mode=<?php echo $count_mode;?>&order=average_per_flight&year=<?php echo $year;?>">Points per flight</a></td>
		<td> View pilot results </td>
	</tr>
<?php
$i=0;
while ($val = mysql_fetch_array($que)){
$i++;
?>
	<tr align="center">
		<td><a href="results_pilot.php?pilot_id=<?php echo $val['pilot_id'];?>"><?php echo $val['pseudo'];?></a> - <img src="img/flags/_tn_<?php echo strtolower($val['iso']);?>.png" /></td>
		<td><?php echo $i;?></td>
		<td><?php echo $val['total'];?></td>
		<td><?php echo $val['tasks'];?></td>
		<td><?php echo $val['flights'];?></td>
		<td><?php echo $val['average_per_task'];?></td>
		<td><?php echo $val['average_per_flight'];?></td>
		<td><a href="results_pilot.php?pilot_id=<?php echo $val['pilot_id'];?>"><img src="../pgearth/images/famfamfamicons/chart_line.png" border="0"></a></td>
	</tr>
<?php
 }
echo "</table>";
}
// end of if all_flights





if ($count_mode=="by_country") {
$sql = 'SELECT sum(first_try_score) as total , count( id ) as flights , count( distinct task_id ) as tasks , count( distinct pilot_id) as pilots , pseudo, iso, nom_pays_en, 
round(sum(first_try_score)/count(distinct pilot_id)) as average_per_pilot,
round(sum(first_try_score)/count(distinct task_id)) as average_per_task,
round(sum(first_try_score)/count(id)) as average_per_flight
FROM `pgsim_comp_results` left join `auteur` on id_auteur = pilot_id left join pays on id_pays = pays_auteur
where year(pgsim_comp_results.date) = '.$year.'
and not cancelled group by pays_auteur
order by '.$orderItem.' desc ';
$que = mysql_query($sql);
?>

<h3>Country ranking (using 'first attempt' result of each pilot on each task).</h3>
<table border="1">
	<tr align="center">
		<td> Country </td>
		<td> Rank </td>
		<td> <a href="?count_mode=<?php echo $count_mode;?>&order=total&year=<?php echo $year;?>">Total scoring</a></td>
		<td> Pilots </td>
		<td> Tasks flown </td>
		<td> Total attempts</td>
		<td> <a href="?count_mode=<?php echo $count_mode;?>&order=average_per_task&year=<?php echo $year;?>">Points per pilot</a></td>
		<td> <a href="?count_mode=<?php echo $count_mode;?>&order=average_per_task&year=<?php echo $year;?>">Points per task</a></td>
		<td> <a href="?count_mode=<?php echo $count_mode;?>&order=average_per_flight&year=<?php echo $year;?>">Points per flight</a></td>
		<td> View national results </td>
	</tr>
<?php

$i=0;
while ($val = mysql_fetch_array($que)){
$i++;
?>
	<tr align="center">
		<td><?php echo $val['nom_pays_en'];?> - <img src="img/flags/_tn_<?php echo strtolower($val['iso']);?>.png" /></td>
		<td><?php echo $i;?></td>
		<td><?php echo $val['total'];?></td>
		<td><?php echo $val['pilots'];?></td>
		<td><?php echo $val['tasks'];?></td>
		<td><?php echo $val['flights'];?></td>
		<td><?php echo $val['average_per_pilot'];?></td>
		<td><?php echo $val['average_per_task'];?></td>
		<td><?php echo $val['average_per_flight'];?></td>
		<td><a href="results_country.php?country_id=<?php echo $val['iso'];?>"><img src="../pgearth/images/famfamfamicons/chart_line.png" border="0"></a></td>
	</tr>

<?php
 }
echo "</table>";
}
// end of if by_country

?>


</div>


    </div>
</div>

<p>
<a href="index.php"><< Back to index</a>
</p>

</body>
</html>
