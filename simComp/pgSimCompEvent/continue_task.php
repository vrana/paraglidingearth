<?php
session_start();
include "../connexion.php";


$vEvent = mysql_fetch_array(mysql_query("select * from pgsim_comp_events where id =".$_GET['event']));

$poo = $_GET['task']-1;

$query = "select id from pgsim_comp_tasks where competition = ".$_GET['event']." order by starts limit ".$poo.", 1 ";
if (isset($_GET['result_id'])) $query = "select task_id as id from pgsim_comp_results where id =".$_GET['result_id'];
$result= mysql_query($query);
$num = mysql_num_rows($result);
$val = mysql_fetch_array($result);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<script type="text/javascript" src="../scripts/overlib.js"></script>
<script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAAloePAlEicH6IDTkMBLcJrhQ5O_AQOV-t-OYn1cRAcVTwH8AssxSaNjYyjfTXYy8UNvlgWybY6A3fug" type="text/javascript"></script>

<script type="text/javascript" src="../scripts/highslide/highslide.js"></script>
<script type="text/javascript">  
    /* <![CDATA[ */
    hs.showCredits = false;
    hs.graphicsDir = '../scripts/highslide/graphics/';
    hs.outlineType = 'rounded-white';
    hs.dimmingOpacity = 0.82;
    /* ]]> */
</script>

<link rel=stylesheet href='../pgearth/style/style.css' type='text/css' />
<title>Continue a task</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta name="generator" content="HAPedit 3.1">
</head>
<body>
<h1><?php echo $vEvent['event_name'];?></h1>


<?php
$here = "start"; 
include "tabs_header.php";

if ($num == 0) {
	echo "<h1>No task are open currently...</h1>";
} 

if ($num == 1) {

$task_id = $val['id'];

$queryFirstFlight = "select * from pgsim_comp_results where task_id = $task_id and  pilot_id =". $_SESSION['id_membre']."";
if (isset($_GET['result_id'])) $queryFirstFlight = "select * from pgsim_comp_results where id = ".$_GET['result_id']." and  pilot_id =". $_SESSION['id_membre']."";
$resultFirstFlight= mysql_query($queryFirstFlight);
$numFirstFlight = mysql_num_rows($resultFirstFlight);
$valFirstFlight = mysql_fetch_array($resultFirstFlight);


$query = "select *, datediff(expires, now()) as expireDiff from pgsim_comp_tasks where id = $task_id";
$result= mysql_query($query);
$val = mysql_fetch_array($result);


$querySite = "select latd, longd, nom from site where id_site = ".$val['site_id'];
$resultSite= mysql_query($querySite);
$valSite = mysql_fetch_array($resultSite);

$queryAuthor = "select pseudo from auteur where id_auteur = ".$val['organised_by'];
$resultAuthor= mysql_query($queryAuthor);
$valAuthor = mysql_fetch_array($resultAuthor);

$totalDistance = 0;


?>

<div>
  <div style='width: 564px; float:right' >
  <div id='map' style='width: 560px; height: 300px;'> </div>

 </div>

  <div  style="margin-right:568px">
  <div class="rubriqueSite">
    <h1><?php echo $val['task_name'];?></h1>
    <h2><?php echo "This task will expire in ".$val['expireDiff']." days";?></h2>

    <blockquote><?php echo stripslashes($val['task_description']);?></blockquote>
  </div>



<?php
if ($numFirstFlight>0) {
?>

<form method="post" action="pgSimComp.php" name="compete">

<div class="rubriqueSite">
<input type="hidden" name="glider" id="yellow" value="<?php echo $valFirstFlight['glider']; ?>">
<input type="hidden" name="continue" value="1" />
<input type="hidden" name="result_id" value="<?php echo $valFirstFlight['id'];?>" />
<input type="hidden" name="task_id" value="<?php echo $task_id;?>" />
<input type="hidden" name="competition_id" value="<?php echo $_GET['event'];?>" />
<h1 style="text-align:center;"><a href="javascript:void()" onclick="window.document.compete.submit()"  style="padding:6px; border:solid; border-width:2px; border-color:blue ; background-color:lightblue;">Continue the task !</a></h1>

</form>
  
  <div class="rubriqueSite">
  <table>
  <tr>
  <td>
  <b>Task length</b>
    <ul class="menu">
	<?php for ($p=1; $p<7; $p++){ 
		if ($val['lat_b'.$p] and $val['lng_b'.$p]) {
			$totalDistance += $val['distance_b'.$p]/1000;?>
		<li> &nbsp;to waypoint #<?php echo $p;?> :&nbsp;&nbsp;&nbsp;<?php echo $val['distance_b'.$p]/1000;?> km </li>
	<?php }
	}?>
		<li style="list-style-type:none"> _________________________</li>
	</ul>
	<b>Total task length : <?php echo $totalDistance;?> km</b> </li>

  </td>
  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  <td valign="top">
  <b>Weather conditions</b>
  <ul class="menu">
	<li> Wind : at <?php echo $val['wind_speed']?> m/s, from <?php echo $val['wind_direction']?>&deg;</li>
	<li> Thermals
		<ul class="menu">
			<li>max height : <?php echo $val['thermals_height']?> m</li>
			<li>max uplift : <?php echo $val['thermals_speed']?> m/s</li>
			<li>thermal activity : <?php
			switch ($val['thermals_density']){
				case 0 : echo "absolutely no thermals"; break;
				case 1 : echo "very few thermals"; break;
				case 2 : echo "some thermals"; break;
				case 3 : echo "quite a lot of thermals"; break;
				case 4 : echo "plenty of thermlas"; break;
			}
			echo " (".$val['thermals_density']."/4)";?></li>
		</ul>
	</ul>
	</td>
	</tr>
	</table>

  
  </div>

<?php
} else {
?>
<div class="rubriqueSite">
You are not supposed to be here<br />
it seems that you don t have a flight saved that you could continue...
</div>
<?php
}
?>

</div>

<div style="clear:right">&nbsp;</div>
</div>

</body>

<script type="text/javascript">
// Display the map, with some controls and set the initial location 
      var mapobj = document.getElementById("map");
      var map = new GMap2(mapobj);
      map.addControl(new GSmallMapControl());
 //     map.addControl(new GMapTypeControl());
	  map.addMapType(G_SATELLITE_3D_MAP);
      map.addMapType(G_PHYSICAL_MAP);
      map.addControl(new GHierarchicalMapTypeControl());
      map.setMapType(G_PHYSICAL_MAP);
      map.setCenter(new GLatLng(<?php echo $valSite['latd'].",".$valSite['longd']; ?>), 13);
	  
// Display markers
        var baseIcon = new GIcon(G_DEFAULT_ICON);
        baseIcon.shadow = "http://www.google.com/mapfiles/shadow50.png";
        baseIcon.iconSize = new GSize(20, 34);
        baseIcon.shadowSize = new GSize(37, 34);
        baseIcon.iconAnchor = new GPoint(9, 34);
        baseIcon.infoWindowAnchor = new GPoint(9, 2);
		
        function createMarker(point, number, distance) {
          // Create a lettered icon for this point using our icon class
          var numberedIcon = new GIcon(baseIcon);
          numberedIcon.image = "img/markers/number" + number + ".png";
          markerOptions = { icon:numberedIcon };
          var marker = new GMarker(point, markerOptions);

          GEvent.addListener(marker, "click", function() {
            if (number==0) marker.openInfoWindowHtml("Waypoint <b>" + number + " : Start  point</b>");
			else marker.openInfoWindowHtml("Waypoint <b>" + number + "</b> : " + distance/1000 + " km leg");
          });
          return marker;
        }

	map.addOverlay(createMarker(new GLatLng(<?php echo $valSite['latd'].", ".$valSite['longd']?>), 0, 0));
	
	var bounds = new GLatLngBounds();
	bounds.extend(new GLatLng(<?php echo $valSite['latd'].", ".$valSite['longd']?>));
<?php
   $iJs = 1;
   $polyline = "";
   for ($i=1; $i<7; $i++){
       if ($val['lat_b'.$i] and $val['lng_b'.$i]){
		   
           echo "  map.addOverlay(createMarker(new GLatLng(".$val['lat_b'.$i].", ".$val['lng_b'.$i]."), ".$iJs.", ".$val['distance_b'.$i]."));\n" ;
           echo "  bounds.extend(new GLatLng(".$val['lat_b'.$i].", ".$val['lng_b'.$i]."));";
		   $polyline .= ",\n        new GLatLng(".$val['lat_b'.$i].", ".$val['lng_b'.$i].")" ;
		   $iJs++;
        }
    }
?>
        var polyline = new GPolyline([
  		  new GLatLng(<?php echo $valSite['latd'].", ".$valSite['longd']?>),
  		  <?php echo $polyline?>
		], "#ff0000", 10);
		map.addOverlay(polyline);
		
		var zoomLevel  = map.getBoundsZoomLevel(bounds);
		//var taskCenter = new GLatLng(bounds.getCenter();
		map.setZoom(zoomLevel);
		map.panTo(bounds.getCenter());

</script>


<?php 
}  //end of "if 1 task found"
?>
</html>
