<?php

function checkBrowser($input) {

$browsers = "mozilla msie gecko firefox chrome ";
$browsers.= "konqueror safari netscape navigator ";
$browsers.= "opera mosaic lynx amaya omniweb";

$browsers = split(" ", $browsers);

$userAgent = strToLower( $_SERVER['HTTP_USER_AGENT']);

// echo $userAgent;

$l = strlen($userAgent);
for ($i=0; $i<count($browsers); $i++){
  $browser = $browsers[$i];
  $n = stristr($userAgent, $browser);
  if(strlen($n)>0){
    $version = "";
    $navigator = $browser;
    $j=strpos($userAgent, $navigator)+$n+strlen($navigator)+1;
    for (; $j<=$l; $j++){
      $s = substr ($userAgent, $j, 1);
      if(is_numeric($version.$s) )
      $version .= $s;
      else
      break;
    }
  }
}

    if (strpos($userAgent, 'linux')) {
        $platform = 'linux';
    }
    else if (strpos($userAgent, 'macintosh') || strpos($userAgent, 'mac platform x')) {
        $platform = 'mac';
    }
    else if (strpos($userAgent, 'windows') || strpos($userAgent, 'win32')) {
        $platform = 'windows';
    }

if ($input=='array') {
        return array(
        "browser"      => $navigator,
        "version"      => $version,
        "platform"     => $platform,
        "userAgent"    => $userAgent);
}else{
    return "$navigator $version on $platform";
}

}

/* USAGE: */
//array output:
//  $i = checkBrowser('array');
//  print_r($i);
//will give:
//Array ( [browser] => firefox [version] => 3.0 [platform] => windows [userAgent] => mozilla/5.0 (windows; u; windows nt 5.1; pl; rv:1.9.0.6) gecko/2009011913 firefox/3.0.6 )

//string output:
echo checkBrowser('string');
//will give: firefox 3.0

?>
