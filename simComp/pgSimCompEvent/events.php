<?php
session_start();
include "../connexion.php";

/****** Cookie ******/
$time = microtime();
$time = explode(" ", $time);
$time = $time[1] + $time[0];
$start_load = $time;
if ($_SESSION['creercookie']=="oui"){
    $duree = 365*24*60*60; // 1 an en secondes !
    setcookie("passcookie",$_SESSION['passwordcrypte'],time()+$duree);
    setcookie("pseudocookie",$_SESSION['pseudo_membre'],time()+$duree);
    $_SESSION['creercookie']="non";
}
if (!isset($_SESSION['tested_cookie'])){
include "../pgearth/cookie.php";}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1083947-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript" src="../scripts/overlib.js"></script>
<link rel=stylesheet href='../pgearth/style/style.css' type='text/css' />
<title>pgSim Competition Events</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<script type="text/javascript" src="../scripts/moodalbox/js/mootools.js"></script>
<script type="text/javascript" src="../scripts/moodalbox/js/moodalbox.js"></script>
<link rel="stylesheet" href="../scripts/moodalbox/css/moodalbox.css" type="text/css" media="screen" />
</head>
<body>

<h1>Welcome to pgSim Competition Events</h1> 
 
<?php
$here = "results_overall"; 
include "tabs_header.php";
?>

<div class="rubriqueSite">

<br />&nbsp;

  <link href="../pgearth/style/style_tabs.css" rel="stylesheet" type="text/css">

  <div id="tabs">
      <ul>
	<?php
	$displayEvent=0;
	$qEvent = "select * from pgsim_comp_events left join auteur on id_auteur=organised_by  where id <> 0 order by id";
	$rEvent = mysql_query($qEvent);

	while ($vEvent=mysql_fetch_array($rEvent)){
	?>
		<li <?php if($_GET['event']== $vEvent['id']) {echo "class=\"here\""; $displayEvent=1; $event_id=$vEvent['id'];$event_comment= $vEvent['event_comment'];$event_author=$vEvent['pseudo'];} ?>><a href="?event=<?php echo $vEvent['id'];?>"><?php echo $vEvent['event_name']?></a></li>
	<?php
	}
	?>
      </ul>
  </div>

  <div id="content">
<?php
if ($displayEvent==0) echo "<h2>Select an event above</h2>";
else {

echo "<h2>Event details</h2>";

$qTask = "select * from pgsim_comp_tasks where competition = ".$event_id." order by starts";
$rTask = mysql_query($qTask);
$num  = mysql_num_rows($rTask);

echo $num." tasks : <em>".$event_comment."</em> <br />
Task set up by ".$event_author."
<br />
<table><tr><td width=\"33%\">
<script src=\"http://connect.facebook.net/en_US/all.js#xfbml=1\"></script><fb:like></fb:like>
</td><td width=\"34%\">
<!-- AddToAny BEGIN -->
<div class=\"a2a_kit a2a_default_style\">
<a class=\"a2a_dd\" href=\"http://www.addtoany.com/share_save\">Share</a>
<span class=\"a2a_divider\"></span>
<a class=\"a2a_button_email\"></a>
<a class=\"a2a_button_facebook\"></a>
<a class=\"a2a_button_twitter\"></a>
<a class=\"a2a_button_delicious\"></a>
<a class=\"a2a_button_google_gmail\"></a>
<a class=\"a2a_button_hotmail\"></a>
</div>
<script type=\"text/javascript\" src=\"http://static.addtoany.com/menu/page.js\"></script>
<!-- AddToAny END -->
</td><td width=\"33%\">
<form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\">
<input type=\"hidden\" name=\"cmd\" value=\"_s-xclick\">
<input type=\"hidden\" name=\"encrypted\" value=\"-----BEGIN PKCS7-----MIIHNwYJKoZIhvcNAQcEoIIHKDCCByQCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYARwM9kaUXggwKsET13OVlaY/38PkI/LMx+HM/DLeJ/zFCzKFAo2/ks10zqlEHNS3w/ifOuva3ZhO97dxZE6B3HNwP9opfgcTuQWV9DAEvUgzH7D/yxzkK3YC5UapJqp8e8cPOoghCYXCop1IEjEtZ46MbCqvzHV9U3yU+xkK6heDELMAkGBSsOAwIaBQAwgbQGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQI7UaKYlwsqO2AgZBZPGdzeFZMkjY0H0I/eluGvFz7whTFQZPAhOp/N8EQsaUzzlrlRdCUJh2ipFDiyPqK5wRiNzwnTKuxXOPuJpND0oRBL7w9OrXAqwNV6F4139VgtkjKvUFu5vQ3nzi7bOFr8DQYlyxFld/kEUfJs3WHDN/ijo649tBOohoGSgkx/XJloEXa9XlbFQ0rYcpX6tegggOHMIIDgzCCAuygAwIBAgIBADANBgkqhkiG9w0BAQUFADCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wHhcNMDQwMjEzMTAxMzE1WhcNMzUwMjEzMTAxMzE1WjCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMFHTt38RMxLXJyO2SmS+Ndl72T7oKJ4u4uw+6awntALWh03PewmIJuzbALScsTS4sZoS1fKciBGoh11gIfHzylvkdNe/hJl66/RGqrj5rFb08sAABNTzDTiqqNpJeBsYs/c2aiGozptX2RlnBktH+SUNpAajW724Nv2Wvhif6sFAgMBAAGjge4wgeswHQYDVR0OBBYEFJaffLvGbxe9WT9S1wob7BDWZJRrMIG7BgNVHSMEgbMwgbCAFJaffLvGbxe9WT9S1wob7BDWZJRroYGUpIGRMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbYIBADAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBAIFfOlaagFrl71+jq6OKidbWFSE+Q4FqROvdgIONth+8kSK//Y/4ihuE4Ymvzn5ceE3S/iBSQQMjyvb+s2TWbQYDwcp129OPIbD9epdr4tJOUNiSojw7BHwYRiPh58S1xGlFgHFXwrEBb3dgNbMUa+u4qectsMAXpVHnD9wIyfmHMYIBmjCCAZYCAQEwgZQwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tAgEAMAkGBSsOAwIaBQCgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0xMDA1MjcxMjEwNTJaMCMGCSqGSIb3DQEJBDEWBBShXSoHOWafB3FyTtNPz1D5NtYCfTANBgkqhkiG9w0BAQEFAASBgCV1UOx+zTVBSrgcMiFFkQTx+Xm7vJTi/VfyERT41Ycb1asLhg6+7BGR4fia0AFAMBLdcBp44wYDTJK1HRWsNQcQOmSoJcptEmd6ChkgQ5FkQSIJrl+GmtquIXTlwm0SAQ8iO14RUWcrJSpVPi7owoSWU7raOLBn+y3xp8mAVgev-----END PKCS7-----
\">
<input style=\"border : 0px\" type=\"image\" src=\"https://www.paypal.com/en_US/i/btn/btn_donate_SM.gif\" border=\"0\" name=\"submit\" alt=\"PayPal - The safer, easier way to pay online!\">
<img alt=\"\" border=\"0\" src=\"https://www.paypal.com/fr_FR/i/scr/pixel.gif\" width=\"1\" height=\"1\">
</form>
</td></tr></table>";
?>

<?php if ($event_id==12) { ?>
<style>
/* fluid iframes begin */
.iframeWrapper {
position: relative;
padding-bottom: 52.25%;
padding-top: 0px;
height: 0;
z-index: 2;
float: right;
}
.iframeWrapper iframe {
/*position: absolute;*/
top: 0;
left: 0;
width: 520px;
height: 300px !important;
z-index: 1;
overflow:hidden;
}
.lt-ie8 .iframeWrapper iframe {
width: 520px;
height: 300px !important;
}
/* fluid iframes end */
</style>
<div class="iframeWrapper"><iframe scrolling="no" frameborder="0" allowfullscreen name="redbullxalps-livetracking-athlete" src="http://www.redbullxalps.com/live-tracking.html?men=1&map=3d&zoom=10&utm_source=lt-iframe&utm_medium=full&utm_campaign=rbx13"></iframe></div>
<?php } ?>

<?php 

echo "<ul>";

$eventIsOpen = false;
$countTask=0;
while ($vTask=mysql_fetch_array($rTask)){

  $countTask++;
  $isOpen='finished';
  $actualTime = time();


  $opens= strtotime($vTask['starts']);
  $closes= strtotime($vTask['expires']);

  if ( $actualTime < $opens ) $isOpen='<b>not open yet</b>';
  if ( $actualTime > $opens and $actualTime < $closes ) $isOpen='<b>open</b>';
  if ( $actualTime > $opens ) $eventIsOpen = true;
?>

  <li><b><?php echo $vTask['task_name'];?></b>
   - <?php if($isOpen=='<b>open</b>') echo '<img src="../pgearth/images/famfamfamicons/tick.png" /> '; else echo '<img src="../pgearth/images/famfamfamicons/cross.png" /> ';echo $isOpen;?>
   <!--  -->
  <?php if ($isOpen=='<b>not open yet</b>') echo "<b>";?> Starting : <?php  echo date("r e", $opens);?><?php if ($isOpen=='<b>not open yet</b>') echo "</b>";?>,<br />
  <?php if ($isOpen=='<b>open</b>') echo "<b>";?> ending : <?php  echo date("r e", $closes);?><?php if ($isOpen=='<b>open</b>') echo "</b>";?>
  <?php if ($isOpen=='<b>open</b>') echo "<b> - <a href='start_task.php?event=".$event_id."&task=".$countTask."'>Fly the task</a></b>";?>
  </li>
  <?php
}
?>
</ul>

<div
 style="background-color:#FFEFD5;text-align:center;border:1px solid #CD853F" >
Resume a crashed flight  !<br />
<a href="http://www.paraglidingforum.com/viewtopic.php?p=203830#203830" target="_blank">See here how you can do that !</a> Let me know on the same topic if it worked fine for you or not.
</div>

<?php
if (isset($taskId)) {
  // echo "<b>DEBUG</b> : click <a href='task_points.php?task_id=".$taskId."' target='_blank'>here to recalculate points for the task</a> (then refresh this page)"; 
  // include 'task_points.php';
}
?>

<?php if ($eventIsOpen /*is event open or finished*/) {
?>

<h2>Results</h2>

<div id="tabs">
  <ul>
    <li <?php if (!isset($_GET['task']) and !isset($_GET['show'])) echo "class=\"here\"";?>><a href="?event=<?php echo $event_id;?>">General</a></li>

<?php
      $qTask = "select * from pgsim_comp_tasks where competition = '".$event_id."' order by starts";
    
      $rTask = mysql_query($qTask);
      $taskCount=0;
      $taskId=0;
      while ($vTask = mysql_fetch_array($rTask)){
      $taskCount++;
?>
    <li <?php if ($_GET['task']==$taskCount) { echo "class=\"here\""; $taskId=$vTask['id'];$task_id=$taskId;} ?>><a href="?event=<?php echo $vTask['competition'];?>&task=<?php echo $taskCount;?>"><?php echo $vTask['task_name'];?></a></li>
<?php
      }
?>

    <li <?php if (isset($_GET['show'])) echo "class=\"here\"";?>><a href="?event=<?php echo $event_id;?>&show=stats">Event stats</a></li>

   </ul>
</div>
<div id="content">
<?php
   
if (isset($_GET['show'])) {
  echo "<table width='100%'><tr><td width='50%' align='center'><img src='event_stats.php?event_id=".$event_id."' /></td>";
  echo "<td width='50%' align='center'><img src='event_stats_completed.php?event_id=".$event_id."' /></td></tr></table>";
} else {
   
   if (isset($_GET['task'])) {
         echo "<table width='100%'><tr><td valign='top' width='650px'>";
         $qResult = "select pgsim_comp_results.id as id, igc, igc_ajax, kml, date, pseudo, distance, flight_duration, task_id, browser, pilot_id, iso, nom_pays_en, first_try_score as final_score
 from pgsim_comp_results left join auteur on pilot_id = id_auteur left join pays on pays_auteur = id_pays 
 where task_id = $taskId order by first_try_score desc";
         $qTask = "SELECT expires, is_pge_challenge FROM pgsim_comp_tasks WHERE id = ".$taskId;
         $rTask = mysql_query($qTask);
         $vTask = mysql_fetch_array($rTask);
   }
   else {
         $qResult = "select pgsim_comp_results.id as id, pseudo, iso, nom_pays_en, sum(first_try_score) as final_score, pilot_id from pgsim_comp_results
 left join auteur on pilot_id = id_auteur 
 left join pgsim_comp_tasks on task_id = pgsim_comp_tasks.id
 left join pays on pays_auteur = id_pays 
 where pgsim_comp_tasks.competition = '$event_id' 
 group by pilot_id 
 order by final_score desc";
   }
  // echo $qTask;
  $rResult = mysql_query($qResult);
  $nResult = mysql_num_rows($rResult);
  if (isset($_GET['task'])) {
        // $taskId = $vTask['task_id']; include 'task_points.php';
        if ( time() > strtotime($vTask['expires']) )  echo "<p><b>Closed task : definitive results</b></p>";
        else echo "<p><b>Results are temporary since the task is not over yet.</b></p>";
        echo "<table style='text-align:center'><tr><td>&nbsp;Rank&nbsp;</td><td>&nbsp;Pilot&nbsp;</td>
        <td>&nbsp;Points&nbsp;</td><td>&nbsp;Distance&nbsp;</td>
        <td>&nbsp;Time (min.)&nbsp;</td><td>&nbsp;View flight&nbsp;</td>
        <td>&nbsp;Browser&nbsp;</td></tr>";
     } else {

        $qEvent = "select * from pgsim_comp_events where id = $event_id";
        $rEvent = mysql_query($qEvent);
        $vEvent = mysql_fetch_array($rEvent);

        echo "<table style='text-align:center;border-style:solid; border-width:1px;border-color:#aaa #aaa #fff #fff; border-collapse:collapse;'>
        <tr><td>&nbsp;&nbsp;&nbsp;Rank&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;Pilot&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;Score&nbsp;&nbsp;&nbsp;</td>";
        $howManyTasks =$vEvent['how_many_tasks'];
        for ($i=1 ; $i <=  $howManyTasks; $i++){
        echo "<td>&nbsp;&nbsp;&nbsp;Task $i&nbsp;&nbsp;&nbsp;</td>";
      }
      echo "</tr>";
    }

   $rank = 0 ;
 
   while ($vResult=mysql_fetch_array($rResult)){
    $rank++;
    if (isset($_GET['task'])) {

        if ($vResult['distance']==999999) $flight_distance = "completed task";
        else $flight_distance = round($vResult['distance']/10)/100 . " km";

        $task_status = "coming";
        if ( time() > strtotime($vTask['starts']) ) $task_status = "opened";
        if ( time() > strtotime($vTask['expires'])) $task_status = "closed"; 


        echo "
        <tr><td>".$rank."</td><td>".$vResult['pseudo']." <img src='../pgearth/images/drapeaux/_tn_".strtolower($vResult['iso']).".png' title='".$vResult['nom_pays_en']."' /></td><td>".$vResult['final_score']."</td><td>".$flight_distance."</td><td>". round($vResult['flight_duration']/6)/10 ."</td>";

        if ($vResult['igc'] == "") {
             if (strtotime($vResult['date']) > strtotime("now")-60) echo "<td><img src='../pgearth/images/famfamfamicons/flight.png' title='flying now' />";
             else { echo "<td>";
               if ($vResult['pilot_id']==$_SESSION['id_membre'] and $task_status=="opened") {
                  if ($task_id==512) echo "<a href='continue_task.php?result_id=".$vResult['id']."' >";
                  else echo "<a href='continue_task.php?event=".$event_id."&task=".$_GET['task']."' >";
               }
               echo "<img src='../pgearth/images/famfamfamicons/cross.png' border='0' />";
               if ($vResult['pilot_id']==$_SESSION['id_membre'] and $task_status=="opened") 
                        echo " - resume flight</a>";
             }  
        }
        else {
            if (time() < strtotime($vTask['expires']) and ($task_id<>512 or task_id<>612))  {
              echo "<td> <img src='../pgearth/images/famfamfamicons/time.png' title='flight is visible when the task is over'/> ";
            }
            else { 
               echo "<td> <a href='visugps.php?flight_id=".$vResult['id']."' rel='moodalbox 602 402' title='View flight'><img src='../pgearth/images/famfamfamicons/map.png' border='0'></a>";
               echo " - <a href='http://www.paraglidingearth.com/pgSimCompEvent/igc_result.php?flight_id=".$vResult['id']."'>igc</a>";
               echo " - <a href='http://cunimb.net/igc2kml.php?color=yellow&width=2&alti=gnss&link=http://www.paraglidingearth.com/pgSimCompEvent/igc_result.php?flight_id=".$vResult['id']."'>kml</a>";
            }
        }
               if ($_SESSION['id_membre']==1 and $task_status=="opened") echo " - <a  href='reset_try.php?result_id=".$vResult['id']."' target='_blank'><img src='../pgearth/images/famfamfamicons/bin.png' border='0' /></a>";
               if ($_SESSION['id_membre']==$vResult['pilot_id'] and $vResult['flight_duration']<600 and $task_status=="opened")  { 
                  echo " - <a  href='reset_own_try.php?result_id=".$vResult['id']."' target='_blank'><img src='../pgearth/images/famfamfamicons/bin.png' border='0' alt='cancel your flight to give another try on the task'/></a>";
               }
               if ($_SESSION['id_membre']==1 and $vResult['flight_duration']<600 and $task_status=="opened")  { 
                  echo " - <a  href='mail_warning.php?result_id=".$vResult['id']."' target='_blank'><img src='../pgearth/images/famfamfamicons/email.png' border='0' alt='send a mail to pilot so he can reset his flight'/></a>";
               }
               if ($_SESSION['id_membre']==1 and $task_status=="opened")  { 
					if ($vResult['igc']<>"")
						echo " - <a href='visugps_admin.php?source=igc&flight_id=".$vResult['id']."' rel='moodalbox 602 402' title='View flight' >admin igc</a>";
					if ($vResult['igc_ajax']<>"")
						echo " - <a href='visugps_admin.php?source=ajax&flight_id=".$vResult['id']."' rel='moodalbox 602 402' title='View flight' >admin igc ajax</a>";
		  else 
			  echo " - no igc";
               }
        echo "</td>";
        echo "<td>".$vResult['browser']."</td></tr>";

    } else {        

          echo "<tr style='border-style : solid; border-width:1px; border-color : #aaa #aaa #fff #fff;'><td>".$rank."</td><td>".$vResult['pseudo']." <img src='../pgearth/images/drapeaux/_tn_".strtolower($vResult['iso']).".png' title='".$vResult['nom_pays_en']."' /></td>
          <td>".$vResult['final_score']."</td>";
          for ($i=1; $i <= $howManyTasks; $i++){

            $qResultTasks = "select first_try_score, is_pge_challenge
                              from pgsim_comp_results 
                              left join pgsim_comp_tasks on task_id=pgsim_comp_tasks.id 
                              where pgsim_comp_tasks.competition = '".$event_id."' and pilot_id = ".$vResult['pilot_id']." and is_pge_challenge = $i";
            // echo $qResultTasks;
            $rResultTasks = mysql_query($qResultTasks);
            $vResultTasks=mysql_fetch_array($rResultTasks);
            echo "<td>".$vResultTasks['first_try_score']."</td>";
           }
          echo "</tr>";
    }
   }
    echo "</table>";

  if ($nResult==0) echo "<p><b>No results available.</b></p>";

}

if (isset($_GET['task'])) {
?>
  </td>
  <td valign="top" align="left" width="400">
  <div class="rubriqueSite">
  <div class="titreMenu">Comments on the task :</div>
  <?php
  if (isset($_SESSION['id_membre'])) {
    $showForm = true ;
    $member_pseudo = $_SESSION['pseudo_membre'];
  } else  {
    $showForm= false ;
  }

  $aboutComment = " about the Task";
  $linkurl = "http://www.paraglidingearth.com/pgSimCompEvent/events.php?event=".$event_id."&task=".$_GET['task'];
  $root = $_SERVER['DOCUMENT_ROOT']."/scripts_php/phpComments/";

  include $root.'comments.php';
 
  echo "</div>
  </td></tr></table>";
}
?>
</div>
<?php
}

include "task_points.php";
?>


<?php } /* end of if eventIsOpen ?? */?>

    </div>
</div>

</body>
</html>
