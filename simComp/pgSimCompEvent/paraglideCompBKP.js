// based on milktruck.js  -- Copyright Google 2007
function ajaxSaveNewTaskResultToDb(pilot_id, task_id, competition_id, elapsed_time, distance, browser, glider_id, igc_ajax) {        var req =  createAjaxInstance();	var data = "pilot_id=" + pilot_id + "&task_id=" + task_id + "&competition_id=" + competition_id + "&elapsed_time=" + elapsed_time				+ "&distance=" + distance + "&browser=" + browser + "&glider_id=" + glider_id + "&igc_ajax=" + igc_ajax ;	req.onreadystatechange = function()	{ 		if(req.readyState == 4)		{			if(req.status == 200)			{				MY_RESULT_ID = req.responseText;			}				else				{				alert("Error: returned status code " + req.status + " " + req.statusText);			}			}	}; 	req.open("POST", "ajax_save_result.php", true); 	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");	req.send(data); } 
function ajaxUpdateTaskResultToDb(pilot_id, task_id, result_id, elapsed_time, distance, lat, lng, alt, waypoint, igc_ajax) {        var req =  createAjaxInstance();	var data = "pilot_id=" + pilot_id + "&task_id=" + task_id + "&result_id=" + result_id + "&elapsed_time=" + elapsed_time				+ "&distance=" + distance + "&lat=" + lat + "&lng=" + lng + "&alt=" + alt + "&waypoint=" + waypoint + "&igc_ajax=" + igc_ajax ;	req.onreadystatechange = function()	{ 		if(req.readyState == 4)		{			if(req.status == 200)			{			//	if (req.responseText == 'good') var alert = 'toto' ;			}				else				{				alert("Error: returned status code " + req.status + " " + req.statusText);			}			}	}; 	req.open("POST", "ajax_update_result.php", true); 	req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");	req.send(data);         document.getElementById("jsInfo3").innerHTML = '';} 
// Code for Monster Milktruck demo, using Earth Plugin.truck = null;// Pull the Milktruck model from 3D Warehouse.var MODEL_URL = 'http://sketchup.google.com/3dwarehouse/download?mid='+GLIDER+'&rtyp=zip&fn=paraglider&ctyp=paraglider';//var MODEL_URL = 'http://sketchup.google.com/3dwarehouse/download?mid=3c9a1cac8c73c61b6284d71745f1efa9&rtyp=zip&fn=milktruck&ctyp=milktruck';var TICK_MS = 66;var STEER_ROLL = 7.0;var ROLL_SPRING = 0.14;var ROLL_DAMP = -0.06;var STEER_TILT = 3.0;var TILT_SPRING = 0.14;var TILT_DAMP = -0.03;var TAKEOFF_ALTITUDE = 1000;var THERMAL_DENSITY = 100*THERMAL_DENSITY;var I_CLOUDS = 0;var cloudModel = new Array(THERMAL_DENSITY);var cloudModelScale = new Array(THERMAL_DENSITY);var lineStringPlacemark = new Array(THERMAL_DENSITY);/*   we define here 2 lines (line1 and line2) which are :   - line 1 : passing at goal point and last turnpoint   - line 2 : passing at goal point, perpendicular to line1 = goalLine   lines are defined by equations coefficients ai and bi  : ' lat = ai*lng + bi ' with i=1 and i=2   we will then calculate distances to line2 to make goal    fyi : lastwaypoint-1 is goal, lastwaypoint-2 is last turnpoint before goal*/var A1 = 0;//var goalLineDirection = 90 + getBearing(WAYPOINT[LAST_WAYPOINT - 2], WAYPOINT[LAST_WAYPOINT - 1]);//if (goalLineDirection==90 || goalLineDirection==180 || goalLineDirection==270 || goalLineDirection==360) goalLineDirection = goalLineDirection+0.000001;//var goalLinePoint1 = Point_At_Distance_And_Bearing(WAYPOINT[LAST_WAYPOINT - 1][0], WAYPOINT[LAST_WAYPOINT - 1][1], 200, goalLineDirection);//var goalLinePoint2 = Point_At_Distance_And_Bearing(WAYPOINT[LAST_WAYPOINT - 1][0], WAYPOINT[LAST_WAYPOINT - 1][1], 200, goalLineDirection+180);//var A2 = (goalLinePoint1[0] - goalLinePoint2[0]) / (goalLinePoint1[1] - goalLinePoint2[1]);if ( WAYPOINT[LAST_WAYPOINT - 1][0] != WAYPOINT[LAST_WAYPOINT - 2][0] && WAYPOINT[LAST_WAYPOINT - 1][1] != WAYPOINT[LAST_WAYPOINT - 2][1] )A1 = (WAYPOINT[LAST_WAYPOINT - 1][0] - WAYPOINT[LAST_WAYPOINT - 2][0]) / (WAYPOINT[LAST_WAYPOINT - 1][1] - WAYPOINT[LAST_WAYPOINT - 2][1]);if (WAYPOINT[LAST_WAYPOINT - 1][0] == WAYPOINT[LAST_WAYPOINT - 2][0]) A1 = 0.0000001;if (WAYPOINT[LAST_WAYPOINT - 1][1] == WAYPOINT[LAST_WAYPOINT - 2][1]) A1 = 99999999;var A2 = -1/A1; //perpendicular to line1var B2 =  WAYPOINT[LAST_WAYPOINT - 1][0] - A2 * WAYPOINT[LAST_WAYPOINT - 1][1] ; //goal belongs to line2//alert('goal line : y = '+A2+'.x +'+B2);function Truck() {  var me = this;  me.doTick = true;    // We do all our motion relative to a local coordinate frame that is  // anchored not too far from us.  In this frame, the x axis points  // east, the y axis points north, and the z axis points straight up  // towards the sky.  //  // We periodically change the anchor point of this frame and  // recompute the local coordinates.  me.localAnchorLla = [0, 0, 0];  me.localAnchorCartesian = V3.latLonAltToCartesian(me.localAnchorLla);  me.localFrame = M33.identity();  // Position, in local cartesian coords.  me.pos = [0, 0, 0];  me.sideOfGoalLine = 0;    // Velocity, in local cartesian coords.  me.vel = [0, 0, 0];  // have I taken off once ?  me.haveLaunched = 0;  me.inFlight = 0;  // where am i looking ?  me.headHeading = 0;  me.headTilt = 80;  // task waypoints  me.actualWaypoint = 1;// settings if continuing a former flight  if (TEMP_DURATION > 0){    me.actualWaypoint = TEMP_WAYPOINT;    me.inFlight=0;    me.haveLaunched = 1;  }   me.lastBeep = (new Date()).getTime();  me.beepFrequency = 0;  // AJAX result storing to db  me.saveStartToDb = 0;  me.resultId = MY_RESULT_ID;  me.lastSaveToDB =  TEMP_DURATION;//  me.alert = "problem?";  // thermals and wind  me.wind=[0,0,0];  var windDirectionLocal = 90-WIND_DIRECTION ; // because x axis points east while wind direction is counted from north..  var windDirectionRadians = windDirectionLocal * Math.PI / 180;  me.windX = WIND_SPEED * Math.cos(windDirectionRadians);  me.windY = WIND_SPEED * Math.sin(windDirectionRadians);  me.flyingInThermal = 0;  me.thermalUpliftSpeed = 0;  //flight stats  me.maxDistanceFromTakeOff = 0;  me.minDistanceToNextWaypoint = LEG_LENGTH[1];  me.distanceInCurrentLeg = 0;  me.flightDistance = 0;  me.goalReached = 0;  me.maxAltitude = 0 ;  me.flightDurationMilliSec = TEMP_DURATION*1000;  //flightlog : stocks flight waypoints  var actualDate = new Date();  me.startFlightTime = actualDate.getTime()-1000*TEMP_DURATION;  me.flightDuration = TEMP_DURATION;  var today = actualDate.getDate() ;  if (today < 10){ var todayString = '0'+today; }  else { var todayString = ''+today; }  var actualMonth = actualDate.getMonth() ;  actualMonth = actualMonth + 1 ;  if ( actualMonth < 10){ var monthString = '0'+actualMonth; }  else { var monthString = ''+actualMonth; }   actualYear = actualDate.getFullYear()+'';  actualYear = actualYear.substr(2,4);  var igcDate = todayString+''+monthString+actualYear;  me.igc  = 'AXGD001 SP24XC  SW2.70\n';  me.igc += 'HFDTE'+ igcDate +'\n';  me.igc += 'HOPLTPILOT: '+ PILOT_NAME +'\n';  me.igc += 'HOGTYGLIDERTYPE: Paraglide\n';  me.igc += 'HODTM100GPSDATUM: WGS-84\n';  me.igc += 'HOCCLCOMPETITION CLASS: PGEPGSIM\n';  me.igc_ajax = me.igc;  //me.igc += 'HOSITSite: '+ SITE_NAME +'\n';  me.StopWrittingIGCOnceForAll = false;    var dateNow = new Date();    var latText='N';    var lngText='E';    var lat = TAKEOFF_LAT ;    var lng = TAKEOFF_LNG ;    if (lat<0) {latText='S'; lat = - lat}     if (lng<0) {lngText='W'; lng = - lng}    var lat = addExtraZeros(2, Math.floor(lat))+''+ addExtraZeros(5, Math.round( 60 * Math.round(100000 * (lat - Math.floor(lat))) / 100)); // convert decimal degrees to decimal minutes    var lng = addExtraZeros(3, Math.floor(lng))+''+ addExtraZeros(5, Math.round( 60 * Math.round(100000 * (lng - Math.floor(lng))) / 100)); // convert decimal degrees to decimal minutes    me.igc += 'B'+ addExtraZeros(2,dateNow.getHours()) +''+addExtraZeros(2,dateNow.getMinutes())+''+addExtraZeros(2,dateNow.getSeconds());    me.igc += lat+''+latText;    me.igc += lng+''+lngText;    me.igc += 'A'+addExtraZeros(10, Math.round(ge.getGlobe().getGroundAltitude(TAKEOFF_LAT, TAKEOFF_LNG)))+'\n';    if (TEMP_ALT>0) me.igc += 'A'+addExtraZeros(10, Math.round(TEMP_ALT))+'\n';  me.oneOutOfHundred = 0;  // Orientation matrix, transforming model-relative coords into local  // coords.  me.modelFrame = M33.identity();  me.roll = 0;  me.rollSpeed = 0;    me.tilt = 0;  me.tiltSpeed = 0;    me.idleTimer = 0;  me.fastTimer = 0;  me.popupTimer = 0;  me.camZoom = 1;//  ge.getOptions().setFlyToSpeed(100);  // don't filter camera motion  google.earth.fetchKml(ge, MODEL_URL,                               function(obj) { me.finishInit(obj); });}Truck.prototype.finishInit = function(kml) {  var me = this;  // The model zip file is actually a kmz, containing a KmlFolder with  // a camera KmlPlacemark (we don't care) and a model KmlPlacemark  // (our milktruck).  me.placemark = kml.getFeatures().getChildNodes().item(1);  me.model = me.placemark.getGeometry();  me.orientation = me.model.getOrientation();  me.location = me.model.getLocation();  me.model.setAltitudeMode(ge.ALTITUDE_ABSOLUTE);  me.orientation.setHeading(90);  me.model.setOrientation(me.orientation);  ge.getFeatures().appendChild(me.placemark);  me.balloon = ge.createHtmlStringBalloon('');  me.balloon.setFeature(me.placemark);  me.balloon.setMaxWidth(200);  // window.setTimeout(alert('please be patient... :('), 5000);  me.teleportTo(TAKEOFF_LAT, TAKEOFF_LNG, TAKEOFF_HEADING);  me.lastMillis = (new Date()).getTime();  me.lastThermalSetUpPos = [0, 0, 0];  me.thermalSpeed = new Array(THERMAL_DENSITY);  me.thermalCenter = new Array(THERMAL_DENSITY);  me.thermalCenterLla = new Array(THERMAL_DENSITY);  me.thermalMidAirCenter = new Array(THERMAL_DENSITY);  me.thermalMidAirCenterLla = new Array(THERMAL_DENSITY);  me.thermalTopCenter = new Array(THERMAL_DENSITY);  me.thermalTopCenterLla = new Array(THERMAL_DENSITY);  me.nearbyThermalsId = new Array(THERMAL_DENSITY);  me.nearbyThermalsSearch = false;  me.nearbyThermalsCount = 0;  for(var i=0; i < THERMAL_DENSITY; i++ ){// document.getElementById("jsInfo3").innerHTML = i+'/'+THERMAL_DENSITY;     /////  SET Thermals around takeoff  ///       var speed = THERMAL_VZ_MAX * (0.8 + (0.4*Math.random()));       // set the offset between paraglide and thermal ground positions       // due to drifting thermal (if wind blows from north, offset the thermals centers to the north of the wing so the glider       // is surrounded by thermals at its altitude)       var offsetDistanceX = TAKEOFF_ALTITUDE * me.windX / speed ;       var offsetDistanceY = TAKEOFF_ALTITUDE * me.windY / speed ;       var topOffsetDistanceX =   THERMAL_TOP * me.windX / speed ;       var topOffsetDistanceY =   THERMAL_TOP * me.windY / speed ;       var randX = offsetDistanceX + 20000*(Math.random()-0.5);       var randY = offsetDistanceY + 20000*(Math.random()-0.5);       me.thermalSpeed[i] = speed;       me.thermalCenter[i] = [randX, randY, 0];       me.thermalCenterLla[i] = V3.cartesianToLatLonAlt(V3.add(me.localAnchorCartesian, M33.transform(me.localFrame, me.thermalCenter[i])));       me.thermalTopCenter[i] = [randX - topOffsetDistanceX, randY - topOffsetDistanceY, 0];       me.thermalTopCenterLla[i] = V3.cartesianToLatLonAlt(V3.add(me.localAnchorCartesian, M33.transform(me.localFrame, me.thermalTopCenter[i])));       // Create the line for the thermal placemark       lineStringPlacemark[i] = ge.createPlacemark('');       // Create the LineString       var lineString = ge.createLineString('');       lineStringPlacemark[i].setGeometry(lineString);       lineString.setAltitudeMode(ge.ALTITUDE_ABSOLUTE);       lineString.getCoordinates().pushLatLngAlt(me.thermalCenterLla[i][0],   me.thermalCenterLla[i][1],   0);       lineString.getCoordinates().pushLatLngAlt(me.thermalTopCenterLla[i][0],   me.thermalTopCenterLla[i][1],  THERMAL_TOP);       // Create a style and set width and color of line       lineStringPlacemark[i].setStyleSelector(ge.createStyle(''));       var lineStyle = lineStringPlacemark[i].getStyleSelector().getLineStyle();       lineStyle.setWidth(5);       lineStyle.getColor().set('00ff0000');  // aabbggrr format       // Add the feature to Earth       ge.getFeatures().appendChild(lineStringPlacemark[i]);       // Put the clouds       google.earth.fetchKml(ge, 'http://www.paraglidingearth.com/pgSimCompEvent/3dModels/cumulus.zip',                             function(object) {                                    cloudPlacemark = object.getFeatures().getChildNodes().item(1);                                    cloudModel[I_CLOUDS] = cloudPlacemark.getGeometry();                                    cloudModel[I_CLOUDS].setAltitudeMode(ge.ALTITUDE_ABSOLUTE);                                    cloudModel[I_CLOUDS].getLocation().setLatLngAlt(me.thermalTopCenterLla[I_CLOUDS][0], me.thermalTopCenterLla[I_CLOUDS][1], THERMAL_TOP);                                    cloudModel[I_CLOUDS].getOrientation().setHeading(Math.round(180*Math.random()));                                    // cloudModel[I_CLOUDS].getOrientation().setTilt(180);                                    cloudModelScale[I_CLOUDS] = 2 + 3*(me.thermalSpeed[I_CLOUDS]-THERMAL_VZ_MAX)/THERMAL_VZ_MAX ;                                    cloudModel[I_CLOUDS].getScale().set(cloudModelScale[I_CLOUDS],                                                                        cloudModelScale[I_CLOUDS],                                                                        cloudModelScale[I_CLOUDS]);                                     ge.getFeatures().appendChild(object);                                    I_CLOUDS = I_CLOUDS+1;document.getElementById("loading_text").innerHTML = '<b>Loading...<br />Heating ground to make thermals : ' + Math.round( 100* I_CLOUDS / THERMAL_DENSITY ) + ' %</b>' ;                               }       );    }// alert('here');  var href = window.location.href;  var pagePath = href.substring(0, href.lastIndexOf('/')) + '/';  me.shadow = ge.createGroundOverlay('');  me.shadow.setVisibility(false);  me.shadow.setIcon(ge.createIcon(''));  me.shadow.setLatLonBox(ge.createLatLonBox(''));  me.shadow.setAltitudeMode(ge.ALTITUDE_CLAMP_TO_GROUND);  me.shadow.getIcon().setHref(pagePath + 'img/shadowrect.png');  me.shadow.setVisibility(true);  ge.getFeatures().appendChild(me.shadow);//=======================  GOAL GROUNDOVERLAY   ==========================var goalGroundOverlay = ge.createGroundOverlay('');goalGroundOverlay.setIcon(ge.createIcon(''))goalGroundOverlay.getIcon().setHref('');goalGroundOverlay.setLatLonBox(ge.createLatLonBox(''));var northGoalArray = Point_At_Distance_And_Bearing(WAYPOINT[LAST_WAYPOINT - 1][0], WAYPOINT[LAST_WAYPOINT - 1][1], 200, 0);    me.northGoal = northGoalArray[0];var southGoalArray = Point_At_Distance_And_Bearing(WAYPOINT[LAST_WAYPOINT - 1][0], WAYPOINT[LAST_WAYPOINT - 1][1], 200, 180);    me.southGoal = southGoalArray[0];var eastGoalArray  = Point_At_Distance_And_Bearing(WAYPOINT[LAST_WAYPOINT - 1][0], WAYPOINT[LAST_WAYPOINT - 1][1], 5, 90);    me.eastGoal = eastGoalArray[1];var westGoalArray  = Point_At_Distance_And_Bearing(WAYPOINT[LAST_WAYPOINT - 1][0], WAYPOINT[LAST_WAYPOINT - 1][1], 5, 270);    me.westGoal = westGoalArray[1];    me.rotationGoal = fixAngle2(90 - getBearingDouble(WAYPOINT[LAST_WAYPOINT - 2], WAYPOINT[LAST_WAYPOINT - 1]));    me.rotationGoal2 = 90+(180 * Math.atan(A2) / Math.PI); //   alert('Rot = '+me.rotationGoal+' || '+me.rotationGoal2);var latLonBoxGoal = goalGroundOverlay.getLatLonBox();latLonBoxGoal.setBox(me.northGoal, me.southGoal, me.eastGoal, me.westGoal, me.rotationGoal2);ge.getFeatures().appendChild(goalGroundOverlay);//=======================  GOAL GROUNDOVERLAY   ==========================var goalLineDirection = fixAngle2(90 + getBearingDouble(WAYPOINT[LAST_WAYPOINT - 2], WAYPOINT[LAST_WAYPOINT - 1]));if (goalLineDirection==90 || goalLineDirection==180 || goalLineDirection==270 || goalLineDirection==360) goalLineDirection = goalLineDirection+0.000001;//alert('goal line : '+goalLineDirection);var goalLinePoint1 = Point_At_Distance_And_Bearing(WAYPOINT[LAST_WAYPOINT - 1][0], WAYPOINT[LAST_WAYPOINT - 1][1], 200, goalLineDirection);var goalLinePoint2 = Point_At_Distance_And_Bearing(WAYPOINT[LAST_WAYPOINT - 1][0], WAYPOINT[LAST_WAYPOINT - 1][1], 200, fixAngle2(goalLineDirection+180));me.A2 = (goalLinePoint1[0] - goalLinePoint2[0]) / (goalLinePoint1[1] - goalLinePoint2[1]);me.B2 =  goalLinePoint1[0] - A2 * goalLinePoint1[1] ; //goalLinePoint1 belongs to line//alert('me.A2 = '+me.A2+'; me.B2='+me.B2);  me.takeOffAltitude = TAKEOFF_ALTITUDE;  if (TEMP_ALT>0) me.takeOffAltitude = TEMP_ALT; // changeOpacity(30, 'totot');   google.earth.addEventListener(ge, "frameend", function() { me.tick(); });  me.cameraCut();}function placeCloud(object, lat, lng) {        cloudPlacemark = object.getFeatures().getChildNodes().item(1);        cloudModel[I_CLOUDS] = cloudPlacemark.getGeometry();        cloudModel[I_CLOUDS].setAltitudeMode(ge.ALTITUDE_ABSOLUTE);        cloudModel[I_CLOUDS].getLocation().setLatLngAlt(lat, lng, THERMAL_TOP);        ge.getFeatures().appendChild(object);        I_CLOUDS = I_CLOUDS+1;}   leftButtonDown = false;hardLeftButtonDown = false;rightButtonDown = false;hardRightButtonDown = false;gasButtonDown = false;reverseButtonDown = false;takeoffButtonDown = false;turnHeadLeft = false ;turnHeadRight = false ;turnHeadBack = false ;turnHeadDown = false ;turnHeadUp = false ;cameraZoomIn = false ;cameraZoomOut = false ;varioSoundOnOff = true;pausedGame = false;if (TEMP_DURATION > 0) pausedGame = true;bStall = false;rescue = false;showThermals = false;shiftKey = false;resetPointOfView=false;function switchTrueFalse(stuff){  if (stuff) {    stuff=false  } else {    stuff=true  }  return stuff;}function showHideThermals(showThermals){  if (!showThermals) {   //if thermals are hidden (showThermals is false)    for (var t=0; t < THERMAL_DENSITY; t++){       var lineStyle = lineStringPlacemark[t].getStyleSelector().getLineStyle();       lineStyle.getColor().set('44ff0000');  // aabbggrr format       showThermals = true;       //    }  } else {    for (var t=0; t < THERMAL_DENSITY; t++){       var lineStyle = lineStringPlacemark[t].getStyleSelector().getLineStyle();       lineStyle.getColor().set('00ff0000');  // aabbggrr format       showThermals = false;       //    }  }  return showThermals;}function keyDown(event) {  if (!event) {    event = window.event;  }  if (event.keyCode == 37) {  // Left.    leftButtonDown = true;    event.returnValue = false;  } else if (event.keyCode == 39) {  // Right.    rightButtonDown = true;    event.returnValue = false;  } else if (event.keyCode == 38) {  // Up.    gasButtonDown = true;    event.returnValue = false;  } else if (event.keyCode == 40) {  // Down.    reverseButtonDown = true;    event.returnValue = false;  } else if (event.keyCode == 32) {  // Space Bar.    resetPointOfView = true;    event.returnValue = false;  } else if (event.keyCode == 83) {  // s.    turnHeadLeft = true;    event.returnValue = false;  } else if (event.keyCode == 68) {  // d.    turnHeadRight = true;    event.returnValue = false;  } else if (event.keyCode == 67) {  // c.    turnHeadBack = true;    event.returnValue = false;  } else if (event.keyCode == 88) {  // x.    turnHeadDown = true;    event.returnValue = false;  } else if (event.keyCode == 69) {  // e.    turnHeadUp = true;    event.returnValue = false;  } else if (event.keyCode == 65) {  // a.    cameraZoomIn = true;    event.returnValue = false;  } else if (event.keyCode == 90) {  // z.    cameraZoomOut = true;    event.returnValue = false;  } else if (event.keyCode == 86) {  // v.    varioSoundOnOff = switchTrueFalse(varioSoundOnOff);    event.returnValue = false;  } else if (event.keyCode == 80) {  // p.    pausedGame = switchTrueFalse(pausedGame);    event.returnValue = false;  } else if (event.keyCode == 84) {  // t.//    showThermals = showHideThermals(showThermals);    event.returnValue = false;  } else if (event.keyCode == 82) {  // r.    if (confirm("Are you sure you want to throw your rescue ??"))  { rescue = true; }    event.returnValue = false;  } else if (event.keyCode == 66) {   // b.    bStall = true;    event.returnValue = false;  } else if (event.keyCode == 16) {  // shift.    shiftKey = true;    event.returnValue = false;  }  else {    return true;  }  return false;}function keyUp(event) {  if (!event) {    event = window.event;  }  if (event.keyCode == 37) {  // Left.    leftButtonDown = false;    event.returnValue = false;  } else if (event.keyCode == 39) {  // Right.    rightButtonDown = false;    event.returnValue = false;  } else if (event.keyCode == 38) {  // Up.    gasButtonDown = false;    event.returnValue = false;  } else if (event.keyCode == 40) {  // Down.    reverseButtonDown = false;    event.returnValue = false;  } else if (event.keyCode == 32) {  // Space Bar.    resetPointOfView = false;    event.returnValue = false;  } else if (event.keyCode == 83) {  // s.    turnHeadLeft = false;    event.returnValue = false;  } else if (event.keyCode == 68) {  // d.    turnHeadRight = false;    event.returnValue = false;  } else if (event.keyCode == 88) {  // x.    turnHeadDown = false;    event.returnValue = false;  } else if (event.keyCode == 69) {  // e.    turnHeadUp = false;    event.returnValue = false;  } else if (event.keyCode == 65) {  // a.    cameraZoomIn = false;    event.returnValue = false;  } else if (event.keyCode == 90) {  // z.    cameraZoomOut = false;    event.returnValue = false;  } else if (event.keyCode == 67) {  // c.    turnHeadBack = false;    event.returnValue = false;  } else if (event.keyCode == 66) { // b.    bStall = false;    event.returnValue = false;  } else if (event.keyCode == 16) {  // shift.    shiftKey = false;    event.returnValue = false;  }  return false;}function clamp(val, min, max) {  if (val < min) {    return min;  } else if (val > max) {    return max;  }  return val;}
Truck.prototype.tick = function() {
 var me = this;

  if (I_CLOUDS == THERMAL_DENSITY & TEMP_DURATION==0 & me.inFlight==0) {
    document.getElementById("loading").style.display = 'none';
    document.getElementById("takeOffButton").style.display = 'block';
  }

  if (I_CLOUDS == THERMAL_DENSITY & me.inFlight==1) {
    document.getElementById("loading").style.display = 'none';
    document.getElementById("pilotButtons").style.display = 'block';
  }

  var now = (new Date()).getTime();

  // dt is the delta-time since last tick, in seconds
  var dt = (now - me.lastMillis) / 1000.0;
  if (dt > 0.25) {
    dt = 0.25;
  }

  if (pausedGame || ! me.inFlight) {
     me.flightDurationMilliSec =  me.flightDurationMilliSec ;
     me.flightDuration =  me.flightDuration ;
  } else {
     me.flightDurationMilliSec =  me.flightDurationMilliSec + now - me.lastMillis;
     me.flightDuration =  me.flightDuration + dt;
  }
  
  me.lastMillis = now;


// save the flight as an attempt at takeoff button
if (me.inFlight==1 && !me.saveStartToDb && TEMP_DURATION==0){
   ajaxSaveNewTaskResultToDb(PILOT_ID, TASK_ID, COMPETITION_ID, 0, 0, BROWSER, GLIDER_ID, me.igc_ajax);
   me.saveStartToDb = 1;
   me.lastSaveToDB =  0;
   alert("In order to prevent cheating.... ;)\nyour try on this task has been recorded\nyou will not be able to make\nanother flight later on the task.\nGood flight and good luck!\nid=" + MY_RESULT_ID +":)");
//   alert("If you are not 10 meters above ground in 10 seconds,\nyour flight will be ended\nas a cheating attempt to walk and fly");
   me.startFlightTime = (new Date()).getTime();
}


// save the flight as an attempt after 1 minutes = 60 sec
if (me.flightDuration > 60+me.lastSaveToDB && MY_RESULT_ID > 0){
   var startSavingTime = me.flightDuration;//   document.getElementById("jsInfo3").innerHTML = 'Loading your position...';   var actualTotalDistance = me.flightDistance + me.distanceInCurrentLeg;//   alert("1 minutes since last save;\nTime to upadte result to db\nid : " + MY_RESULT_ID + "\ndistance : " + actualTotalDistance + " :)");   ajaxUpdateTaskResultToDb(PILOT_ID, TASK_ID, MY_RESULT_ID, me.flightDuration, actualTotalDistance, me.lla[0], me.lla[1], me.lla[2], me.actualWaypoint );   me.lastSaveToDB = me.flightDuration;//   document.getElementById("jsInfo3").innerHTML = '';//   alert ('startSavingTime = '+startSavingTime+'\nme.flightDuration = '+me.flightDuration+'\nok ?');}// after 10 secs, check if airbornif (me.flightDuration > 20 && !me.inFlight && TEMP_DURATION==0){//alert('flight end 1');   me.endFlight();}// document.getElementById("jsInfo").innerHTML = 'thermals : ';  var c0 = 1;  var c1 = 0;  var gpos = V3.add(me.localAnchorCartesian,                    M33.transform(me.localFrame, me.pos));  var lla = V3.cartesianToLatLonAlt(gpos);  me.lla=lla;  // am i flying in a thermal ????  if (me.flyingInThermal == 0 & me.lla[2] < THERMAL_TOP & !pausedGame){    for(var j=0; j < me.nearbyThermalsCount; j++){//document.getElementById("jsInfo3").innerHTML = 'watching '+ me.nearbyThermalsCount +' thermals';	me.thermalMidAirCenterLla[j] = [ me.thermalCenterLla[me.nearbyThermalsId[j]][0] + me.lla[2] * (me.thermalTopCenterLla[me.nearbyThermalsId[j]][0]-me.thermalCenterLla[me.nearbyThermalsId[j]][0]) / THERMAL_TOP,
	                                 me.thermalCenterLla[me.nearbyThermalsId[j]][1] + me.lla[2] * (me.thermalTopCenterLla[me.nearbyThermalsId[j]][1]-me.thermalCenterLla[me.nearbyThermalsId[j]][1]) / THERMAL_TOP,
	                                 me.lla[2] ];
       var distanceToCenter = getDistance(me.lla, me.thermalMidAirCenterLla[j]);
      if (distanceToCenter  < THERMAL_RADIUS){
        me.myActualThermalSpeed =  me.thermalSpeed[me.nearbyThermalsId[j]];
        me.myActualThermalCenter =  me.thermalCenter[me.nearbyThermalsId[j]];
        me.myActualThermalCenterLla =  me.thermalCenterLla[me.nearbyThermalsId[j]];
        me.myActualThermalTopCenter =  me.thermalTopCenter[me.nearbyThermalsId[j]];
        me.myActualThermalTopCenterLla =  me.thermalTopCenterLla[me.nearbyThermalsId[j]];
        me.inThermal();
      }
    }
  } else {
     if (!pausedGame & me.lla[2] < THERMAL_TOP) me.inThermal();
  }

  if (V3.length([me.pos[0], me.pos[1], 0]) > 500) {
    //  Re-anchor our local coordinate frame whenever we've strayed a bit away from it.
    //  This is necessary because the earth is not flat!
    //  This is also the moment where we will spot which thermals are clse around,
    //  we will also move the faraway thermals closer to us
    me.spotNearbyThermals();
    me.bringBackThermals();
    me.adjustAnchor();
  }

  if (!me.nearbyThermalsSearch) me.spotNearbyThermals();  // at take off, first searh for the surroundongs thermals

  // COMPETITON TASK : where is the next waypoint ?
  me.distanceToNextWaypoint = getDistance(me.lla, [ WAYPOINT[me.actualWaypoint][0], WAYPOINT[me.actualWaypoint][1], 0]);
//  me.flightDuration = (now - me.startFlightTime)/1000;
  if (me.distanceToNextWaypoint < me.minDistanceToNextWaypoint) {
	me.minDistanceToNextWaypoint = me.distanceToNextWaypoint;
	me.distanceInCurrentLeg = LEG_LENGTH[me.actualWaypoint] - me.minDistanceToNextWaypoint;
  }	
  me.headingToNextWaypoint  = getBearing(me.lla, [ WAYPOINT[me.actualWaypoint][0], WAYPOINT[me.actualWaypoint][1], 0]);

  // display info to fly to next waypoint
  if (me.inFlight){
      var durationMinutes = Math.floor(me.flightDuration / 60);
      var durationSeconds = Math.round(me.flightDuration - (durationMinutes*60));
      document.getElementById("waypoint").innerHTML  = 'flightTime : '+durationMinutes+'min.'+durationSeconds+'sec.';
  } else {
	me.flightDuration = TEMP_DURATION;
	document.getElementById("waypoint").innerHTML  = 'flightTime : '+me.flightDuration+' sec. <br />Hit <em>"Run !"</em> button to take off';
  }


  //You made it to the (not last) waypoint : let's fly to the next one
  if  ( me.actualWaypoint < LAST_WAYPOINT-1 && me.distanceToNextWaypoint < WAYPOINT_RADIUS ) {
      me.flightDistance = me.flightDistance + LEG_LENGTH[me.actualWaypoint];
      me.actualWaypoint++;
      me.distanceInCurrentLeg = 0;
      me.minDistanceToNextWaypoint = getDistance(me.lla, [ WAYPOINT[me.actualWaypoint][0], WAYPOINT[me.actualWaypoint][1], 0]);
      if ( me.actualWaypoint == LAST_WAYPOINT-1 ) alert('Warning : goal system has changed !!\nYou must now fly above the goal point (or cross the goal line)');
  }

  //--- goal reach detection special test
  if ( me.actualWaypoint == LAST_WAYPOINT-1 ) {
// document.getElementById("jsInfo3").innerHTML = 'to goal !';
    var formerSideOfGoalLine = me.sideOfGoalLine;
    me.sideOfGoalLine = A2 * me.lla[1] + B2 - me.lla[0];
// document.getElementById("jsInfo3").innerHTML = 'side of goal line = '+me.sideOfGoalLine;
    if(formerSideOfGoalLine*me.sideOfGoalLine<0 && getDistance(me.lla, WAYPOINT[LAST_WAYPOINT-1])<200) {
      me.flightDistance = me.flightDistance + LEG_LENGTH[me.actualWaypoint];
      me.actualWaypoint++;
      me.goalReached = 1;
      me.endFlight();
    }
  }
  
  
  
  var dir = me.modelFrame[1];
  var up = me.modelFrame[2];

  var absSpeed = V3.length(me.vel);

  var groundAlt = ge.getGlobe().getGroundAltitude(lla[0], lla[1]);
  var airborne = (groundAlt + 0.30 < me.pos[2]);
  var steerAngle = 0;

  var normal = estimateGroundNormal(gpos, me.localFrame);

  // Steering.
  if (pausedGame) {
     var turnSpeed = 0;
     steerAngle = 0;
  } else {

   if ((leftButtonDown || rightButtonDown || hardLeftButtonDown || hardRightButtonDown ) & me.inFlight==1) {
      var TURN_SPEED_MIN = 54.0;  // radians/sec
      var TURN_SPEED_MAX = 120.0;  // radians/sec
  
      var turnSpeed;
 
      var SPEED_MAX_TURN = 25.0;
      var SPEED_MIN_TURN = 120.0;
      if (hardLeftButtonDown || hardRightButtonDown) {
        turnSpeed = TURN_SPEED_MAX ;
        me.vel[2] = -8 ;
      } else {
        turnSpeed = TURN_SPEED_MIN;
      }
      if (leftButtonDown || hardLeftButtonDown) {
        steerAngle = turnSpeed * dt * Math.PI / 180.0;
      }
      if (rightButtonDown || hardRightButtonDown) {
        steerAngle = -turnSpeed * dt * Math.PI / 180.0;
      }
    } else {
      if (me.inFlight) {me.vel[2] = -1.3 ;}
    }
  } //end of "if paused game"

  // Turn.
  var newdir = V3.rotate(dir, up, steerAngle);
  me.modelFrame = M33.makeOrthonormalFrame(newdir, up);
  me.vel = V3.rotate(me.vel, up, steerAngle);
  dir = me.modelFrame[1];
  up = me.modelFrame[2];

  var forwardSpeed = 0;
  var verticalSpeed = 0;


  forwardSpeed = V3.dot(dir, me.vel);
  verticalSpeed = V3.dot(up, me.vel);

	    //alert('there1');
	
	// Vertical sink rate  
	if (me.inFlight & !(hardLeftButtonDown || hardRightButtonDown)) {
                if (bStall) {
                        me.vel = [0, 0, -6];
                }    
                if (me.vel[0] == 0 && !bStall) {
                        me.vel = V3.add(me.vel, V3.scale(dir, 10));
                }    
		if (forwardSpeed > WING_VX_VMINI & forwardSpeed <= WING_VX_VZMINI) {
			me.vel[2] = WING_VZ_VMINI + ((WING_VZ_VZMINI-WING_VZ_VMINI)/(WING_VX_VZMINI-WING_VX_VMINI))*(forwardSpeed -WING_VX_VMINI);
		}	
		if (forwardSpeed > WING_VX_VZMINI & forwardSpeed <= WING_VX_GLIDEMAX) {
			me.vel[2] = WING_VZ_VZMINI - ((WING_VZ_VZMINI-WING_VZ_GLIDEMAX)/(WING_VX_GLIDEMAX-WING_VX_VZMINI))*(forwardSpeed -WING_VX_VZMINI);
		}	
		if (forwardSpeed > WING_VX_GLIDEMAX) {
			me.vel[2] = WING_VZ_GLIDEMAX - ((WING_VZ_GLIDEMAX-WING_VZ_VMAX)/(WING_VX_VMAX-WING_VX_GLIDEMAX))*(forwardSpeed -WING_VX_GLIDEMAX);
		}
                if (rescue) {
                        me.vel = [0, 0, -6];
                }    	
	}

    // Apply engine/reverse accelerations.
    var ACCEL = 2.0;
    var DECEL = 2.0;
    var MAX_SPEED = WING_VX_VMAX;
    var MIN_SPEED = WING_VX_VMINI;

    forwardSpeed = V3.dot(dir, me.vel);
  if (me.inFlight && !pausedGame) {
    if (gasButtonDown & forwardSpeed < MAX_SPEED) {
      // Accelerate forwards.
      me.vel = V3.add(me.vel, V3.scale(dir, ACCEL * dt));
    } else if (reverseButtonDown & forwardSpeed > MIN_SPEED) {
      me.vel = V3.add(me.vel, V3.scale(dir, -DECEL * dt));
    }
  }
  

  //Evaluate local wind direction
  if (me.inFlight && !pausedGame) {
  gpos = V3.add(me.localAnchorCartesian,
                M33.transform(me.localFrame, me.pos));
  lla = V3.cartesianToLatLonAlt(gpos);     //get actual position as we need it to compute ridge effect depending on glider position
    me.windLocal(lla);
    //me.vel[2] = me.wind;
  } else {
    me.wind = [0, 0, 0];
  }


  // Move.
  if (pausedGame) {
     document.getElementById("takeOffButton").style.display = 'none';
     document.getElementById("jsInfo3").innerHTML = '<b>Paused game<br />\'p\' key to resume flying</b>';
     document.getElementById("jsInfo3").innerHTML += 'me.vel : ['+me.vel[0]+','+me.vel[1]+']';
     var deltaPos = [0,0,0];
  } else {
//     alert('toto?');
     var deltaPos = V3.scale(me.vel, dt);
     document.getElementById("jsInfo3").innerHTML = 'press \'p\' key to pause';
  } // end of if paused game 

  if (me.inFlight) {                  //if airborn, float with the wind
     airSpeed = V3.length(V3.add(me.vel, me.wind));
     deltaPos =  V3.add(deltaPos, V3.scale(me.wind, dt));
    if (me.inFlight & me.flyingInThermal==1 & !pausedGame){
      deltaPos =  V3.add(deltaPos, V3.scale([0, 0, me.thermalUpliftSpeed], dt));
    }
  }
  me.pos = V3.add(me.pos, deltaPos);
  gpos = V3.add(me.localAnchorCartesian,
                M33.transform(me.localFrame, me.pos));
  lla = V3.cartesianToLatLonAlt(gpos);


  // Don't go underground.
  groundAlt = ge.getGlobe().getGroundAltitude(lla[0], lla[1]);
  if (me.pos[2] < groundAlt) {
    me.pos[2] = groundAlt;
    me.vel = [0,0,0];
    if (me.inFlight == 1)  { alert ('flight end\nmy alt : '+me.pos[2]+'\nthe ground : '+groundAlt); me.endFlight();}
 //   else {WINCH_ALTITUDE = WINCH_ALTITUDE+100; me.teleportTo(me.lla[0],me.lla[1]);}
  }

  // Get a start for the flight to be able to end it later
  if (me.pos[2]-10 > groundAlt && me.haveLaunched==1 && me.inFlight==0) {
    me.inFlight = 1;
    document.getElementById("takeOffButton").style.display = 'none';
    document.getElementById("pilotButtons").style.display = 'block';
  }



   //Taking off
  if ((takeoffButtonDown  || TEMP_DURATION > 0) && me.haveLaunched==0 && I_CLOUDS==THERMAL_DENSITY){
    //TODO : re-orient glider to the max-slope direction + test if enough slope to launch
    me.vel = [10*dir[0],10*dir[1],0];
    me.takeOffAltitude = me.pos[2];
    me.maxAltitude = me.pos[2];
    me.haveLaunched = 1 ;
  }


   // Make our orientation always up.
   //me.modelFrame = M33.makeOrthonormalFrame(dir, up);


  // Propagate our state into Earth.
  gpos = V3.add(me.localAnchorCartesian,
                M33.transform(me.localFrame, me.pos));
  lla = V3.cartesianToLatLonAlt(gpos);
  me.model.getLocation().setLatLngAlt(lla[0], lla[1], lla[2]);
  var newhtr = M33.localOrientationMatrixToHeadingTiltRoll(me.modelFrame);

  if (me.oneOutOfHundred == 350){     if (!me.StopWrittingIGCOnceForAll && me.inFlight==1){       var dateNow = new Date();       var latText='N';       var lngText='E';       var lat = lla[0];       var lng = lla[1];       if (lla[0]<0) {latText='S'; lat = - lat}        if (lla[1]<0) {lngText='W'; lng = - lng}
       var lat = addExtraZeros(2, Math.floor(lat))+''+ addExtraZeros(5, Math.round( 60 * Math.round(100000 * (lat - Math.floor(lat))) / 100)); // convert decimal degrees to decimal minutes
       var lng = addExtraZeros(3, Math.floor(lng))+''+ addExtraZeros(5, Math.round( 60 * Math.round(100000 * (lng - Math.floor(lng))) / 100)); // convert decimal degrees to decimal minutes
       me.igc += 'B'+ addExtraZeros(2,dateNow.getHours()) +''+addExtraZeros(2,dateNow.getMinutes())+''+addExtraZeros(2,dateNow.getSeconds());
       me.igc += lat+''+latText;
       me.igc += lng+''+lngText;
       me.igc += 'A'+addExtraZeros(10, Math.round(lla[2]))+'\n';
    }

    me.oneOutOfHundred = 0;
    var distanceFromTakeOff = getDistance([me.lla[0], me.lla[1]],[TAKEOFF_LAT, TAKEOFF_LNG]);
    if (distanceFromTakeOff > me.maxDistanceFromTakeOff)   me.maxDistanceFromTakeOff = distanceFromTakeOff ;
    if (me.pos[2] > me.maxAltitude)   me.maxAltitude = me.pos[2] ;
  }

  // Compute roll and tilt according to steering.
  // TODO : tilt does not seem to work...
  var absRoll = newhtr[2];
  me.rollSpeed += steerAngle * forwardSpeed * STEER_ROLL;
  // Spring back to center, with damping.
  me.rollSpeed += (ROLL_SPRING * -me.roll + ROLL_DAMP * me.rollSpeed);
  me.roll += me.rollSpeed * dt;
  me.roll = clamp(me.roll, -90*turnSpeed/120, 90*turnSpeed/120);
  absRoll += me.roll;

  var absTilt = newhtr[1];
  me.tiltSpeed += Math.abs(steerAngle) * forwardSpeed * STEER_TILT;
  // Spring back to center, with damping.
  me.tiltSpeed += (TILT_SPRING * -me.tilt + TILT_DAMP * me.tiltSpeed);
  me.tilt += me.tiltSpeed * dt;
  me.tilt = clamp(me.tilt, -90*turnSpeed/120, 90*turnSpeed/120);
  absTilt += me.tilt;

 // absTilt =   absRoll * absRoll / 90 ;

  me.orientation.set(newhtr[0], absTilt, absRoll);

  var latLonBox = me.shadow.getLatLonBox();
  var radius = .00005;
  latLonBox.setNorth(lla[0] - radius);
  latLonBox.setSouth(lla[0] + radius);
  latLonBox.setEast(lla[1] - 4*radius);
  latLonBox.setWest(lla[1] + 4*radius);
  latLonBox.setRotation(-newhtr[0]);

  me.oneOutOfHundred = me.oneOutOfHundred + 1;

  
/*-----------------POINT OF VIEW--------------------------------------------*/ 
  
  var speedTurnHead = 5 ;

  if (resetPointOfView) {me.headHeading = 0; me.headTilt=80;}
  if (turnHeadLeft) me.headHeading = me.headHeading - speedTurnHead;
  if (turnHeadRight) me.headHeading = me.headHeading + speedTurnHead;
  if (turnHeadDown & me.headTilt > 2) me.headTilt = me.headTilt - speedTurnHead;
  if (turnHeadUp & me.headTilt < 175) me.headTilt = me.headTilt + speedTurnHead;
  me.cameraFollow(dt, gpos, me.localFrame, me.headHeading, me.headTilt);

/*---------------END OF POINT OF VIEW---------------------------------------*/ 


/*
  document.getElementById("jsInfo").innerHTML += '<br />flying ?  '+ me.inFlight +'';
  document.getElementById("jsInfo").innerHTML += '<br />x speed = '+ Math.round(100*me.vel[0])/100 +' m/s';
  document.getElementById("jsInfo").innerHTML += '<br />absx speed = '+ Math.round(100*absSpeed)/100 +' m/s';
  document.getElementById("jsInfo").innerHTML += '<br />y speed = '+ Math.round(100*me.vel[1])/100 +' m/s';
  document.getElementById("jsInfo").innerHTML += '<br />vertical air speed = '+Math.round(100*verticalSpeed)/100 +' m/s';
*/	


  //================ INSTRUMENTS  ============================
  var Vz  = Math.round(10*deltaPos[2]/dt)/10 ;
  var Vz1px=150;
  var Vz2px=1;

  if (Vz >= 0 & Vz <= 4){
    var Vz1px =   Math.round(150-20*Vz);
    var Vz2px =   Math.round(1+20*Vz);
  }
  if (Vz > 4 & Vz < 8){
    var Vz1px =   70;
    var Vz2px =   Math.round(81-20*(Vz-4));
  }
  if (Vz < 0 & Vz >= -4) {
    var Vz1px =   150;
    var Vz2px =   Math.round(1-20*Vz);
  }
  if (Vz < -4 & Vz > -8) {
    var Vz1px =   150-20*(Vz+4);
    var Vz2px =   81;
  }
  document.getElementById("varioBarSpacer").style.height = Vz1px+"px";
  document.getElementById("varioBar").style.height = Vz2px+"px";

  var temperature = Math.round(temperatureInit - 0.8*(me.pos[2]-me.takeOffAltitude)/100);
  document.getElementById("varioTemperature").innerHTML = "<font face='Comic Sans MS' color='#000000' size=5> "+Math.round(temperature)+"</font>";

  var heading = Math.round(180*Math.acos(deltaPos[1]/Math.pow(deltaPos[0]*deltaPos[0]+deltaPos[1]*deltaPos[1],0.5))/Math.PI);
  if (deltaPos[0] < 0) { heading = 360 - heading; }

  var wingHeading = me.model.getOrientation().getHeading();
  if (wingHeading < 0) { wingHeading = 360 + wingHeading; }
  var popo = 2*wingHeading + 110;
  //document.getElementById("compass_text").innerHTML = ""+wingHeading;
  document.getElementById("compass_image").style.right = popo+"px";
  document.getElementById("varioAltitude").innerHTML = "<font face='Comic Sans MS' color='#000000' size=5> "+Math.round(me.pos[2])+"</font>";
  document.getElementById("varioClimbingRate").innerHTML = "<font face='Comic Sans MS' color='#000000' size=4> "+Vz+"</font>";

//hands position regarding speed
  var hands_pos=2;
		if (forwardSpeed <= WING_VX_VZMINI) {
			hands_pos = 26 *(WING_VX_VZMINI - forwardSpeed)/(WING_VX_VZMINI - WING_VX_VMINI) + 54;
		}	
		if (forwardSpeed > WING_VX_VZMINI & forwardSpeed <= WING_VX_GLIDEMAX) {
			hands_pos = 24 *(WING_VX_GLIDEMAX - forwardSpeed)/(WING_VX_GLIDEMAX - WING_VX_VZMINI) + 30;
		}	
		if (forwardSpeed > WING_VX_GLIDEMAX) {
			hands_pos = 28 *(WING_VX_VMAX - forwardSpeed)/(WING_VX_VMAX - WING_VX_GLIDEMAX) + 2;
		}
  document.getElementById("hands").style.top = hands_pos+"px";


  // GPS map center
//  mapGPS.panTo(new GLatLng(lla[0], lla[1]));
  //  GPS info
  document.getElementById("gpsGroundSpeed").innerHTML = Math.round(3.6*10*airSpeed)/10+"<br />";
  document.getElementById("gpsGroundSpeed").innerHTML += "<div class='gpsScreenText'>&nbsp;<br />Ground GR :";
  if (Vz<0) { document.getElementById("gpsGroundSpeed").innerHTML += "<div class='gpsScreenNumber'>"+Math.round(-10*airSpeed/Vz)/10+"</div></div>"; }
  else { document.getElementById("gpsGroundSpeed").innerHTML += "<div class='gpsScreenNumber'>&nbsp;--&nbsp;</div></div>"; }
  document.getElementById("gpsHeadingToWaypoint").innerHTML = me.headingToNextWaypoint+"";
  var d=me.distanceToNextWaypoint/1000;
  document.getElementById("gpsDistanceToWaypoint").innerHTML = Math.round(100*d)/100 +"";
  rots.headingImg.rotateTo(me.headingToNextWaypoint-heading); 
 // rots.compassImg.rotateTo(-heading); 
  

  // vario beep
  if (Vz >= 0){
    me.beepFrequency = 1+Vz;
  } else {
    me.beepFrequency = 0;
  }
 if (me.beepFrequency > 0 & varioSoundOnOff){
   var deltaBeep = (now-me.lastBeep)/1000.0;  //when was last beep in seconds ?
   if (deltaBeep > 1/me.beepFrequency) {
     if (!pausedGame) soundManager.play('beep');
     me.lastBeep = now;
   }
 }




  // Hack to work around focus bug
  // TODO: fix that bug and remove this hack.
  ge.getWindow().blur();
};



// Calculate local windSpeed vector
Truck.prototype.windLocal = function(lla) {
  var me = this;
  var windDirectionLocal = 90-WIND_DIRECTION ; // because x axis points east while wind direction is counted from north..
  var windDirectionRadians = windDirectionLocal * Math.PI / 180;
  //get glider altitude
  var groundAlt = ge.getGlobe().getGroundAltitude(lla[0], lla[1]);
  var altitude = me.pos[2]-groundAlt;
   //      document.getElementById("jsInfo2").innerHTML += '<br />AGL = '+Math.round(altitude)+" m";

  // let s orientate the wind parallel to the ground (up or down) if the glider is less than 200m AGL
  if (altitude<200){
     //get ground altitude 10 meters further than
     // the glider actual position in the direction of the wind
     // (to see if we are above a slope)
     var pointFurther = [me.pos[0]+10*Math.cos(windDirectionRadians),
                          me.pos[1]+10*Math.sin(windDirectionRadians),
                            0] ;
     gPointFurther = V3.add(me.localAnchorCartesian,
                M33.transform(me.localFrame, pointFurther));

     var llaPointFurther = V3.cartesianToLatLonAlt(gPointFurther);

     var pointFurtherAlt = ge.getGlobe().getGroundAltitude(llaPointFurther[0], llaPointFurther[1]);

     var groundAngle = Math.atan((groundAlt-pointFurtherAlt)/10);

     // if between 150 and 200 AGL : lets minize the ground deflection in a linear way
     // groundAngle varies from groundAngle at 150m to 0 at 200m
     if (altitude > 150) {
	groundAngle = (200-altitude) * groundAngle / 50;
     }

     // wind direction oriented parallel to the groundAngle
     me.wind = [-1*WIND_SPEED*Math.cos(windDirectionRadians)*Math.cos(groundAngle),
                     -1*WIND_SPEED*Math.sin(windDirectionRadians)*Math.cos(groundAngle),
                        WIND_SPEED*Math.sin(groundAngle)];
  } else {
    me.wind = [-WIND_SPEED*Math.cos(windDirectionRadians), -WIND_SPEED*Math.sin(windDirectionRadians), 0];
  }
};


// TODO: would be nice to have globe.getGroundNormal() in the API.
function estimateGroundNormal(pos, frame) {
  // Take four height samples around the given position, and use it to
  // estimate the ground normal at that position.
  //  (North)
  //     0
  //     *
  //  2* + *3
  //     *
  //     1
  var pos0 = V3.add(pos, frame[0]);
  var pos1 = V3.sub(pos, frame[0]);
  var pos2 = V3.add(pos, frame[1]);
  var pos3 = V3.sub(pos, frame[1]);
  var globe = ge.getGlobe();
  function getAlt(p) {
    var lla = V3.cartesianToLatLonAlt(p);
    return globe.getGroundAltitude(lla[0], lla[1]);
  }
  var dx = getAlt(pos1) - getAlt(pos0);
  var dy = getAlt(pos3) - getAlt(pos2);
  var normal = V3.normalize([dx, dy, 2]);
  return normal;
}



Truck.prototype.scheduleTick = function() {
  var me = this;
  if (me.doTick) {
    setTimeout(function() { me.tick(); }, TICK_MS);
  }
};


// Cut the camera to look at me.
Truck.prototype.cameraCut = function() {
  var me = this;
  var lo = me.model.getLocation();
  var la = ge.createLookAt('');
  if (TEMP_ALT > 0) la.set(lo.getLatitude(), lo.getLongitude(),
                        10 + me.pos[2] /* altitude */,
                        ge.ALTITUDE_ABSOLUTE,
                        fixAngle(me.model.getOrientation().getHeading()),
                        80, /* tilt */
                        50 /* range */         
                     );
  else               la.set(lo.getLatitude(), lo.getLongitude(),
                        10 + me.pos[2] /* altitude */,
                        ge.ALTITUDE_ABSOLUTE,
                        fixAngle(me.model.getOrientation().getHeading()-10),
                        80, /* tilt */
                        50 /* range */         
                     );
  ge.getView().setAbstractView(la);
  ge.getOptions().setFlyToSpeed(ge.SPEED_TELEPORT);

};

Truck.prototype.cameraFollow = function(dt, truckPos, localToGlobalFrame, headHeading, headTilt) {

  var me = this;

  if (turnHeadBack) {headHeading = 180; headTilt=80;}

  var c0 = Math.exp(-dt / 0.5);
  var c1 = 1 - 0.7 * c0;

  var la = ge.getView().copyAsCamera(ge.ALTITUDE_RELATIVE_TO_GROUND);

  var truckHeading = me.model.getOrientation().getHeading();
  var camHeading = la.getHeading();
  var camTilt    = la.getTilt();

  var deltaHeading = fixAngle(truckHeading + headHeading - camHeading);
  var heading = camHeading + c1 * deltaHeading;
  heading = fixAngle(heading);

  var headingRadians = heading / 180 * Math.PI;

  var deltaTilt = fixAngle(headTilt - camTilt);
  var tilt = camTilt + c1 * deltaTilt;

 if (cameraZoomIn){
    me.camZoom = clamp(0.90 * me.camZoom, 0.05, 100);
  }
  if (cameraZoomOut){
    me.camZoom = clamp(1.1 * me.camZoom, 0.05, 100);
  } 
  
  var TRAILING_DISTANCE = 90 * me.camZoom * Math.sin ( tilt / 180 * Math.PI ) ;
  var CAM_HEIGHT = 90 * me.camZoom * Math.cos ( tilt / 180 * Math.PI ) ;
 
  var headingDir = V3.rotate(localToGlobalFrame[1], localToGlobalFrame[2],
                             -headingRadians);
  var camPos = V3.add(truckPos, V3.scale(localToGlobalFrame[2], CAM_HEIGHT));
  camPos = V3.add(camPos, V3.scale(headingDir, -TRAILING_DISTANCE));
  var camLla = V3.cartesianToLatLonAlt(camPos);
  var camLat = camLla[0];
  var camLon = camLla[1];
  var camAlt = camLla[2];

  while (camAlt <= 0) {
    me.camZoom = clamp(0.90 * me.camZoom, 0.0005, 100);
    TRAILING_DISTANCE = 90 * me.camZoom * Math.sin ( tilt / 180 * Math.PI ) ;
    CAM_HEIGHT = 90 * me.camZoom * Math.cos ( tilt / 180 * Math.PI ) ;
 
    headingDir = V3.rotate(localToGlobalFrame[1], localToGlobalFrame[2],
                               -headingRadians);
    camPos = V3.add(truckPos, V3.scale(localToGlobalFrame[2], CAM_HEIGHT));
    camPos = V3.add(camPos, V3.scale(headingDir, -TRAILING_DISTANCE));
    camLla = V3.cartesianToLatLonAlt(camPos);
    camLat = camLla[0];
    camLon = camLla[1];
    camAlt = camLla[2];
  }

  la.set(camLat, camLon, camAlt, ge.ALTITUDE_ABSOLUTE, 
        heading, tilt /*tilt*/, 0 /*range*/);
  ge.getView().setAbstractView(la);
};


// heading is optional.
Truck.prototype.teleportTo = function(lat, lon, heading) {
  var ABOVE_TAKEOFF = 0;
  var headingRadians = heading / 180 * Math.PI;
  var me = this;

  var groundAltitude = ge.getGlobe().getGroundAltitude(lat, lon);

  me.model.getLocation().setLatitude(lat);
  me.model.getLocation().setLongitude(lon);

  if (TEMP_ALT > 0 && TEMP_ALT > groundAltitude) var myAltitude = TEMP_ALT ; 
  if (TEMP_ALT > 0 && groundAltitude >= TEMP_ALT)  var myAltitude = groundAltitude+50 ; 
  if (TEMP_ALT <= 0)  var myAltitude = groundAltitude + 50 + WINCH_ALTITUDE;
  me.model.getLocation().setAltitude(myAltitude);  

//alert(groundAltitude+' // '+myAltitude);

  if (heading == null) {
    heading = 0;
  }

  me.vel=[0,0,0];

  me.previousAnchorLla = [lat,lon,0];
  me.localAnchorLla = [lat, lon, 0];
  me.localAnchorCartesian = V3.latLonAltToCartesian(me.localAnchorLla);
  me.localFrame = M33.makeLocalToGlobalFrame(me.localAnchorLla);
  me.modelFrame = M33.identity();
  me.modelFrame[0] = V3.rotate(me.modelFrame[0], me.modelFrame[2], -headingRadians);
  me.modelFrame[1] = V3.rotate(me.modelFrame[1], me.modelFrame[2], -headingRadians);
  
  if (TEMP_ALT > 0) {
      var headingLocal = 90-heading ; // because x axis points east while wind direction is counted from north..
      var headingLocalRadians = headingLocal * Math.PI / 180;
      me.vel = [10*Math.cos(headingLocalRadians),10*Math.sin(headingLocalRadians), -2];
  }
  me.pos = [0, 0, myAltitude];
 
  TAKEOFF_ALTITUDE = me.pos[2];

  me.cameraCut();

};

// Move our anchor closer to our current position.  Retain our global
// motion state (position, orientation, velocity).
Truck.prototype.adjustAnchor = function() {
  var me = this;
  var oldLocalFrame = me.localFrame;

  var globalPos = V3.add(me.localAnchorCartesian,
                         M33.transform(oldLocalFrame, me.pos));
  var newAnchorLla = V3.cartesianToLatLonAlt(globalPos);
  newAnchorLla[2] = 0;  // For convenience, anchor always has 0 altitude.

  var newAnchorCartesian = V3.latLonAltToCartesian(newAnchorLla);
  var newLocalFrame = M33.makeLocalToGlobalFrame(newAnchorLla);

  var oldFrameToNewFrame = M33.transpose(newLocalFrame);
  oldFrameToNewFrame = M33.multiply(oldFrameToNewFrame, oldLocalFrame);

  var newVelocity = M33.transform(oldFrameToNewFrame, me.vel);
  var newModelFrame = M33.multiply(oldFrameToNewFrame, me.modelFrame);
  var newPosition = M33.transformByTranspose(
      newLocalFrame,
      V3.sub(globalPos, newAnchorCartesian));

  me.previousAnchorLla = me.localAnchorLla;
  me.localAnchorLla = newAnchorLla;
  me.localAnchorCartesian = newAnchorCartesian;
  me.localFrame = newLocalFrame;
  me.modelFrame = newModelFrame;
  me.pos = newPosition;
  me.vel = newVelocity;
}


Truck.prototype.bringBackThermals = function() {
  var me = this;
//alert('bring back thermals');
  var countMovedThermals = 0;
  for (var t=0; t < THERMAL_DENSITY; t++){

    if (getDistance(me.lla, [me.thermalCenterLla[t][0],me.lla[1],0]) > 10000) {  // further than 10km in lat

      countMovedThermals++; var formerLat = me.thermalCenterLla[t][0];  var formerLng = me.thermalCenterLla[t][1]; 

      var deltaLat = me.lla[0] + me.previousAnchorLla[0] - 2 * me.thermalCenterLla[t][0] ;

           me.thermalCenterLla[t][0] = me.lla[0] + me.previousAnchorLla[0] - me.thermalCenterLla[t][0];
           me.thermalTopCenterLla[t][0] = me.thermalTopCenterLla[t][0] + deltaLat ;

           cloudModel[t].getLocation().setLatLngAlt(me.thermalTopCenterLla[t][0], me.thermalTopCenterLla[t][1], THERMAL_TOP);
           var lineString = ge.createLineString('');
           lineStringPlacemark[t].setGeometry(lineString);
           lineString.setAltitudeMode(ge.ALTITUDE_ABSOLUTE);
           lineString.getCoordinates().pushLatLngAlt(me.thermalCenterLla[t][0],   me.thermalCenterLla[t][1],   0);
           lineString.getCoordinates().pushLatLngAlt(me.thermalTopCenterLla[t][0],   me.thermalTopCenterLla[t][1],  THERMAL_TOP);
           ge.getFeatures().appendChild(lineStringPlacemark[t]);

  //     var lineStyle = lineStringPlacemark[t].getStyleSelector().getLineStyle();
  //     lineStyle.getColor().set('440000ff');  // aabbggrr format
  //     alert('moved from : ['+formerLat+', '+formerLng+']\nto : ['+me.thermalCenterLla[t][0]+', '+me.thermalCenterLla[t][1]+']'); 

    }  //end of if (further than 10km in lat)

    if (getDistance(me.lla, [me.lla[0], me.thermalCenterLla[t][1], 0]) > 10000) {  // further than 10km in lng

     countMovedThermals++;
     var deltaLng = me.lla[1] + me.previousAnchorLla[1] - 2 * me.thermalCenterLla[t][1];

           me.thermalCenterLla[t][1] = me.lla[1] + me.previousAnchorLla[1] - me.thermalCenterLla[t][1] ;
           me.thermalTopCenterLla[t][1] = me.thermalTopCenterLla[t][1] + deltaLng ;

           cloudModel[t].getLocation().setLatLngAlt(me.thermalTopCenterLla[t][0], me.thermalTopCenterLla[t][1], THERMAL_TOP);
           var lineString = ge.createLineString('');
           lineStringPlacemark[t].setGeometry(lineString);
           lineString.setAltitudeMode(ge.ALTITUDE_ABSOLUTE);
           lineString.getCoordinates().pushLatLngAlt(me.thermalCenterLla[t][0],   me.thermalCenterLla[t][1],   0);
           lineString.getCoordinates().pushLatLngAlt(me.thermalTopCenterLla[t][0],   me.thermalTopCenterLla[t][1],  THERMAL_TOP);
           ge.getFeatures().appendChild(lineStringPlacemark[t]);
    }  //end of if (further than 10km in lat)

  }    //end of for 

//alert('bring back thermals done : '+countMovedThermals+' thermals moved');

} // end bringBackThermals


// Keep an angle in [-180,180]
function fixAngle(a) {
  while (a < -180) {
    a += 360;
  }
  while (a > 180) {
    a -= 360;
  }
  return a;
}
// Keep an angle in [0,360]
function fixAngle2(a) {
  while (a < 0) {
    a += 360;
  }
  while (a > 360) {
    a -= 360;
  }
  return a;
}

Truck.prototype.endFlight = function(){
  var me = this;
 
  document.getElementById("vario").style.display = "none";
  document.getElementById("GPS").style.display = "none";
  document.getElementById("pilotButtons").style.display = "none";
  document.getElementById("compass_container").style.display = "none";
  
    //********** IGC Last Point at Landing
    var dateNow = new Date();
    var latText='N';
    var lngText='E';
    var lat = me.lla[0];
    var lng = me.lla[1];
    if (me.lla[0]<0) {latText='S'; lat = - lat} 
    if (me.lla[1]<0) {lngText='W'; lng = - lng}
    var lat = addExtraZeros(2, Math.floor(lat))+''+ addExtraZeros(5, Math.round( 60 * Math.round(100000 * (lat - Math.floor(lat))) / 100)); // convert decimal degrees to decimal minutes
    var lng = addExtraZeros(3, Math.floor(lng))+''+ addExtraZeros(5, Math.round( 60 * Math.round(100000 * (lng - Math.floor(lng))) / 100)); // convert decimal degrees to decimal minutes
    me.igc += 'B'+ addExtraZeros(2,dateNow.getHours()) +''+addExtraZeros(2,dateNow.getMinutes())+''+addExtraZeros(2,dateNow.getSeconds());
    me.igc += lat+''+latText;
    me.igc += lng+''+lngText;
    me.igc += 'A'+addExtraZeros(10, Math.round(me.lla[2]))+'\n';
    me.igc +='LXGD Downloaded 2006-05-01  16:40:12';
    //********** End of IGC Last Point at Landing

  me.StopWrittingIGCOnceForAll = true;

//  document.getElementById("map3d").style.display = "none";

  if (me.goalReached==0) me.flightDistance = me.flightDistance + me.distanceInCurrentLeg;
  else me.flightDistance = 999999;
  
  document.igcForm.igcFile.value = me.igc;
  document.igcForm.result_id.value = MY_RESULT_ID ;
  document.igcForm.last_waypoint_reached.value = me.actualWaypoint;
  document.igcForm.flight_distance.value = me.flightDistance;
  document.igcForm.task_last_waypoint.value = LAST_WAYPOINT;
  document.igcForm.flight_duration.value = me.flightDuration;
  document.igcForm.start_flight_time.value = Math.floor( me.startFlightTime / 1000 );
  document.igcForm.end_flight_time.value = Math.floor( dateNow / 1000 );
 // document.getElementById("igcForm").style.display = "block";
 // document.igcForm.submit();
  
  truck = null;
  ge = null;
  
  document.igcForm.submit();
  
}

Truck.prototype.inThermal = function(){
  var me = this;

 // document.getElementById("jsInfo3").innerHTML = '<br /><b>IN THERMAL !!!!</b>' ;
  me.flyingInThermal = 1;

  var thermalMidAirCenterLla = [ me.myActualThermalCenterLla[0] + me.lla[2] * (me.myActualThermalTopCenterLla[0]-me.myActualThermalCenterLla[0]) / THERMAL_TOP,
	                      me.myActualThermalCenterLla[1] + me.lla[2] * (me.myActualThermalTopCenterLla[1]-me.myActualThermalCenterLla[1]) / THERMAL_TOP,
	                      me.lla[2] ] ;

  var distanceToCenter = getDistance(me.lla, thermalMidAirCenterLla);

 // document.getElementById("jsInfo3").innerHTML += '<br />distance to center : '+distanceToCenter+" m";

  if(distanceToCenter < 0.6 * THERMAL_RADIUS){
    me.thermalUpliftSpeed = (0.7-1) *  me.myActualThermalSpeed  * distanceToCenter / (0.6 * THERMAL_RADIUS) +  me.myActualThermalSpeed  ;
  } else {
    me.thermalUpliftSpeed = ( (0.7-(-0.2)) * me.myActualThermalSpeed  * distanceToCenter / ((0.6-1) * THERMAL_RADIUS ))
                                + (-0.2-((0.7-(-0.2))/(0.6-1))) * me.myActualThermalSpeed  ;     //linear uplift with distance to center (with 'downlift' in border of thermal)
  }
  
  var liftDecreaseAltitude = 50 ;
  if ( (me.lla[2]+liftDecreaseAltitude) > THERMAL_TOP) {
       me.thermalUpliftSpeed = me.thermalUpliftSpeed * (THERMAL_TOP-me.lla[2]) / liftDecreaseAltitude ;
  }

  if (distanceToCenter > THERMAL_RADIUS ||  me.lla[2]>THERMAL_TOP){
    me.flyingInThermal = 0 ;
//  document.getElementById("jsInfo3").innerHTML = '<br />not in thermal :(' ;
  }
}



Truck.prototype.spotNearbyThermals = function(){
  var me = this;
  // we moved 500 m  away from previous thermals spoting position
  // let s find which thermals are nearby
  me.nearbyThermalsCount=0;
  for (var t=0; t < THERMAL_DENSITY; t++) {
    var thermalMidAirAround = [ me.thermalCenterLla[t][0] + me.lla[2] * (me.thermalTopCenterLla[t][0]-me.thermalCenterLla[t][0]) / THERMAL_TOP,
	                    me.thermalCenterLla[t][1] + me.lla[2] * (me.thermalTopCenterLla[t][1]-me.thermalCenterLla[t][1]) / THERMAL_TOP,
	                    me.lla[2] ];
    if ( getDistance(me.lla, thermalMidAirAround) < 800 ) {
       me.nearbyThermalsId[me.nearbyThermalsCount] = t;
       me.nearbyThermalsCount++;
    }
  }
  me.nearbyThermalsSearch=true;
}

function getDistance(lla1, lla2){
 var R = 6371000; // earth radius in meters
 var lat1 = lla1[0]*Math.PI/180;
 var lon1 = lla1[1]*Math.PI/180;
 var lat2 = lla2[0]*Math.PI/180;
 var lon2 = lla2[1]*Math.PI/180;
 var d = Math.acos(Math.sin(lat1)*Math.sin(lat2) +
                  Math.cos(lat1)*Math.cos(lat2) *
                  Math.cos(lon2-lon1)) * R;
 return Math.round(d);
}

function getBearing(lla1, lla2) {
  var lat1 = lla1[0].toRad();
  var lat2 = lla2[0].toRad();
  var dLon = (lla2[1]-lla1[1]).toRad();
  var y = Math.sin(dLon) * Math.cos(lat2);
  var x = Math.cos(lat1)*Math.sin(lat2) -
          Math.sin(lat1)*Math.cos(lat2)*Math.cos(dLon);
  return Math.round(Math.atan2(y, x).toBrng());
}
function getBearingDouble(lla1, lla2) {
  var lat1 = lla1[0].toRad();
  var lat2 = lla2[0].toRad();
  var dLon = (lla2[1]-lla1[1]).toRad();
  var y = Math.sin(dLon) * Math.cos(lat2);
  var x = Math.cos(lat1)*Math.sin(lat2) -
          Math.sin(lat1)*Math.cos(lat2)*Math.cos(dLon);
  return Math.atan2(y, x).toBrng();
}


function getLatLngFromBearingAndDistance(lla, direction, distance){
  var lat1 = lla[0]*Math.PI/180;
  var lng1 = lla[1]*Math.PI/180;
     var lat = Math.asin(Math.sin(lat1)*Math.cos(distance)+Math.cos(lat1)*Math.sin(distance)*Math.cos(direction));
     if  (Math.cos(lat)==0)
        var lng = lng1 ;      // endpoint a pole
     else
        var lng = (lng1-Math.asin(Math.sin(direction)*Math.sin(distance)/Math.cos(lat1))+Math.PI % 2*Math.PI)-Math.PI;

     var result = Array(lat, lng, 0);
     return result;
}

function getCartesianDistance(point1, point2){
 var d = Math.sqrt((point2[0]-point1[0])*(point2[0]-point1[0]) + (point2[1]-point1[1])*(point2[1]-point1[1]));
 return d;
}

function addExtraZeros(size, number){
  var temp = ''+number; //convert number to string
  while (temp.length < size){
    temp = '0'+temp;
  }
  return temp;
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
// extend Number object with methods for converting degrees/radians
Number.prototype.toRad = function() {  // convert degrees to radians
  return this * Math.PI / 180;
}
Number.prototype.toDeg = function() {  // convert radians to degrees (signed)
  return this * 180 / Math.PI;
}
Number.prototype.toBrng = function() {  // convert radians to degrees (as bearing: 0...360)
  return (this.toDeg()+360) % 360;
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

function Point_At_Distance_And_Bearing(start_lat,start_lon,distance,bearing) { // input is in degrees, km, degrees
	// http://www.fcc.gov/mb/audio/bickel/sprong.html
	var earth_radius = 6378137; // equatorial radius
	// var earth_radius = 6356752; // polar radius
	// var earth_radius = 6371000; // typical radius
	var start_lat_rad = start_lat.toRad();
	var start_lon_rad = start_lon.toRad();
	var ending_point = []; // output
	var isig = 0; // azimuth < 180 degrees
	var a = (360-bearing) % 360;
	if (a > 180) { a = 360 - a; isig = 1; } // azimuth > 180 degrees
	var a = a.toRad(); var bb = (Math.PI/2)-start_lat_rad; var cc = distance/earth_radius;
	var sin_bb = Math.sin(bb); var cos_bb = Math.cos(bb); var cos_cc = Math.cos(cc);
	var cos_aa = cos_bb*cos_cc+(sin_bb*Math.sin(cc)*Math.cos(a));
	if (cos_aa <= -1) { cos_aa = -1; } if (cos_aa >= 1) { cos_aa = 1; }
	var aa = Math.acos(cos_aa);  //  (cos_aa.toFixed(15) == 1) ? 0 : Math.acos(cos_aa);
	var cos_c = (cos_cc-(cos_aa*cos_bb))/(Math.sin(aa)*sin_bb);
	if (cos_c <= -1) { cos_c = -1; } if (cos_c >= 1) { cos_c = 1; }
	var c = Math.acos(cos_c);  //  (cos_c.toFixed(15) == 1) ? 0 : Math.acos(cos_c);
	var end_lat_rad = (Math.PI/2)-aa;
	var end_lon_rad = start_lon_rad-c;
	if (isig == 1) { end_lon_rad = start_lon_rad + c; }
	if (end_lon_rad > Math.PI) { end_lon_rad = end_lon_rad - (2*Math.PI); }
	if (end_lon_rad < (0-Math.PI)) { end_lon_rad = end_lon_rad + (2*Math.PI); }
	ending_point[0] = end_lat_rad.toDeg(); ending_point[1] = end_lon_rad.toDeg();
	return (ending_point);
}