<?php
include '../connexion.php';

$pretenders = array();
$randomNumbers = array();

$competition = $_GET['competition'];
$min_tasks=$_GET['min_tasks'];

?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../pgearth/style/style.css"/>
</head>
<body>

<?php

echo "<b>Pilots with the number of tasks they flew ($min_tasks tasks to be elligible for the prizes) : </b><br />";

$query = "select distinct pilot_id, pseudo from pgsim_comp_results left join auteur on pilot_id=id_auteur where competition like '$competition'";
$result = mysql_query($query);
while ($val=mysql_fetch_array($result)){
   $q = "select count(id) as tasks from pgsim_comp_results where pilot_id = ".$val['pilot_id']." and competition like '$competition'";
   $r = mysql_query($q);
   while ($v=mysql_fetch_array($r)){
      if ($v['tasks'] >= $min_tasks ) echo "<img src='../pgearth/images/famfamfamicons/bullet_green.png' /> <b>";
      else echo "<img src='../pgearth/images/famfamfamicons/bullet_red.png' /> ";
      echo $val['pseudo']; 
      if ($v['tasks'] >= $min_tasks ) echo "</b>";
      echo " : ".$v['tasks']." tasks flown";
      echo "<br />";
      if ($v['tasks'] >= $min_tasks ){
         $pretenders[]  = $val["pseudo"];
         $randomNumbers[] =  rand(0,1000);
      } 
   }
}

echo "<hr /><b>Elligible pilots are assignated a random number (from 0 to 1000) : </b><br />";
for($i=0; $i<count($pretenders); $i++){
   echo $pretenders[$i]." => ".$randomNumbers[$i]."<br />";
}

echo "<hr /><b>Elligible pilots ordered by their random number :</b><br />";

array_multisort($randomNumbers, SORT_DESC, $pretenders);

for($i=0; $i<count($pretenders); $i++){
   echo $i+1;
   echo " : ";
   if ($i==0) echo "<img src='../pgearth/images/famfamfamicons/medal_gold_3.png' /> <b>";
   if ($i==1) echo "<img src='../pgearth/images/famfamfamicons/medal_silver_3.png' /> <b>";
   if ($i==2) echo "<img src='../pgearth/images/famfamfamicons/medal_bronze_3.png' /> <b>";
   echo $pretenders[$i]." => ".$randomNumbers[$i];
   if ($i < 3) echo "</b>"; 

   if ($i < 3) {
      $qmail = "select mail from auteur where pseudo like '".$pretenders[$i]."' ";
      $rmail = mysql_query($qmail);
      $vmail = mysql_fetch_array($rmail);
   
      if ($vmail['mail']<>"") echo " <img src='../pgearth/images/famfamfamicons/email_add.png' />";
      else echo " <img src='../pgearth/images/famfamfamicons/email_delete.png' />";
   }

   echo "<br />";
}

?>
</body>
</html>