<?php
session_start();
include "../connexion.php";

  $pseudo = "Anonymous";  $id_pilot = 0;

if (isset ($_SESSION['site'])){
  $result = mysql_query("select latd, longd, nom, winch from site where id_site=".$_SESSION['site'].";");
  $val = mysql_fetch_array($result);
    $site_name = $val['nom'];
    $site_lat = $val['latd'];
    $site_lng = $val['longd'];


  if (isset ($_SESSION['id_membre'])){
    $auteur = mysql_fetch_array(mysql_query("select id_auteur, pseudo from auteur where id_auteur=".$_SESSION['id_membre'].";"));
    $pseudo = $auteur['pseudo']; $id_pilot = $auteur['id_auteur'];

  }
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

<head>

<link href="../pgearth/style/style.css" rel="stylesheet" type="text/css" />
<link
    href="external_js/carpe_slider/default.css"
    rel="stylesheet"
    type="text/css" />

<script
    language="JavaScript"
    src="external_js/carpe_slider/slider.js" >
</script>

<title>Let's go fly</title>

<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />

</head>

<body>

 <?php
 if (!isset ($_SESSION['site'])){ ?>
   to play the simmulator, you must first go to a site page on <a href="../pgearth/?accueil=accueil">paraglidingEarth</a>,<br />
   then click on 'Fly this site' link you will find at the top left corner of the site page
<?php } else { 
?>
  <form action="pgepgsim_v0.php" method="post">
    <hr />
    Pilot : <?php echo $pseudo;?> <br />
    Site : <?php echo $val['nom'];?> <br />
    Takeoff heading : <input type="text" name="takeoff_heading"  size="4" value="<?php echo $_GET['takeoff_heading'];?>" /> degrees<br />
<?php
 if ($val['winch']=="1"){ 
?>
    Winch start altitude: <input type="text" name="winch_altitude"  size="4" value="600" /> metres
<?php } ?>
    <hr />
    Select glider look :
    <table width="100%">
    <tr>
     <td width="33%">
    <label for="striped"><img src ="http://sketchup.google.com/3dwarehouse/download?mid=46001b9d0a2df91b5e6ec957e2143302&rtyp=st&ctyp=other"  />
    <input type="radio" name="glider" id="striped" value="4e01a5a1f374d9d3a426bad1ca64772" checked> White/blue striped </label>
     </td><td width="34%">
    <label for="yellow"><img src ="http://sketchup.google.com/3dwarehouse/download?mid=52df851a45ec0dc73a426bad1ca64772&rtyp=st&ctyp=other"  />
    <input type="radio" name="glider" id="yellow" value="52df851a45ec0dc73a426bad1ca64772"> Yellow   </label>
     </td><td width="33%">
    <label for="red"><img src ="http://sketchup.google.com/3dwarehouse/download?mid=206fcd97f76b645f3a426bad1ca64772&rtyp=st&ctyp=other"  />
    <input type="radio" name="glider" id="red" value="206fcd97f76b645f3a426bad1ca64772"> Red </label>
     </td>
     </tr><tr>
     <td>
    <label for="blue"><img src ="http://sketchup.google.com/3dwarehouse/download?mid=c5e687317aa46cc13a426bad1ca64772&rtyp=st&ctyp=other"  />
    <input type="radio" name="glider" id="blue" value="c5e687317aa46cc13a426bad1ca64772"> Blue </label>
     </td><td>
    <label for="white"><img src ="http://sketchup.google.com/3dwarehouse/download?mid=123f9c53d58dff103a426bad1ca64772&rtyp=st&ctyp=other"  />
    <input type="radio" name="glider" id="white" value="123f9c53d58dff103a426bad1ca64772"> White  </label>
     </td><td>
    <label for="pink"><img src ="http://sketchup.google.com/3dwarehouse/download?mid=d592753b940336353a426bad1ca64772&rtyp=st&ctyp=other"  />
    <input type="radio" name="glider" id="pink" value="d592753b940336353a426bad1ca64772"> Pink  </label>
     </td>
    </tr>
    </table>
    <hr />

<table>
<tr>
  <td colspan="3">
    <b>Wind : </b>
  </td>
</tr>
<tr>
 <td align="right">
  Wind speed :
 </td>
 <td>
<div class="carpe_horizontal_slider_display_combo">
  <div class="carpe_horizontal_slider_track"  style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <div class="carpe_slider_slit" style="background-color: #300; border-color: #b99 #fdd #fdd #b99;">&nbsp;</div>
    <!-- Default position: 80px -->
    <div
      class="carpe_slider"
      id="sliderWindSpeed"
      display="displayWindSpeed"
      style="left: 40px; background-color: #369; border-color: #69c #036 #036 #69c;">&nbsp;</div>
  </div>
  <div class="carpe_slider_display_holder" style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <!-- Default value: 180 -->
    <input
      class="carpe_slider_display"
      name="wind_speed"
      id="displayWindSpeed"
      style="background-color: #fff; color: #258;"
      type="text"
      from="0"
      to="10"
      value="4" />
  </div>
</div>
  </td>
  <td>  &nbsp;meters per second
  </td>
 </tr>
 <tr>
  <td align="right">
    Wind direction :
  </td>
  <td>
<div class="carpe_horizontal_slider_display_combo">
  <div class="carpe_horizontal_slider_track"  style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <div class="carpe_slider_slit" style="background-color: #300; border-color: #b99 #fdd #fdd #b99;">&nbsp;</div>
    <!-- Default position: 80px -->
    <div
      class="carpe_slider"
      id="sliderWindDirection"
      display="displayWindDirection"
      style="left: <?php echo $_GET['takeoff_heading']/3.6;?>px; background-color: #369; border-color: #69c #036 #036 #69c;">&nbsp;</div>
  </div>
  <div class="carpe_slider_display_holder" style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <!-- Default value: 180 -->
    <input
      class="carpe_slider_display"
      name="wind_direction"
      id="displayWindDirection"
      style="background-color: #fff; color: #258;"
      type="text"
      from="0"
      to="360"
      value="<?php echo $_GET['takeoff_heading'];?>" />
  </div>
</div>
    </td>
    <td>   &nbsp;degrees
    </td>
  </tr>
<tr>
  <td colspan="3">
    <b>Thermals : </b>
  </td>
</tr>
        <tr>
          <td align="right"> Thermals density :</td><td>

<div class="carpe_horizontal_slider_display_combo">
  <div class="carpe_horizontal_slider_track"  style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <div class="carpe_slider_slit" style="background-color: #300; border-color: #b99 #fdd #fdd #b99;">&nbsp;</div>
    <!-- Default position: 80px -->
    <div
      class="carpe_slider"
      id="sliderThermalsDensity"
      display="displayThermalsDensity"
      style="left: 100px; background-color: #369; border-color: #69c #036 #036 #69c;">&nbsp;</div>
  </div>
  <div class="carpe_slider_display_holder" style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <!-- Default value: 180 -->
    <input
      class="carpe_slider_display"
      name="thermal_density" 
      id="displayThermalsDensity"
      style="background-color: #fff; color: #258;"
      type="text"
      from="0"
      to="4"
      value="4" />
  </div>
</div>
          </td>
          <td>  &nbsp;thermals&nbsp;per&nbsp;km&nbsp;square
          </td>
        </tr>
        <tr>
          <td align="right">
            Thermals max height : </td>
          <td>

<div class="carpe_horizontal_slider_display_combo">
  <div class="carpe_horizontal_slider_track"  style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <div class="carpe_slider_slit" style="background-color: #300; border-color: #b99 #fdd #fdd #b99;">&nbsp;</div>
    <!-- Default position: 80px -->
    <div
      class="carpe_slider"
      id="sliderThermalsHeight"
      display="displayThermalsHeight"
      style="left: 59px; background-color: #369; border-color: #69c #036 #036 #69c;">&nbsp;</div>
  </div>
  <div class="carpe_slider_display_holder" style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <!-- Default value: 180 -->
    <input
      class="carpe_slider_display"
      name="thermal_height" 
      id="displayThermalsHeight"
      style="background-color: #fff; color: #258;"
      type="text"
      from="0"
      to="4200"
      value="2460" />
  </div>
</div>

          </td>
          <td> &nbsp;meters&nbsp;ASL
          </td>
        </tr>
        <tr>
          <td align="right">
            Thermals average max uplift speed :  </td>
            <td>

<div class="carpe_horizontal_slider_display_combo">
  <div class="carpe_horizontal_slider_track"  style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <div class="carpe_slider_slit" style="background-color: #300; border-color: #b99 #fdd #fdd #b99;">&nbsp;</div>
    <!-- Default position: 80px -->
    <div
      class="carpe_slider"
      id="sliderThermalsSpeed"
      display="displayThermalsSpeed"
      style="left: 50px; background-color: #369; border-color: #69c #036 #036 #69c;">&nbsp;</div>
  </div>
  <div class="carpe_slider_display_holder" style="background-color: #fff; border-color: #def #9ab #9ab #def;">
    <!-- Default value: 180 -->
    <input
      class="carpe_slider_display"
      name="thermal_speed"
      id="displayThermalsSpeed"
      style="background-color: #fff; color: #258;"
      type="text"
      from="2"
      to="10"
      value="6" />
  </div>
</div>

          </td>
          <td> meters per second
          </td>
        </tr>
        <tr>
          <td align="right">
            Thermals size :</td><td>
            <input type="radio" name="thermal_size" id="thermal_size_small" value="40"><label for="thermal_size_small"> small </label>
            <br />
            <input type="radio" name="thermal_size" id="thermal_size_medium" value="60"  checked><label for="thermal_size_medium"> medium </label>
            <br />
            <input type="radio" name="thermal_size" id="thermal_size_big" value="80"><label for="thermal_size_big"> large </label>
          </td>
        </tr>
      </table>
    <hr />

    <input type="hidden" name="site_name" value="<?php echo $site_name;?>" />
    <input type="hidden" name="takeoff_lat" value="<?php echo $site_lat;?>" />
    <input type="hidden" name="takeoff_lng" value="<?php echo $site_lng;?>" />
    <input type="hidden" name="pilot_id" value="<?php echo $id_pilot;?>" />
    <input type="hidden" name="pilot_name" value="<?php echo $pseudo;?>" />
    <center>
      click here <img src="../pgearth/images/famfamfamicons/arrow_right.png" /> <input type="submit" value ="Let's fly !" /> <img src="../pgearth/images/famfamfamicons/arrow_left.png" />  click here
    </center>
  </form>
 <?php } ?>
 <hr /><hr />
 <p>
   Some things you should know before you fly :
 </p>
 <p>
   This will only work for <b>Firefox2/3 and IE6/7 users, under Windows</b> (googleearth restrictions, let's hope this will change fast...) <br />
   You will be taken to the gps location of the take off<br />
   Wait a bit until the landscape is loaded : if you can t take off because the wing looks 'caught' in the terrain, refresh the page ('F5' key) once or twice<br />
   Ridge soaring and thermals are simulated, wich can you help staying airborne and gain some altitude<br />
   You can pilot using the arrows keys or the buttons that are located under the flying window<br />
   The glider can someone disappear, don't worry, it will come back after a few time at the center of the window<br />
   When flying close to the mountain, the camera may not be able to stay behind your glider : it will then move up, so your glider will seem to be going down. Don't get surprised !<br />
   If you cant see your glider at take off, it may be because the take off is on a slope, making it not possible for the camera to position behind it : this will stop after you flew a few meters away (or make a slight turn)
 </p>
 That is about it, discussion about this simulation is on <a href="http://www.paraglidingforum.com/viewtopic.php?t=18805">here on paragliding forum</a>.

</body>

</html>