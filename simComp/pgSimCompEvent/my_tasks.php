<?php
session_start();
include "../connexion.php";

// TIME CONVERTER
function secondsToHoursMinutesSeconds($time){
if ($time < 86400 AND $time>=3600)  // si le nombre de secondes ne contient pas de jours mais contient des heures
{  // on refait la m�me op�ration sans calculer les jours
$heure = floor($time/3600);
$reste = $time%3600;
$minute = floor($reste/60);
$seconde = $reste%60;
$result = $heure.'h.'.$minute.'min.'.$seconde.'s.';
}
elseif ($time<3600 AND $time>=60)
{  // si le nombre de secondes ne contient pas d'heures mais contient des minutes
$minute = floor($time/60);
$seconde = $time%60;
$result = $minute.'min.'.$seconde.'s.';
}
elseif ($time < 60)
// si le nombre de secondes ne contient aucune minutes
{ $result = $time.'s.'; }
return $result;
}
//ENDOF TIME CONVERTER

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="../scripts/overlib.js"></script>
<link rel=stylesheet href='../pgearth/style/style.css' type='text/css' />
<title>My tasks</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta name="generator" content="HAPedit 3.1">
</head>
<body>
<?php
$queryTasks  =  "select * from pgsim_comp_tasks 
	left join site on site_id=id_site
	left join pays on pays = id_pays
	where organised_by = ".$_SESSION['id_membre']."
	order by task_length";
$resultTasks =  mysql_query($queryTasks);
$numTasks    =  mysql_num_rows($resultTasks);
?>
<h1>The <?php echo $numTasks;?> tasks I built</h1>

<?php
$here="my_tasks"; 
include "tabs_header.php";
?>
	
<div class="rubriqueSite">
<div class="titreMenu">
My tasks 
</div>
<?php if ($numTasks==0) { ?>
You built no tasks.
<?php } else { ?>
<table>
	<tr align="center">
		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td><b>&nbsp;Task&nbsp;name&nbsp;</b></td>
		<td><b>&nbsp;Definitive/testing<br />state&nbsp;</b></td>		
		<td><b>&nbsp;Task&nbsp;length&nbsp;</b></td>
		<td><b>&nbsp;Site&nbsp;name&nbsp;</b></td>
		<td><b>&nbsp;Competed&nbsp;</b></td>
		<td><b>&nbsp;Completed&nbsp;</b></td>
		<td><b>&nbsp;Fastest time<br />for completed task&nbsp;</b></td>
		<td><b>&nbsp;Watch task details&nbsp;<br />and compete</b></td>
	</tr>

<?php while ($vTasks=mysql_fetch_array($resultTasks)){
		$qSite = "select nom, id_site, iso from site left join pays on site.pays=id_pays where id_site = ".$vTasks['site_id'];
		$vSite = mysql_fetch_array(mysql_query($qSite));
		
		$qResults = "select * from pgsim_comp_results where task_id = ".$vTasks['id'];
		$vResults = mysql_fetch_array(mysql_query($qResults));
		$nResults = mysql_num_rows(mysql_query($qResults));
		$qCompleted = "select * from pgsim_comp_results where task_id = ".$vTasks['id']." and distance = 999999 order by flight_duration asc";
		$vCompleted = mysql_fetch_array(mysql_query($qCompleted));
		$nCompleted = mysql_num_rows(mysql_query($qCompleted));
?>
	<tr align="center">
		<td></td>
		<td><b>&nbsp;<a href="start_task.php?task_id=<?php echo $vTasks['id'];?>"><?php echo $vTasks['task_name'];?></a>&nbsp;</b></td>
		<td><?php if ($vTasks['testing']){$icon="flag_orange.png"; $title="task in testing mode : results may be subject to deletion";} else {$icon="flag_green.png"; $title="Task in final version, definitive results";}?>
		<img src="../pgearth/images/famfamfamicons/<?php echo $icon;?>" title="<?php echo $title;?>">
<?php if ($_SESSION['id_membre']==$vTasks['organised_by'] and $vTasks['testing']) { echo " - <a href='edit_task.php?task_id=".$vTasks['id']."'><img src='../pgearth/images/famfamfamicons/page_edit.png' border='0' title='Edit the task' /> </a>
		<a href=\"#\" onClick=\"confirmDeleteTask('delete_task.php?task_id=".$vTasks['id']."')\"><img src='../pgearth/images/famfamfamicons/bin.png' border='0' title='Delete task' /> </a>"; } ?>
		</td>
		<td>&nbsp;<?php echo $vTasks['task_length']/1000;?> km&nbsp;</td>
		<td><b>&nbsp;<?php echo stripslashes($vSite['nom']);?>&nbsp;</b></td>
		<td>&nbsp;<?php echo $nResults;?>&nbsp;</td>
		<td>&nbsp;<?php echo $nCompleted;?>&nbsp;</td>
		<td>&nbsp;<?php if ($nCompleted>0) echo secondsToHoursMinutesSeconds($vCompleted['flight_duration']); else echo "X";?>&nbsp;</td>
		<td><a href="start_task.php?task_id=<?php echo $vTasks['id'];?>"><b>Go!<img src="../pgearth/images/famfamfamicons/flight.png" border="0"></b></a></td>
	</tr>
<?php
	}
?>
</table><?php } ?>
</div>