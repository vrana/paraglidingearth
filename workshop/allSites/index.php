<!DOCTYPE html>
<html>
<head>
	
	<title>PgEarth - All sites</title>

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	

	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==" crossorigin=""/>
	<link rel="stylesheet" href="markerCluster.css" />
	
	
	<script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js" integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg==" crossorigin=""></script>

	 <script src="../../assets/js/localhost/jquery-2.1.4.min.js"></script>
	 
	<script src="markerCluster.js"></script>

	<style>
		html, body {
			height: 100%;
			margin: 0;
		}
		#map {
			width: 600px;
			height: 400px;
		}
	</style>
	<style>body { padding: 0; margin: 0; } #map { height: 100%; width: 100vw; }</style>
	
</head>
<body>

<div id='map'></div>

<script>
	var map = L.map('map').fitWorld();
	
	var sitesCluster = L.markerClusterGroup({	disableClusteringAtZoom: 8, 
												spiderfyOnMaxZoom: false 
											});

	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox.streets'
	}).addTo(map);
	
	var siteIcon = L.icon({
		iconUrl: "../../assets/img/site.png",
		iconSize: [24, 28],
		iconAnchor: [12, 28],
		popupAnchor: [0, -25],
		labelAnchor: [12, -12],
		tooltipAnchor: [4, -8],
		className: 'siteIcon'
		});
		
	$.getJSON( "../../assets/ajax/sitesMinJSON.php?north=90&west=-180&south=-90&east=180", function( data ) {
		$.each( data, function( key, val ) {
			var marker = L.marker([val.latLng[0], val.latLng[1]], {icon: siteIcon});
			marker.bindTooltip(val.name);
			sitesCluster.addLayer(marker);
		});
		map.addLayer(sitesCluster);
	});
	
	

</script>



</body>
</html>
