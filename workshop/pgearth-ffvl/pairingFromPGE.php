<?php
include "../../connexion.php";
$q = "SELECT id, name, lat, lng, ffvl_suid FROM `site` WHERE `iso` LIKE 'fr' ";
$r = mysqli_query($bdd, $q);
?>
<html>
<body>
	<style>
		tr {
			border: 1px solid black;
		}
	</style>
	<div id="loading">loading ffvl json...</div>
	<table id="theTable">
		<thead>
			<tr>
				<td>pge ID</td>
				<td>pge name</td>
				<td>pge lat</td>
				<td>pge lng</td>
				<td>ffvl id</td>
				<td>ffvl name</td>
				<td>distance</td>
				<td>pair sites</td>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js"></script>
<script>
	jQuery.ajaxSetup({async:false});
	var pgeSites = [
<?php
while ( $v = mysqli_fetch_array($r) ) {
?>
	{ id: <?php echo $v['id'];?>, name:"<?php echo addslashes($v['name']);?>", lat: <?php echo $v['lat'];?>, lng: <?php echo $v['lng'];?>, ffvl_suid: <?php echo $v['ffvl_suid'];?> },
<?php
}	
?>
	];
	var ffvlSites = [];
	var p = 0;
	var getFFVLData =	$.getJSON("http://data.ffvl.fr/json/sites.json", function( result ) {
							for (var i = 0; i < result.length; i++) {
								if ( result[i].site_type == "vol" && result[i].numero.substr(2,1)=="D" && result[i].numero.substr(6,1)=="A") {   //  we have a main takeoff !!!
									p++;
									// document.write(result[i].numero+" - "+result[i].nom+"<br/>");
									ffvlSites.push({ id:result[i].suid, name:result[i].nom, lat:result[i].lat, lng:result[i].lon });
								}
							}
						});
					
	getFFVLData.done( function () {

		$("#loading").hide();
		$("#theTable").show();	
		var distance = 100000;
		
		for (var i = 0; i < pgeSites.length ; i++) {
//			document.write( "<hr />"+pgeSites[i].name+"<br/>");
			var row = '<tr id="row'+pgeSites[i].id+'" ><td>'+pgeSites[i].id+'</td><td>'+pgeSites[i].name+'</td><td>'+pgeSites[i].lat+'</td><td>'+pgeSites[i].lng+'</td>';
			minDistance = 1000000;
			for (var j = 0; j < ffvlSites.length; j++) {
				var distance =  ( 3959 * Math.acos( Math.cos( toRadians(ffvlSites[j].lat) ) * Math.cos( toRadians( pgeSites[i].lat ) ) * Math.cos( toRadians( pgeSites[i].lng ) - toRadians(ffvlSites[j].lng) ) + Math.sin( toRadians(ffvlSites[j].lat) ) * Math.sin( toRadians( pgeSites[i].lat ) ) ) );			
				if (distance < minDistance) {
					var match = j;
					minDistance = distance;
				}
			}
			row += '<td>'+ffvlSites[match].id+'</td><td>'+ffvlSites[match].name+'</td><td>'+Math.round(1000*minDistance)/1000+'</td><td id="pair'+pgeSites[i].id+'"><span onclick="pair('+pgeSites[i].id+','+ffvlSites[match].id+')">pair sites</span></td></tr>';
			$('#theTable > tbody:last-child').append(row);
			if (pgeSites[i].ffvl_suid == ffvlSites[match].id) {
				$('#pair'+pgeSites[i].id).html("paired!!!");
				$('#row'+pgeSites[i].id).css("background-color","green");
			}
			else if (minDistance < 0.1)  { $('#row'+pgeSites[i].id).css("background-color","lightgreen"); pair(pgeSites[i].id,ffvlSites[match].id); }
			else if (minDistance < 0.25) { $('#row'+pgeSites[i].id).css("background-color","lightgreen"); }
			else if (minDistance < 0.5)  { $('#row'+pgeSites[i].id).css("background-color","lightblue");  }
		}
	});
	
	console.log(p);
	
function pair(pge, ffvl){
		// alert('pge:'+pge+', ffvl:'+ffvl);
		$.getJSON( "pair_sites.php", { ffvl: ffvl, pge:pge }, function (data) {
			if(data.result == 'done') {
				//$('#row'+ffvl).hide();
//				paired_sites++;
//				unpaired_sites--;
				$('#row'+pge).css("background-color","green");
				$('#pair'+pge).html("paired");
			}

			else $('#row'+pge).css("background-color","red");
		});
}

	
// Converts from degrees to radians.
function toRadians(degrees) {
  return degrees * Math.PI / 180;
};

</script>


</body>
</html>
