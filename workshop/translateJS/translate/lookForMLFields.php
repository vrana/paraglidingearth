<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Scan pages for ML tags</title>
</head>
<body>
	<input type="text" id="fileToScanURL" />
	<input type="button" id="scanFileButton" value="scan file" />

	<div id="foundElements"></div>



	<script src="../jquery.js"></script>
	<script>
		$("#scanFileButton").on("click", function(){

			$.get( $("#fileToScanURL").val(), function (html) {


				var foundMLs = $(html).find("ml");
				console.log(foundMLs);

				$.each(foundMLs, function () {

					$("#foundElements").append( $(this).attr("mlKey") +"<hr />");


				});

			});


		})
	</script>
</body>
</html>
