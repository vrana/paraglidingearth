<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title tkey="page-main-title">jQuery-multilang Example</title>
</head>
<body>
	<?php
		include("translate/languageCodes.php");
		echo "	<select id='languageSelect'>";
		foreach ($language_codes as $code => $language) {
			echo "		<option value='".$code."'>".$code." -> ".$language."</option>";
		}
		echo "	</select>";
	?>
	
	<div>
		<p><ml mlKey="Hello">Hello</ml></p>
		<p><ml mlKey="Hi">Hi</ml></p>
		<p><ml mlKey="Howdy">Howdy</ml></p>
		<p><ml mlKey="it_s_me">It' s me</ml></p>
	</div>

	<script src="jquery.js"></script>
	<script src="lang.js"></script>

	<script>
		$("#languageSelect").on('change', function(){
			langCode = $("#languageSelect").find(":selected").attr("value");
			translate(langCode);
		});
	</script>

</body>
</html>
