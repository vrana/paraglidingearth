<!DOCTYPE html>
<html>
<head>
	
	<title>Mobile tutorial - Leaflet</title>

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link rel="shortcut icon" type="image/x-icon" href="docs/images/favicon.ico" />

     <link rel="stylesheet" href="../../assets/css/localhost/leaflet.css">
   


	<style>
		html, body {
			height: 100%;
			margin: 0;
		}
		#map {
			width: 600px;
			height: 400px;
		}
		#windy{
			position: fixed;
			top: 30px;
			border: 1px solid orange;
			width : 300px;
			height: 200px;
		}
	</style>

	
</head>


<body>

	<div id="windy"></div>
	<div id='map'></div>


<script src="../../assets/js/localhost/leaflet.js"></script> 
<script src="https://api4.windy.com/assets/libBoot.js"></script>
<script>
	var map = L.map('map').fitWorld();

	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox.streets'
	}).addTo(map);

	function onLocationFound(e) {
		var radius = e.accuracy / 2;

		L.marker(e.latlng).addTo(map)
			.bindPopup("You are within " + radius + " meters from this point").openPopup();

		L.circle(e.latlng, radius).addTo(map);
	}

	function onLocationError(e) {
		alert(e.message);
	}

	map.on('locationfound', onLocationFound);
	map.on('locationerror', onLocationError);

	map.locate({setView: true, maxZoom: 16});




   const options = {

                // Required: API key
                key: '3vdBtb72vicQnIeP7pu7xbe0jrTALSPL',

                // Put additional console output
                verbose: true,

                // Optional: Initial state of the map
                lat: 50.4,
                lon: 14.3,
                zoom: 5,
        }

    // Initialize Windy API
    windyInit( options, windyAPI => {
        // windyAPI is ready, and contain 'map', 'store',
        // 'picker' and other usefull stuff

        const { windyMap } = windyAPI
        // .map is instance of Leaflet map

        L.popup()
            .setLatLng([50.4, 14.3])
            .setContent("Hello World")
            .openOn( windyMap );

    })


</script>



</body>
</html>
