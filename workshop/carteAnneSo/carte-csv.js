var colorIcon = [];

colorIcon["bleu"] = new L.Icon({
  iconUrl: 'img/marker-icon-blue.png',
  shadowUrl: 'img/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});

colorIcon["rouge"] = new L.Icon({
  iconUrl: 'img/marker-icon-red.png',
  shadowUrl: 'img/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});

colorIcon["vert"] = new L.Icon({
  iconUrl: 'img/marker-icon-green.png',
  shadowUrl: 'img/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});
		
colorIcon["jaune"] = new L.Icon({
  iconUrl: 'img/marker-icon-yellow.png',
  shadowUrl: 'img/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});

var map = L.map('map').setView([45.5, 4], 9);	

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
	maxZoom: 18,
	attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
		'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
		'Imagery © <a href="http://mapbox.com">Mapbox</a>',
	id: 'mapbox.streets'
}).addTo(map);

var etablissements = L.layerGroup().addTo(map);		
var tableauMarkers = [];
var bounds = L.latLngBounds();
var surLaCarte = [];

$.get( "listing.csv", function( csv ) {
	var listings = $.csv.toObjects(csv);

	$.each(listings, function (clef, listing) {
		var adresse = listing.Adresse+", "+listing.CP+" "+listing.Ville+", fr";

		$.get("https://maps.googleapis.com/maps/api/geocode/json", { key: "AIzaSyB6cDg8jawxHJ4ee6QpEefJUR1pZegArJc", address : adresse}, function ( data ) {

			lat = data.results[0].geometry.location.lat;
			lng = data.results[0].geometry.location.lng;

			bounds.extend([lat,lng]);

			popUp = "<strong>"+listing.Nom+"</strong><br /><i class=\"fas fa-link\"></i> <a href=\""+listing.web+"\" target=\"_blank\">Site internet</a><br /><i class=\"fas fa-phone\"></i> "+listing.tel+"<br /><i class=\"far fa-envelope\"></i> "+listing.Adresse+"<br />&nbsp;&nbsp;&nbsp;&nbsp;"+listing.CP+" "+listing.Ville+"<hr /><i class=\"far fa-comment\"></i> "+listing.Commentaire;
			var iconEnCouleur = colorIcon["bleu"];
			if (listing.couleur == "jaune" || listing.couleur == "vert" || listing.couleur == "rouge") iconEnCouleur = colorIcon[listing.couleur];
			marker = L.marker([lat, lng],
					{
						title: listing.Nom+" - "+listing.Ville,
						icon :  iconEnCouleur
					} ).bindPopup(popUp);
			tableauMarkers.push(marker);
			etablissements.addLayer(marker);			

			map.fitBounds(bounds, {padding: [50, 50]});
			surLaCarte.push('<div class="listing-liste" id="listing-'+tableauMarkers.indexOf(marker)+'"><a style="cursor:pointer" onClick="montreMarker('+tableauMarkers.indexOf(marker)+')">'+marker.options.title+'</a></div>');
			document.getElementById('liste').innerHTML = surLaCarte.join('\n');
		}); 
	}); 
});

function montreMarker(i) {
	map.flyTo(tableauMarkers[i].getLatLng());
	tableauMarkers[i].openPopup();
};
