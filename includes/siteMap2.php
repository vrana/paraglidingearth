<?php 
include dirname(__FILE__) . '/../member/ASEngine/AS.php';

if (! app('login')->isLoggedIn()) {
	$isMember = false;
	$userName="";
} else {
	$isMember = true;
	$currentUser = app('current_user');
	$userName = e($currentUser->username);
}
?>
<!DOCTYPE html>
<html>
	<head>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.2/leaflet.css">
		<link rel="stylesheet" href="../assets/css/leaflet.awesome-markers.css">

		<style>
			.disabledFormField {
				width: 20%;
				border: none;
				font-style: italic;	
				text-align: right;
			}
			#add_button  {
				padding-top: 42px;
				background: antiquewhite;
				top: -100px;
				position: relative;
				z-index: 10000;
				padding: 0px;
			}
			.editLink {
				cursor: pointer;
				background: antiquewhite;
			}
		</style>

	</head>
	<body>
		<div id="siteMapContainer" style="height: 600px; width: 100%"></div>
		<div>
			<span id="add_button">
				<?php if ($isMember) { ?>
					<a id="add_landing_button" 			href="#" onclick="javascript: markers['landing'] 		 = createMarker(landing)">Add a landing</a><br />
					<a id="add_takeoff_parking_button"  href="#" onclick="javascript: markers['takeoff_parking'] = createMarker(takeoff_parking)">Add a parking at takeoff</a><br />
					<a id="add_landing_parking_button"  href="#" onclick="javascript: markers['landing_parking'] = createMarker(landing_parking)">Add a parking at landing</a>
					<a id="add_extra_item_button"  		href="#" onclick="javascript: markers['extra_item'] 	 = createMarker(extra_item)">Add an extra item</a>
				<?php } else { ?>
					<a href="#" onclick="javascript:">Login to add/edit information</a><br />					
				<?php } ?>
			</span>
		</div>
		<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.2/leaflet.js"></script> 
		<script src="https://cdn.jsdelivr.net/leaflet.esri/1.0.3/esri-leaflet.js" type="text/javascript"></script>
		<script src="../assets/js/leaflet.awesome-markers.min.js"></script>
		
		<script>
			/* **** maps layer and icons *************/

			var esriImagery = L.esri.basemapLayer("Imagery");
			
			var blueStartIcon = L.AwesomeMarkers.icon({
				icon: 'play',
				markerColor: 'darkblue',
				prefix: 'fa'
			}); 
			var greenPauseIcon = L.AwesomeMarkers.icon({
				icon: 'pause',
				markerColor: 'green',
				prefix: 'fa'
			}); 
			var beigeCarIcon = L.AwesomeMarkers.icon({
				icon: 'car',
				markerColor: 'beige',
				prefix: 'fa'
			}); 
			var redDragIcon = L.AwesomeMarkers.icon({
				icon: 'arrows-alt',
				markerColor: 'red',
				prefix: 'fa'
			}); 
			var purpleIcon = L.AwesomeMarkers.icon({
				icon: 'question',
				markerColor: 'purple',
				prefix: 'fa'
			}); 
			
			
			var items = [];    // an array of items, an item beeing an object, present on the map as a marker
		
			
			$.getJSON("../assets/ajax/siteModalJSON.php",{id: id},  function (json) {  // we query the db for site information
				
				siteMap = L.map("siteMapContainer", {
					zoom: 17,
					center: [json.body["lat"], json.body["lng"]],
					layers: [esriImagery]
				});
			
				siteBounds = L.latLngBounds([json.body["lat"], json.body["lng"]], [json.body["lat"], json.body["lng"]]);
				
				takeoff.lat = json.body["lat"];
				takeoff.lng = json.body["lng"];
				takeoff.altitude = json.body["takeoff_altitude"]; 
				takeoff.description = json.body["takeoff"];
				takeoff.name = json.body["takeoff_name"];
				
				
				
			});


		
		
		
		
		
		</script>
