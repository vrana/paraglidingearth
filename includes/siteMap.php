<?php 
include dirname(__FILE__) . '/../member/ASEngine/AS.php';

if (! app('login')->isLoggedIn()) {
	$isMember = false;
	$userName="";
} else {
	$isMember = true;
	$currentUser = app('current_user');
	$userName = e($currentUser->username);
}
?>
<!DOCTYPE html>
<html>
	<head>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.2.0/leaflet.css">
		<link rel="stylesheet" href="../assets/css/leaflet.awesome-markers.css">

		<style>
			.disabledFormField {
				width: 20%;
				border: none;
				font-style: italic;	
				text-align: right;
			}
			#add_button  {
				padding-top: 42px;
				background: antiquewhite;
				top: -100px;
				position: relative;
				z-index: 10000;
				padding: 0px;
			}
			.editLink {
				cursor: pointer;
				background: antiquewhite;
			}
		</style>

	</head>
	<body>
		<div id="siteMapContainer" style="height: 600px; width: 100%"></div>
		<div>
			<span id="add_button">
				<?php if ($isMember) { ?>
					<a id="add_landing_button" 			href="#" onclick="markers['landing'] 		 = createMarker(landing);  markers['landing'].openPopup(); editItem(landing);">
						Add a landing
					</a><br />
					<a id="add_takeoff_parking_button"  href="#" onclick="markers['takeoff_parking'] = createMarker(takeoff_parking);  markers['takeoff_parking'].openPopup(); editItem(takeoff_parking);">
						Add a parking at takeoff
					</a><br />
					<a id="add_landing_parking_button"  href="#" onclick="markers['landing_parking'] = createMarker(landing_parking);  markers['landing_parking'].openPopup(); editItem(landing_parking);">
						Add a parking at landing
					</a><br />
					<a id="add_extra_item_button"  		href="#" onclick="markers['extraItems[0]'] 	 = createMarker(extraItems[0]);  markers['extraItems[0]'].openPopup(); editItem(extraItems[0]);">
						Add an extra item
					</a>

				<?php } else { ?>
					Login to add/edit information<br />					
				<?php } ?>
			</span>
		</div>
		<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.2/leaflet.js"></script> 
		<script src="https://cdn.jsdelivr.net/leaflet.esri/1.0.3/esri-leaflet.js" type="text/javascript"></script>
		<script src="../assets/js/leaflet.awesome-markers.min.js"></script>
		<script>
			
			var id = <?php if ($_GET['id']) echo $_GET['id']; else echo "0" ?>;

            var siteMap;
            var newMarker = [];
            var markers = [];					// an array of markers for markers on the map
            var extraItems   = [];              // an array of objectsfor the extra items
            var extraMarkers = [];				// an array of markers to put on map for exrta items
            var takeoffPopup = $("<span />");
			var landingPopup = $("<span />");
			var takeoffParkingPopup = $("<span />");
			var landingParkingPopup = $("<span />");
			var newOnMap = false;
			var landingOnMap = false;
			var takeoffParkingOnMap = false;
			var landingParkingOnMap = false;
			var buttonContent = '';
			
			/* ****************   Objects that store map items information ************* */
			var takeoff =		  { item:"takeoff", 		lat:0, lng:0, altitude:0, description:"", name:"", id:""};
			var landing =		  { item:"landing", 		lat:0, lng:0, altitude:0, description:"", name:"", id:""};
			var takeoff_parking = { item:"takeoff_parking", lat:0, lng:0, altitude:0, description:"", name:"", id:""};
			var landing_parking = { item:"landing_parking", lat:0, lng:0, altitude:0, description:"", name:"", id:""};
			var extra_item = 	  [] ;    /**** this one is an empty array of map items objects because many items can be added to the map.... ***/
			extraItems[0] =       { item:"extraItems[0]", type:"", id: 0, lat:0, lng:0, altitude:0, description:"", name:""};


/* **** maps layer and icons *************/

			var esriImagery = L.esri.basemapLayer("Imagery");
			
			var blueStartIcon = L.AwesomeMarkers.icon({
				icon: 'play',
				markerColor: 'darkblue',
				prefix: 'fa'
			}); 
			var alternateBlue = L.AwesomeMarkers.icon({
				icon: 'play',
				markerColor: 'blue',
				prefix: 'fa'
			}); 
			var greenPauseIcon = L.AwesomeMarkers.icon({
				icon: 'pause',
				markerColor: 'darkgreen',
				prefix: 'fa'
			}); 
			var alternateGreen = L.AwesomeMarkers.icon({
				icon: 'pause',
				markerColor: 'green',
				prefix: 'fa'
			}); 
			var beigeCarIcon = L.AwesomeMarkers.icon({
				icon: 'car',
				markerColor: 'orange',
				prefix: 'fa'
			}); 
			var alternateBeige = L.AwesomeMarkers.icon({
				icon: 'car',
				markerColor: 'beige',
				prefix: 'fa'
			}); 
			var redDragIcon = L.AwesomeMarkers.icon({
				icon: 'arrows-alt',
				markerColor: 'red',
				prefix: 'fa'
			}); 

			var purpleIcon = L.AwesomeMarkers.icon({
				icon: 'question',
				markerColor: 'purple',
				prefix: 'fa'
			}); 

		
/* ********  editing elements ****************/		
			function submitEditForm() {
				var data = $("#edit_form").serialize();
				// console.log(data);
				var request = $.post("../assets/ajax/updateItem/siteMapSave.php", data, function(data){})
								  .done(function() {
									  console.log('done!');
									  location.reload();
//									  clearMarkers();    // a dirty way to reload markers....  clear all then redraw all
	//								  drawMarkers(0);   
								  })
								  .fail(function() {
									  //console.log( "error" );
								  });
			} 


			function submitExtraEditForm() {
				var data = $("#edit_form").serialize();
				// console.log(data);
				var request = $.post("../assets/ajax/updateItem/siteMapSaveExtraItem.php", data, function(data){})
								  .done(function() {
									  console.log('done!');
									  location.reload();
//									  clearMarkers();    // a dirty way to reload markers....  clear all then redraw all
	//								  drawMarkers(0);   
								  })
								  .fail(function() {
									  //console.log( "error" );
								  });
			} 


			takeoffPopup.on('click', '.editLink' , function () {
				editMarker('takeoff', takeoffMarker, takeoffDescription, takeoffAltitude, takeoffName);
			});			
				

			function getAltitude(lat, lng, divId) {
				$.getJSON('https://secure.geonames.org/srtm1JSON?lat='+lat+'&lng='+lng+'&username=raf', function( data ) {
					$("#"+divId).val(data.srtm1);
				});
			}


			
/* ****** ajax sites loading **************/	

			var siteBounds = L.latLngBounds();		
			
			
			function createMarker(item) {
				
	//			console.log(item.item);
	//			if (item.type) console.log(item.type);
	//			console.log(item.name);
				newItem = false;
				// if item is new, put it on map center
				if (item.lat == 0 && item.lng==0 ) { 
					var centerLatLng = siteMap.getCenter();
					item.lat = centerLatLng.lat;
					item.lng = centerLatLng.lng;
					newItem = true;
				}
				else centerLatLng = [item.lat, item.lng];

				//  wheter or not base items are present and we must show/hide new items creation links 
				if (item.item=='landing') landingOnMap = true;
				if (item.item=='landing_parking') landingParkingOnMap = true;
				if (item.item=='takeoff_parking') takeoffParkingOnMap = true; 

				// hiding buttons for existing items
				if (item.item != "extraItems[0]" )	$("#add_"+item.item+"_button").hide();

				//  icon choice...
				var isAnAlternateItem = false;			//  sets if the item is a regular site one (takeoff, landing, takeogge parking or landing parking or if it is an alternate one..
				if (item.item=='takeoff') var markerIcon = blueStartIcon;
				else if (item.item=='landing') var markerIcon = greenPauseIcon;
				else if (item.item=='takeoff_parking' || item.item=='landing_parking') var markerIcon =  beigeCarIcon;
				else {    //  we got an 'extra item' here....
					isAnAlternateItem = true;
					if (item.type=='takeoff') var markerIcon = alternateBlue;
					else if (item.type=='landing') var markerIcon = alternateGreen;
					else var markerIcon = alternateBeige;
				}

				// lets make a marker
				marker = L.marker(centerLatLng, {icon: markerIcon}).addTo(siteMap);
				
				//   formating text for pop up...
				var popupHTML = "";
				if (item.type) itemType = "alternate "+item.type;
				else itemType = item.item;
				itemType = itemType.replace("_", " ");

				if (item.altitude == 0) altitude = '';
				else altitude = item.altitude + ' m';

				if(item.name != 'null') name = item.name;
				else name = 'no name...';	
				//   end of formatting text		
				
				if ( isAnAlternateItem ) {
			//		popupHTML += "<a  href='#' modalToOpen='reportItemModal' class='openAnotherModal' itemType='"+itemType+"' itemName='"+name+"' itemId='"+item.id+"'><i class='fa fa-trash'></i> delete this item ?</a>";
					popupHTML += "<a  href='../assets/ajax/reportMapItem.php?itemType="+itemType+"&itemName="+name+"&itemId="+item.id+"&siteId="+id+"' ><i class='fa fa-trash'></i> delete this item ?</a>";
				} else if (item.item != 'takeoff') {
					popupHTML += "<a  href='../assets/ajax/reportMapItem.php?itemType="+itemType+"&itemName="+name+"&itemId="+id+"&siteId="+id+"' ><i class='fa fa-trash'></i> delete this item ?</a>"
				}
				popupHTML += "<div id='popupHTML'><em>"+itemType+"</em><br /><b>"+name+"</b>  <em>"+altitude+"</em><br />";
				popupHTML += item.description;
<?php			if ($isMember) {?>
					popupHTML += "<div class='editLink'>";
					popupHTML += "	<a href='#' onClick='editItem("+item.item+")'>";
					popupHTML += "		<i class='fa fa-edit'></i> Edit / <i class='fa fa-arrows-alt'></i> Move";
					popupHTML += "  </a>";
					popupHTML += "</div>";
<?php			}
?>				popupHTML += "</div>";
				
				var popup = marker.bindPopup(popupHTML);

				if (item.lat != 0 && item.lng != 0) {
					siteBounds.extend([item.lat, item.lng]);
					siteMap.flyToBounds(siteBounds);
				}
								
				return marker;
			}	
			
						
			function editItem(item){
				
				markers[item.item].dragging.enable();
				// console.log(item.item+', '+item.lat);
				
				if (item.item.indexOf("extraItem") >= 0) {
					popupContent = "<form id='edit_form' action='javascript:submitExtraEditForm()'> <div class='form-group'>";
					popupContent += "<input type='hidden' id='site_id' name='site_id'  value='"+id+"'   /> ";
					popupContent += "<input type='hidden' id='id' name='id'  value='"+item.id+"'   /> ";
					popupContent += "<div id='itemFormTextFields' style='display: none'> ";
					popupContent += "<label for='typeSelect'>Item type:</label><select id='typeSelect' name='type' class='form-control'> <option>Select...</option> <option value='takeoff'>alternate takeoff</option> <option value='landing'>alternate landing</option> <option  value='parking'>alternate parking</option> </select>";
				}
				else	{
					popupContent = "<form id='edit_form' action='javascript:submitEditForm()'> <div class='form-group'>";
					popupContent += "<input name='item' type='hidden' id='item_type' value='"+item.item+"' /> ";
					popupContent += "<input type='hidden' id='site_id' name='id'  value='"+id+"'   /> ";
					popupContent += "<div id='itemFormTextFields' style='display: none'> ";
				}
				
				if (item.name=='null') itemName = '';
				else itemName = item.name;
				
				popupContent += "<label for='item_name'>"+item.item+" name:</label><input type='text' class='form-control' id='item_name' name='name' placeholder='Name' value='"+itemName+"' />";
				popupContent += "<label for='item_description'>"+item.item+" description:</label><textarea class='form-control' rows='3' cols='57' id='item_description' name='description' placeholder='Description'>"+item.description+"</textarea>";
				popupContent += "<input type='submit' class='btn btn-success' id='submitEditForm'></input>";
				popupContent += "</div></div>";
				popupContent += "<strong><i class='fa fa-arrows-alt'></i> Drag &amp; drop the marker to its correct location.</strong><br />";
				popupContent += "<div class='form-inline'>";
				popupContent += "gps: <input type='text'  class='disabledFormField' id='item_lat' name='lat' readonly='readonly' value='"+item.lat+"' />, ";
				popupContent += "<input type='text'  	  class='disabledFormField' id='item_lng' name='lng' readonly='readonly' value='"+item.lng+"' /> - ";
				popupContent += " alt: <input type='text' class='disabledFormField' id='item_altitude' readonly='readonly' name='altitude' value='"+item.altitude+"' /> m";
				popupContent += "<br /><a href='#' onClick='$(\"#itemFormTextFields\").show(600);'>Dragging done ! It\'s on the right spot !</a>";
				popupContent += "</div>";
				popupContent += "</form>";
				
				markers[item.item].bindPopup( popupContent ).openPopup();
				
				if (item.item.indexOf("extraItem") >= 0)  $("#typeSelect").val(item.type);
				if (item.altitude==0) getAltitude(markers[item.item].getLatLng().lat, markers[item.item].getLatLng().lng, 'item_altitude');
				
				var description_temp='';
				
				markers[item.item].on('dragstart', function(){
					description_temp = $('#item_description').val();
				});
				
				markers[item.item].on('dragend', function(){
					//console.log(markers[item.item].getLatLng().lat);
					markers[item.item].openPopup();
					$('#item_lat').val(markers[item.item].getLatLng().lat);
					$('#item_lng').val(markers[item.item].getLatLng().lng);
					$('#item_description').val(description_temp);
					getAltitude(markers[item.item].getLatLng().lat, markers[item.item].getLatLng().lng, 'item_altitude');
				});
			}
			



			drawMarkers(1);

			
			
			function drawMarkers(firstTime){
							
				$.getJSON("../assets/ajax/siteModalJSON.php",{id: id},  function (json) {
					
					if (firstTime==1){
						siteMap = L.map("siteMapContainer", {
							zoom: 17,
							center: [json.body["lat"], json.body["lng"]],
							layers: [esriImagery]
						});
					}
				
					siteBounds = L.latLngBounds([json.body["lat"], json.body["lng"]], [json.body["lat"], json.body["lng"]]);

					takeoff.id = json.body["id"];
					takeoff.lat = json.body["lat"];
					takeoff.lng = json.body["lng"];
					takeoff.altitude = json.body["takeoff_altitude"]; 
					takeoff.description = json.body["takeoff"];
					takeoff.name = json.body["takeoff_name"]; 
					markers['takeoff'] = createMarker(takeoff);
					
					$("#add_takeoff_parking_button").hide();
					$("#add_landing_parking_button").hide();
					
					if (json.body["landing_lat"] != 0 & json.body["landing_lng"] != 0) {
						landing.id  = json.body["id"];
						landing.lat = json.body["landing_lat"];
						landing.lng = json.body["landing_lng"];
						landing.altitude = json.body["landing_altitude"]; 
						landing.description = json.body["landing"];
						landing.name = json.body["landing_name"]; 
						markers['landing'] = createMarker(landing);					
					} else {
						$("#add_takeoff_parking_button").show();
						//buttonContent += '<a id="add_landing_button" href="#" onclick="javascript: landingMarker = createMarker('+landing+')">Add a landing</a><br />'; 
						//$("#add_button").html(buttonContent);
					}
					
					if(json.body["takeoff_parking_lat"] != 0 & json.body["takeoff_parking_lng"] != 0) {
						takeoff_parking.id  = json.body["id"];
						takeoff_parking.lat = json.body["takeoff_parking_lat"];
						takeoff_parking.lng = json.body["takeoff_parking_lng"];
						takeoff_parking.altitude = json.body["takeoff_parking_altitude"]; 
						takeoff_parking.description = json.body["takeoff_parking_description"];
						takeoff_parking.name = json.body["takeoff_parking_name"]; 
						markers['takeoff_parking'] = createMarker(takeoff_parking);
					} else {
						$("#add_takeoff_parking_button").show();
						//buttonContent += '<a id="add_takeoff_parking_button" href="#" onclick="javascript: takeoffParkingMarker = createMarker('+takeoff_parking+')">Add a parking at takeoff</a><br />'; 					
						//$("#add_button").html(buttonContent);
					}
			
					if(json.body["landing_parking_lat"] != 0 & json.body["landing_parking_lng"] != 0) {
						landing_parking.id  = json.body["id"];
						landing_parking.lat = json.body["landing_parking_lat"];
						landing_parking.lng = json.body["landing_parking_lng"];
						landing_parking.altitude = json.body["landing_parking_altitude"]; 
						landing_parking.description = json.body["landing_parking_description"];
						landing_parking.name = json.body["landing_parking_name"]; 
						markers['landing_parking'] = createMarker(landing_parking);
					} else {
						$("#add_landing_parking_button").show();
						//buttonContent += '<a id="add_landing_parking_button" href="#" onclick="javascript: landingParkingMarker = createMarker('+landing_parking+')">Add a parking at landing</a><br />'; 					
						//$("#add_button").html(buttonContent);
					}
					
					//markers['takeoff'].openPopup();
				});
				
				$.getJSON("../assets/ajax/siteExtraItemsJSON.php",{siteID: id},  function (result) {
					  extraCount = 0;
					  $.each( result.item, function( key, val ) {
						  extraItems[val.id] = { 
							  item:"extraItems["+val.id+"]", 
							  type:val.type,
							  id: val.id, 
							  lat:val.lat, 
							  lng:val.lng, 
							  altitude:val.altitude, 
							  description:val.description, 
							  name:val.name
							  };
						  //extraItems.push(extraItem);
						  markers['extraItems['+val.id+']'] = createMarker(extraItems[val.id]); 
						  ;
					  });			
				});
			}
			
			
			
			function clearMarkers(){
				
				console.log('clearing...?');

				siteMap.removeLayer(markers['takeoff']);
						console.log("clearing takeoff");
				siteMap.removeLayer(markers['landing']);  
						console.log("clearing landing");
				siteMap.removeLayer(markers['takeoff_parking']); 
						console.log("clearing TO parking");
				siteMap.removeLayer(markers['landing_parking']); 
						console.log("clearing lz parking");
				console.log(extraItems);
				$.each(extraItems, function (key, value) {
					console.log ('here '+key); 
					if ( extraItems[key] ) {
						console.log(key);
						siteMap.removeLayer(markers['extraItems['+key+']']);
						console.log("clearing extra "+key);
					}
				});
			}
			
			
			$(".editLink").on('click', function(){
				//console.log('youpi');
			});
		</script>
	</body>
</html>
	
