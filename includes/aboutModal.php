    <div class="modal fade" id="aboutModal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="modal-title">Welcome to Paragliding.Earth !</h3>
          </div>
          
          <div class="modal-body">
			  
            <ul class="nav nav-tabs" id="aboutTabs">
              <li class="active"><a href="#disclaimer" data-toggle="tab" style="color:darkred;"><i class="fa fa-exclamation-triangle"></i>&nbsp;Disclaimer</a></li>
              <li><a href="#about" data-toggle="tab"><i class="fa fa-question-circle"></i>&nbsp;About this website</a></li>
<!--              <li><a href="#contact" data-toggle="tab"><i class="fa fa-pencil"></i>&nbsp;Contact</a></li> -->
              <li><a href="#credits" data-toggle="tab"><i class="fa fa-copyright"></i>&nbsp;Credits</a></li>
<!--              <li><a href="#help" data-toggle="tab"><i class="fa fa-ambulance"></i>&nbsp;Help us!!!</a></li> -->
              <li><a href="#privacy" data-toggle="tab"><i class="fa fa-lock"></i>&nbsp;Privacy</a></li>
<!--              <li><a href="#original" data-toggle="tab"><i class="fa fa-question"></i>&nbsp;What happened to the good old PGE website ?</a></li>  -->
            </ul>
            
            <div class="tab-content" id="aboutTabsContent">
				
              <div class="tab-pane fade" id="about">
                <p>A work in progress to make a simple <strong>map based collaborative database of paragliding sites, clubs and pros worldwide</strong>.
                </p>
                <div class="panel panel-primary">
                  <div class="panel-heading">What you can find here for the moment (as of jun. 2018)</div>
                  <ul class="list-group">
                    <li class="list-group-item">Fullscreen mobile-friendly map showing all pgEarth paragliding sites, pros and clubs as you pan and zoom</li>
                    <li class="list-group-item">Search on flying sites or cities names</li>
                    <li class="list-group-item">Sites details and pictures, clubs and pros</li>
                    <li class="list-group-item">Sites maps: showing site details : main landings and parkings, but also alternate ones for sites with multiple takeoffs and/or landings...</li>
                    <li class="list-group-item">Register/Log in + add + edit information about sites, clibs, pros.<br />
                    PgEarth is editable again !! Login and help us get the most up to date data !<br />
                    Former pgEarth members who had a valid email along with their account can use the "forgot your password?" feature in order to login to the new site.</li>
                    <li class="list-group-item">Export to gpx: export sites in the viewport into a "gpx" file.</li>
                  </ul>
                 <div class="panel-heading">What you will not find here for the moment</div>
                  <ul class="list-group">
                    <li class="list-group-item">'Advanced' search sites filters</li>
                    <li class="list-group-item">For sure plenty of many things i can't think of... :(</li>
                    <li class="list-group-item">If you want help develop some features, welcome ! : <a href="https://framagit.org/raph-tr/paraglidingearth" target="_blank">the project source code is hosted on framagit.</a> Feel free to join !</li>
                  </ul>
                </div>
              </div>
              
              <div id="disclaimer" class="tab-pane fade text-danger alert-danger active in">
				<p>The data provided on this site is for informational and planning purposes only.</p>
				<p>Absolutely no accuracy or completeness guarantee is implied or intended. All information on this website is for informational purpose only and must not be used and trusted 'as is'. Please understand that it may be outdated, unclear, or simply wrong !</p>
				<p>We hope you are aware that you can not trust anything you read on the internet: the website owner can not be held responsible for any decision taken on the basis of the information presented here : please always consider getting fresh and official legal information from the flying sites local people, clubs and/or authorities and consider the weather carefully before you go fly !</p>
				<p>This beeing said, fly happy and safe ! ;)</p>
              </div>
              
<!--
              <div id="original" class="tab-pane fade">
                <p>The original pgearth website suffered severe stability problems the last months and could not be hosted on the former server anymore.</p>
                <p>Since we had to migrate the whole stuff, we took the chance for a rebuilt, it will bring its inconveniences, but hoppefully it will be for the best..</p>
              </div>
-->

              
 <!--           <div class="tab-pane fade" id="contactBCKUP">
				  
				
			
			  !!!!!!!!!!!!!
			  
			    the form content is loaded
				from a function in assets/js/pge_utils_functions.js 
				
				
			  !!!!!!!!!!!!!
			  
                
                
                
                <p><i class="fa fa-envelope"></i>&nbsp;<em>raphael /at/ disroot /dot/ org</em></p>
            </div>
-->			
              <div class="tab-pane fade" id="credits">
                <p><img alt="Creative Commons License" style="margin: 6px" src="assets/img/cc-by-sa-80x15.png" /><br />The content here is  licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-Share Alike 3.0 Unported License</a>.</p>
                <p>Website built with <a href="http://leafletjs.com/" target="_blank">Leaflet</a>, from a <a href="https://github.com/bmcbride/bootleaf" target="_blank">bootleaf</a> basis.</p>
                <p>A big thank you to the <a href="#" class="openAnotherModal" modalToOpen="featureModal" feature="club" id="1">"Ducks Parapente" pg club in Grenoble</a> - fr, for offering hosting to the project.</p>
                <p><a href="http://gucparapente.fr" target="_blank"><img src="assets/img/header-guc.jpg"><i class="fa fa-external-link"></i></a></p>
                <p>A big thank you also to paraglidingforum.com for having kindly offered pgearth a free hosting for over a decade ! Bless you, guys !</p>
                <p>And of course a big thank you to all contributors !! :)</p>
              </div>
              
              <div class="tab-pane fade" id="privacy">
				<p>We care about your privacy : our code is google tools and facebook buttons free so no-one at gafam will ever track you here !<br />Don't thank us, it's our pleasure !</p>
				<p>This beeing said, we tried our best, but they must know you're here anyway... ;)</p>
				<p>We also try to use as few cookies as possible, which is why you have to login at every connection, sorry for the inconveniance.</p>
			  </div>
				

<!--
             <div class="tab-pane fade" id="helpBCKUP">
				 <div class="panel panel-primary">
					<div class="panel-heading">You like PgEarth ? YOU CAN HELP US !</div>
						<ul class="list-group">
							<li class="list-group-item"><i class="fa fa-pencil-square-o"></i> <strong>by editing the information</strong>: log in and help keeping the information up to date. The website is efficient only if people like you keep the information correct.</li>
							<li class="list-group-item"><i class="fa fa-bullhorn"></i> <strong>by reporting errors</strong>: no need to be logged in : report to the moderators the information that you think should be deleted (trash icons).</li>
							<li class="list-group-item"><i class="fa fa-share-alt"></i> <strong>by bringing your friends</strong>: the more the merrier. Advertise around you ! (We are not big fans of social networks, but if you are, go ahead and give us some buzz! :)</li>
							<li class="list-group-item"><i class="fa fa-commenting-o"></i> <strong>by translating/improving the english</strong>: information on pgearth is meant to be readen by as many people as possible, that is why we chose to publish in english : translate the data fields if you can or contact us to improve the english of the website itself.</li>
							<li class="list-group-item"><i class="fa fa-balance-scale"></i> <strong>by beeing a moderator</strong>: help us keep the data clean by deleting incorrect information.</li>
							<li class="list-group-item"><i class="fa fa-laptop"></i> <strong>by joining the code git</strong>: the code git <a href="https://framagit.org/raph-tr/paraglidingearth" target="_blank">is here</a>.</li>
							<li class="list-group-item"><i class="fa fa-coffee"></i> <strong>by offering a coffee</strong>: to be frank we already have our own coffee, but that would help with hosting, domain names, bought scripts, etc. <a href="#" onClick="toggleFinances()">Have a look at our finances</a>
							<div id="finances" class="alert-info" style="display:none">
								<h4>2017 pgearth finances</h4>
								<div class="meter orange">
									<span style="width: 42%"></span>
								</div>
								 
								<table class="table table-striped table-bordered table-hover table-condensed" >
									<thead>
										<tr>
											<th>Expenses</th>
											<th>&euro;</th>
											<th>Income</th>
											<th>&euro;</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>hosting</td>
											<td>43</td>								
											<td>hosting offered by <a href="/?club=1">guc parapente</a></td>
											<td>43</td>
										</tr>
										<tr>
											<td>domain name pgearth.com</td>
											<td>10</td>								
											<td>ads on the site</td>
											<td>0</td>
										</tr>
										<tr>
											<td>domain name pg.earth</td>
											<td>31</td>								
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td>membership script bought on code canyon</td>
											<td>13</td>								
											<td></td>
											<td></td>
										</tr>
										<tr>
											<th>total</th>
											<th>97</th>								
											<th></th>
											<th>43</th>
										</tr>
									</tbody>
								</table>
								<p>Funded : 42%, missing 51 &euro;, updated : sept. 2017<br /><a href="https://paypal.me/pgearth"><i class="fa fa-paypal"></i> pgearth on paypal</a>.</p>
							</div>
							</li>
					</ul>
				</div>
				
			  </div>
-->				

              
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

<style>
.meter { 
	height: 13px;  /* Can be anything */
	position: relative;
	background: #555;
	-moz-border-radius: 25px;
	-webkit-border-radius: 25px;
	border-radius: 25px;
	padding: 4px;
	box-shadow: inset 0 -1px 1px rgba(255,255,255,0.3);
}

.meter > span {
  display: block;
  height: 100%;
  border-top-right-radius: 8px;
  border-bottom-right-radius: 8px;
  border-top-left-radius: 20px;
  border-bottom-left-radius: 20px;
  background-color: rgb(43,194,83);
  background-image: linear-gradient(
    center bottom,
    rgb(43,194,83) 37%,
    rgb(84,240,84) 69%
  );
  box-shadow: 
    inset 0 2px 9px  rgba(255,255,255,0.3),
    inset 0 -2px 6px rgba(0,0,0,0.4);
  position: relative;
  overflow: hidden;
}
.orange > span {
  background-color: #f1a165;
  background-image: linear-gradient(to bottom, #f1a165, #f36d0a);
}

</style>
