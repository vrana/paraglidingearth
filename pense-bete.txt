pour appliquer le stash et récupérer les mots de passe dans les fichiers concernés

  git stash apply stash@{0}

avant de committer, on supprime les modifications avec les mots de passe

  git checkout -- connexion.php
  git checkout -- member/ASEngine/ASConfig.php 
