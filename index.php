<?php 
include dirname(__FILE__) . '/member/ASEngine/AS.php';

if (! app('login')->isLoggedIn()) {
	$isMember = false;
	$userName="";
	$_SESSION['username'] = "";
	
	app('register')->botProtection();

//    redirect("login.php");
} else {
	$isMember = true;
	$currentUser = app('current_user');
	$userName = e($currentUser->username);
	$_SESSION['username'] = $userName;
	$_SESSION['userId'] = ASSession::get('user_id');
	//echo "<script>console.log('username : ".app('current_user')." / ".ASSession::get('userName')."- id:". ASSession::get('user_id')."');</script>";
}
?><!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1,user-scalable=no,maximum-scale=1,width=device-width">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="theme-color" content="#000000">
    <meta name="title" content="paragliding earth, free flights sites database, worldwide, collaborative.">
    <meta name="description" content="paragliding earth is an open, collaborative, worldwide database of free flight sites.">
    <meta name="author" content="pgEarth">
    <title>Paragliding Earth</title>
    <meta name="google-site-verification" content="ZQ9i3-VeKP3SjNNHtVUkRFKg94g5uLVnDFhL-7h9bP8" />
    <meta name="google-site-verification" content="EUGbno1LjP68UhZmBjmc09v4VccaTHMapTkoFbwYBEU" />

<?php if ( ! $isMember ) { ?>
 <!--       <link rel='stylesheet' href='member/ASLibrary/css/style3.css' type='text/css' media='all' />  -->
<?php } ?>

    <link rel="stylesheet" href="assets/external-scripts/lightbox/lightbox.min.css">
    <link rel="stylesheet" href="assets/css/localhost/bootstrap.min.css">
<!--    <link rel="stylesheet" href="assets/css/localhost/font-awesome.min.css">  -->
    <link href="assets/css/fa/css/all.min.css" rel="stylesheet">
    <link href="assets/css/fa/css/v4-shims.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/localhost/leaflet.css">
<!--    <link rel="stylesheet" href="assets/css/leaflet.1.2.css">-->
    <link rel="stylesheet" href="assets/css/localhost/MarkerCluster.css">
    <link rel="stylesheet" href="assets/css/localhost/MarkerCluster.Default.css">
    <link rel="stylesheet" href="assets/css/localhost/L.Control.Locate.css">
    <link rel="stylesheet" href="assets/leaflet-groupedlayercontrol/leaflet.groupedlayercontrol.css">
	<link rel="stylesheet" href="assets/css/localhost/bootstrap-editable.css"/>
    <link rel="stylesheet" href="assets/css/app.css">
    <link rel="stylesheet" href="assets/css/hoverbox.css">
    <link rel="stylesheet" href="assets/css/leaflet.label.css">
	<link rel="stylesheet" href="assets/css/leaflet.awesome-markers.css">
  <link rel="stylesheet" href="assets/css/toggle.css">



    <link rel="stylesheet" href="assets/css/pge.css">
    <link rel="stylesheet" href="assets/css/pge_windy.css">
    <link rel="stylesheet" href="assets/css/pge_siteFlyableTab.css">

    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon-76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicon-120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicon-152.png">
    <link rel="icon" sizes="196x196" href="assets/img/favicon-196.png">
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico">
    <link rel="icon" type="image/png" href="assets/img/icon16.png" />
    

  </head>

  <body>
	  
  <!--******* navbar *****************-->  
  <?php include("includes/navbar.php"); ?>

  <!--******* windy overlay *****************-->  
  <div class="windyContainer">
	<div class="windyClose"><a herf="#" onclick="myToggleWindy.toggle(false, false, false);"><img src="assets/img/windyClose.png"></a></div>
    <div id="windy" >
    </div>
  </div>



	<!--******* sidebar *****************-->  
	<div id="sidebar">
		<div class="sidebar-wrapper">
		  <div class="panel panel-default" id="features">
			<div class="panel-heading">
			  <h3 class="panel-title">Sites list
			</div>
			<div class="panel-body">
			  <div class="row">
				<div class="col-xs-8 col-md-8">
				  <input type="text" class="form-control search" placeholder="Filter" />
				</div>
				<div class="col-xs-4 col-md-4">
				  <button type="button" class="btn btn-primary pull-right sort" data-sort="feature-name" id="sort-btn"><i class="fa fa-sort"></i>&nbsp;&nbsp;Sort</button>
				</div>
			  </div>
			  <button type="button" class="btn btn-xs btn-default pull-right" id="sidebar-hide-btn"><i class="fa fa-chevron-left"></i></button></h3>
			</div>
			<div class="sidebar-table">
			  <table class="table table-hover" id="feature-list">
				<thead class="hidden">
				  <tr>
					<th>Icon</th>
				  </tr>
				  <tr>
					<th>Name</th>
				  </tr>
				  <tr>
					<th>Chevron</th>
				  </tr>
				</thead>
				<tbody class="list">
					<tr class="feature-row">
						<td></td>
						<td style="vertical-align: middle;">zoom in to get items list</td>
						<td style="vertical-align: middle;"></td>
					</tr>
				</tbody>
			  </table>
			</div>
		  </div>
		</div>
	</div>


	<!--******* map container *****************-->  
    <div id="container">
      <div id="map"></div>
    </div>
    
    
    
    <div id="loading">
      <div class="loading-indicator">
        <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-info progress-bar-full"></div>
        </div>
      </div>
    </div>
    
    <div id="loadingWeather">
    </div>


	<nav class="navbar navbar-fixed-bottom navbar-light bg-light">
		<div class="container">
			<div class="navbar-header navbar-brand">New cool features on PgEArth !</div>
			<div class="" style="clear: both;">
				<ul class="navbar-nav nav navbar-bottom">
					<li><a> Is a site flyable ? now ? later? </a></li>
					<li><a> Filter sites </a></li>
					<li><a> Show wind overlay </a></li>
					<li><a><button id="closeBottomNavbar" type="button" class="btn btn-default">Close</button></a></li>
				</ul>
			</div>
		</div>
	</nav>

<!--******* modals  *****************-->  

	<!--******* "splash screen" modal  *****************-->  
	<?php
		include("includes/splashModal.php");
	?>

	<!--******* "about" modal  *****************-->  
	<?php
		include("includes/aboutModal.php");
	?>
	
	<!--******* "contact" modal  *****************-->  
	<?php
		include("includes/contactModal.php");
	?>
	
	<!--******* "site" modal  *****************-->  
	<?php
		include("includes/siteModal.php");
	?>

	<!--******* "week news" modal  *****************-->  
    <div class="modal fade" id="newsModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">This week news : what has been new on pgEarth this last week ?</h4>
          </div>
          <div class="modal-body" id="newsModalBody">

            <ul class="nav nav-tabs" id="newsTabs">
              <li class="active"><a href="#newsModalModifications" data-toggle="tab"><i class="fa fa-edit"></i>&nbsp;Site modifications</a></li>
              <li>				 <a href="#picturesModalModifications" data-toggle="tab"><i class="fa fa-camera"></i>&nbsp;Pictures</a></li>           
              <li>				 <a href="#membersModalModifications" data-toggle="tab"><i class="fa fa-user"></i>&nbsp;Members</a></li>
              <li>				 <a href="#clubsModalModifications" data-toggle="tab"><i class="fa fa-users"></i>&nbsp;Clubs</a></li>
              <li>				 <a href="#prosModalModifications" data-toggle="tab"><i class="fa fa-thumbs-o-up"></i>&nbsp;Pros</a></li>
            </ul>

            <div class="tab-content" id="newsTabsContent">
              <div class="tab-pane fade active in" id="newsModalModifications">
                <p style="text-align=center"><i class="fa fa-spinner fa-spin"></i> sites modifications.</p>
              </div>
              <div class="tab-pane fade" id="picturesModalModifications">
                <p style="text-align=center"><i class="fa fa-spinner fa-spin"></i> new pictures. (coming soon (?)...)</p>
              </div>
              <div class="tab-pane fade" id="membersModalModifications">
                <p style="text-align=center"><i class="fa fa-spinner fa-spin"></i> new members. (coming soon (?)...)</p>
              </div>
              <div class="tab-pane fade" id="clubsModalModifications">
                <p style="text-align=center"><i class="fa fa-spinner fa-spin"></i> clubs modifications. (coming soon (?)...)</p>
              </div>
              <div class="tab-pane fade" id="prosModalModifications">
                <p style="text-align=center"><i class="fa fa-spinner fa-spin"></i> pros modifications. (coming soon (?)...)</p>
              </div>            
            </div>    <!-- /.tab-content -->
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


	<!--******* "member" modal  *****************-->  
    <div class="modal fade" id="memberModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Member details</h4>
          </div>
          <div class="modal-body" id="memberModalBody">
			  
            <ul class="nav nav-tabs" id="newsTabs">
              <li class="active"><a href="#memberModalDetails" data-toggle="tab"><i class="fa fa-user"></i>&nbsp;Profile</a></li>
              <li>				 <a href="#memberModalActivity" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Activity</a></li>           
              <li>				 <a href="#memberModalPictures" data-toggle="tab"><i class="fa fa-camera"></i>&nbsp;Pictures</a></li>
              <li>				 <a href="#memberModalContact" data-toggle="tab"><i class="fa fa-envelope-o"></i>&nbsp;Contact</a></li>
            </ul>

            <div class="tab-content" id="memberModalContent">
              <div class="tab-pane fade active in" id="memberModalDetails">
                <p style="text-align=center"><i class="fa fa-spinner fa-spin"></i> details</p>
              </div>
              <div class="tab-pane fade" id="memberModalActivity">
                <p style="text-align=center"><i class="fa fa-spinner fa-spin"></i> activity</p>
              </div>
              <div class="tab-pane fade" id="memberModalPictures">
                <p style="text-align=center"><i class="fa fa-spinner fa-spin"></i> pictures</p>
              </div>
              <div class="tab-pane fade" id="memberModalContact">
                <p style="text-align=center"><p>&nbsp;</p><p><i class="fa fa-spinner fa-spin"></i> contact form &rarr; to be done... work in progress</p><p>&nbsp;</p>
              </div>
            </div>    <!-- /.tab-content -->
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>

        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


	<!--******* "teleportation" modal  *****************-->  
    <div class="modal fade" id="teleportModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Teleport to GPS coordinates</h4>
          </div>
          <div class="modal-body">
            <p>If you know a gps point and want to magically jump to it, just use the form below.</p>

			<script>
				function setLatLng() {
					document.getElementById("teleportLatlng").value = document.getElementById("teleportLat").value + "," + document.getElementById("teleportLng").value ;
				}
			</script>
			
			<div class="row">
				<div class="col-md-12">
					<div class="well well-sm">
						<div  id="returnmessage"></div>
						<form class="form-horizontal" name="formTeleport">
							<fieldset>
								
								<legend>
									Enter GPS coordinates below :
								</legend>
			
								<div class="form-group">
									<span class="col-md-12"><i class="fa fa-ellipsis-h"></i> Latitude</span>
									<div class="col-md-12">
										<input id="teleportLat" type="text" placeholder="* lat in decimal fomat" class="form-control" onChange="setLatLng()">
									</div>
								</div>
		 
								<div class="form-group">
									<span class="col-md-12"><i class="fa fa-ellipsis-v"></i> Longitude</span>
									<div class="col-md-12">
										<input id="teleportLng" type="text" placeholder="* lng in decimal fomat" class="form-control" onChange="setLatLng()">
									</div>
								</div>

							   <div class="form-group">
									<span class="col-md-12"><i class="fa fa-ellipsis-h"></i>,<i class="fa fa-ellipsis-v"></i>  or if you have it in "lat,lng" format, put it here:</span>
									<div class="col-md-12">
										<input id="teleportLatlng" name="latlng" type="text" placeholder="lat,lng in decimal fomat, eg. '45.678,4.5678'" class="form-control">
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-12 text-center">
										<input type="button" id="submitTeleport" onClick="teleportTo(document.getElementById('teleportLatlng').value)" class="btn btn-primary btn-lg" value="Fly !" />
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
			            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



	<!--******* "report item" modal  *****************-->  
    <div class="modal fade" id="reportItemModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Report content to moderators</h4>
          </div>
          <div class="modal-body">
            <p>Use the form below to report inapropriate content to moderators.</p>
            <div id="reportItemForm"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->	
    </div><!-- /.modal -->

<?php		if ( $currentUser->role_id > 1 /*is admin or moderator*/) {    ?>
	<!--******* "reported items" modal => admins only *****************-->  
    <div class="modal fade" id="reportedModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Reported content (admins only zone !)</h4>
          </div>
          <div class="modal-body">
            <div id="reportedItems">Here should be the list of reported items... (or tabs for sites/clubs/pros/etc..)</div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php 		} ?>


<?php if ( ! $isMember ) { ?>
	<!--******* "login" modal  ***************** -->
           <div class="modal fade" id="loginModal"  tabindex="-1" >
                <div class="modal-dialog" >
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3>pgEarth user: Sign in / Register</h3>
                        </div>
                        <div class="modal-body">
							<div class="alert alert-warning" style="margin-bottom: 15px;">Former pgEarth members who had a valid email as part of their account details can use the "Forgot Password?" feature to 
							get a new login password.
							</div>
							
							
                            <div class="well">

                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#login" data-toggle="tab"><?= trans('login'); ?></a></li>
                                    <li><a href="#create" data-toggle="tab"><?= trans('create_account'); ?></a></li>
                                    <li><a href="#forgot" data-toggle="tab"><?= trans('forgot_password'); ?></a></li>
                                </ul>

                                <div class="tab-content">
                                    <!-- start: Login Tab -->
                                    <div class="tab-pane active in" id="login">
                                        <form class="form-horizontal">
                                            <fieldset>
                                                <div id="legend">
                                                    <legend class=""><?= trans('login'); ?></legend>
                                                </div>

                                                <!-- start: Username -->
                                                <div class="control-group form-group">
                                                    <label class="control-label col-lg-4"  for="login-username">
                                                        <?= trans('username'); ?>
                                                    </label>
                                                    <div class="controls col-lg-8">
                                                      <input type="text" id="login-username" name="username"
                                                             class="input-xlarge form-control"> <br />
                                                    </div>
                                                </div>
                                                <!-- end: Username -->

                                                <!-- start: Password -->
                                                <div class="control-group form-group">
                                                    <label class="control-label col-lg-4" for="login-password">
                                                        <?= trans('password'); ?>
                                                    </label>
                                                    <div class="controls col-lg-8">
                                                        <input type="password" id="login-password"
                                                               name="password" class="input-xlarge form-control">
                                                    </div>
                                                </div>
                                                <!-- end: Password -->
                                                
                                                <input type="hidden" name="bounds" id="loginFormBoundsField" />

                                                <div class="control-group form-group">
                                                    <div class="controls col-lg-offset-4 col-lg-8">
                                                    <button id="btn-login" class="btn btn-success">
                                                        <?= trans('login'); ?>
                                                    </button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                                    <!-- end: Login Tab -->

                                    <!-- start: Registration Tab -->
                                    <div class="tab-pane fade" id="create">
                                        <form class="form-horizontal register-form" id="tab">
                                            <fieldset>
                                                <div id="legend">
                                                    <legend class=""><?= trans('create_account'); ?></legend>
                                                </div>

                                                <div class="control-group  form-group">
                                                    <label class="control-label col-lg-4" for='reg-email' >
                                                        <?= trans('email'); ?> <span class="required">*</span>
                                                    </label>
                                                    <div class="controls col-lg-8">
                                                        <input type="text" id="reg-email" class="input-xlarge form-control">
                                                    </div>
                                                </div>

                                                <div class="control-group  form-group">
                                                    <label class="control-label col-lg-4" for="reg-username">
                                                        <?= trans('username'); ?> <span class="required">*</span>
                                                    </label>
                                                    <div class="controls col-lg-8">
                                                        <input type="text" id="reg-username" class="input-xlarge form-control">
                                                    </div>
                                                </div>

                                                <div class="control-group  form-group">
                                                    <label class="control-label col-lg-4" for="reg-password">
                                                        <?= trans('password'); ?> <span class="required">*</span>
                                                    </label>
                                                    <div class="controls col-lg-8">
                                                        <input type="password" id="reg-password" class="input-xlarge form-control">
                                                    </div>
                                                </div>

                                                <div class="control-group  form-group">
                                                    <label class="control-label col-lg-4" for="reg-repeat-password">
                                                        <?= trans('repeat_password'); ?> <span class="required">*</span>
                                                    </label>
                                                    <div class="controls col-lg-8">
                                                        <input type="password" id="reg-repeat-password"
                                                               class="input-xlarge form-control">
                                                    </div>
                                                </div>

                                                <div class="control-group  form-group">
                                                    <label class="control-label col-lg-4" for="reg-bot-sum">
                                                        <?php echo ASSession::get("bot_first_number"); ?> +
                                                        <?php echo ASSession::get("bot_second_number"); ?>
                                                        <span class="required">*</span>
                                                    </label>
                                                    <div class="controls col-lg-8">
                                                        <input type="text" id="reg-bot-sum" class="input-xlarge form-control">
                                                    </div>
                                                </div>

                                                <div class="control-group  form-group">
                                                    <div class="controls col-lg-offset-4 col-lg-8">
                                                        <button id="btn-register" class="btn btn-success">
                                                            <?= trans('create_account'); ?>
                                                        </button>
                                                    </div>
                                                </div>
                                           </fieldset>
                                        </form>
                                    </div>
                                    <!-- end: Registration Tab -->

                                    <!-- start: Forgot Password Tab -->
                                    <div class="tab-pane in" id="forgot">
                                        <form class="form-horizontal" id="forgot-pass-form">
                                            <fieldset>
                                                <div id="legend">
                                                    <legend class=""><?= trans('forgot_password'); ?></legend>
                                                </div>
                                                <div class="control-group form-group">
                                                    <label class="control-label col-lg-4" for="forgot-password-email">
                                                        <?= trans('your_email'); ?>
                                                    </label>
                                                    <div class="controls col-lg-8">
                                                        <input type="email" id="forgot-password-email"
                                                               class="input-xlarge form-control">
                                                    </div>
                                                </div>

                                                <div class="control-group form-group">
                                                    <div class="controls col-lg-offset-4 col-lg-8">
                                                        <button id="btn-forgot-password" class="btn btn-success">
                                                            <?= trans('reset_password'); ?>
                                                        </button>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <!-- end: Forgot Password Tab -->


                                </div>
                            </div>
                           
                        </div>
           <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
                   </div> 
                </div>
            </div>
	
<?php	} else  {
?>
	<div class="modal modal-visible fade" id="loginModal">
		<div class="modal-dialog" >
			
			<div class="modal-content">
				<div class="modal-header">
					<h3><a href="../">PgEarth</a> &gt; Your profile</h3>
				</div>
				
				<div class="modal-body">

					<div class="profile-details-wrapper content well">

						<?php if (! $currentUser->is_admin) : ?>
						<div class="alert alert-warning" style="margin-top: 30px;">
							<strong><?= trans('note'); ?>! </strong>
							<?= trans('to_change_email_username'); ?>
						</div>
						<?php endif; ?>


						<ul class="nav nav-tabs" style="margin-top:15px">
							<li class="active">	<a data-toggle="tab" href="#yourDetails">Your details</a></li>
							<li>				<a data-toggle="tab" href="#changePassword">Change password</a></li>
						</ul>

						<div class="tab-content">

							<div id="yourDetails" class="tab-pane fade in active">

								<form class="form-horizontal no-submit" id="form-details">
									<fieldset>

										<!-- Form Name -->
										<legend><?php echo trans('your_details'); ?>
										</legend>

										<!-- Text input-->
										<div class="control-group form-group">
											<label class="control-label col-lg-4" for="first_name">
											<?php echo trans('first_name'); ?>
											</label>
											<div class="controls col-lg-8">
												<input id="first_name" name="first_name" type="text"
												value="<?php echo e($currentUser->first_name); ?>"
												class="input-xlarge form-control">
											</div>
										</div>

										<!-- Text input-->
										<div class="control-group form-group">
												<label class="control-label col-lg-4" for="last_name">
											<?php echo trans('last_name'); ?>
											</label>
											<div class="controls col-lg-8">
												<input id="last_name" name="last_name" type="text"
												value="<?php echo e($currentUser->last_name); ?>"
												class="input-xlarge form-control">
											</div>
										</div>

										<!-- Select input-->
										<div class="control-group form-group">
											<label class="control-label col-lg-4" for="country">
												<?php echo 'Country'; ?>
											</label>
											<div class="controls col-lg-8">

												<?php $iso = $currentUser->country; ?>

												<select name="country" id="country" class="select-xlarge form-control">
													<option value="0" label="Select a country ... " selected="selected">Select a country ... </option>
													<option value="AF" label="Afghanistan">Afghanistan</option>
													<option value="AL" label="Albania">Albania</option>
													<option value="DZ" label="Algeria">Algeria</option>
													<option value="AS" label="American Samoa">American Samoa</option>
													<option value="AD" label="Andorra">Andorra</option>
													<option value="AO" label="Angola">Angola</option>
													<option value="AI" label="Anguilla">Anguilla</option>
													<option value="AQ" label="Antarctica">Antarctica</option>
													<option value="AG" label="Antigua and Barbuda">Antigua and Barbuda</option>
													<option value="AR" label="Argentina">Argentina</option>
													<option value="AM" label="Armenia">Armenia</option>
													<option value="AW" label="Aruba">Aruba</option>
													<option value="AU" label="Australia">Australia</option>
													<option value="AT" label="Austria">Austria</option>
													<option value="AZ" label="Azerbaijan">Azerbaijan</option>
													<option value="BS" label="Bahamas">Bahamas</option>
													<option value="BH" label="Bahrain">Bahrain</option>
													<option value="BD" label="Bangladesh">Bangladesh</option>
													<option value="BB" label="Barbados">Barbados</option>
													<option value="BY" label="Belarus">Belarus</option>
													<option value="BE" label="Belgium">Belgium</option>
													<option value="BZ" label="Belize">Belize</option>
													<option value="BJ" label="Benin">Benin</option>
													<option value="BM" label="Bermuda">Bermuda</option>
													<option value="BT" label="Bhutan">Bhutan</option>
													<option value="BO" label="Bolivia">Bolivia</option>
													<option value="BA" label="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
													<option value="BW" label="Botswana">Botswana</option>
													<option value="BV" label="Bouvet Island">Bouvet Island</option>
													<option value="BR" label="Brazil">Brazil</option>
													<option value="BQ" label="British Antarctic Territory">British Antarctic Territory</option>
													<option value="IO" label="British Indian Ocean Territory">British Indian Ocean Territory</option>
													<option value="VG" label="British Virgin Islands">British Virgin Islands</option>
													<option value="BN" label="Brunei">Brunei</option>
													<option value="BG" label="Bulgaria">Bulgaria</option>
													<option value="BF" label="Burkina Faso">Burkina Faso</option>
													<option value="BI" label="Burundi">Burundi</option>
													<option value="KH" label="Cambodia">Cambodia</option>
													<option value="CM" label="Cameroon">Cameroon</option>
													<option value="CA" label="Canada">Canada</option>
													<option value="CT" label="Canton and Enderbury Islands">Canton and Enderbury Islands</option>
													<option value="CV" label="Cape Verde">Cape Verde</option>
													<option value="KY" label="Cayman Islands">Cayman Islands</option>
													<option value="CF" label="Central African Republic">Central African Republic</option>
													<option value="TD" label="Chad">Chad</option>
													<option value="CL" label="Chile">Chile</option>
													<option value="CN" label="China">China</option>
													<option value="CX" label="Christmas Island">Christmas Island</option>
													<option value="CC" label="Cocos [Keeling] Islands">Cocos [Keeling] Islands</option>
													<option value="CO" label="Colombia">Colombia</option>
													<option value="KM" label="Comoros">Comoros</option>
													<option value="CG" label="Congo - Brazzaville">Congo - Brazzaville</option>
													<option value="CD" label="Congo - Kinshasa">Congo - Kinshasa</option>
													<option value="CK" label="Cook Islands">Cook Islands</option>
													<option value="CR" label="Costa Rica">Costa Rica</option>
													<option value="HR" label="Croatia">Croatia</option>
													<option value="CU" label="Cuba">Cuba</option>
													<option value="CY" label="Cyprus">Cyprus</option>
													<option value="CZ" label="Czech Republic">Czech Republic</option>
													<option value="CI" label="Côte d’Ivoire">Côte d’Ivoire</option>
													<option value="DK" label="Denmark">Denmark</option>
													<option value="DJ" label="Djibouti">Djibouti</option>
													<option value="DM" label="Dominica">Dominica</option>
													<option value="DO" label="Dominican Republic">Dominican Republic</option>
													<option value="NQ" label="Dronning Maud Land">Dronning Maud Land</option>
													<option value="DD" label="East Germany">East Germany</option>
													<option value="EC" label="Ecuador">Ecuador</option>
													<option value="EG" label="Egypt">Egypt</option>
													<option value="SV" label="El Salvador">El Salvador</option>
													<option value="GQ" label="Equatorial Guinea">Equatorial Guinea</option>
													<option value="ER" label="Eritrea">Eritrea</option>
													<option value="EE" label="Estonia">Estonia</option>
													<option value="ET" label="Ethiopia">Ethiopia</option>
													<option value="FK" label="Falkland Islands">Falkland Islands</option>
													<option value="FO" label="Faroe Islands">Faroe Islands</option>
													<option value="FJ" label="Fiji">Fiji</option>
													<option value="FI" label="Finland">Finland</option>
													<option value="FR" label="France">France</option>
													<option value="GF" label="French Guiana">French Guiana</option>
													<option value="PF" label="French Polynesia">French Polynesia</option>
													<option value="TF" label="French Southern Territories">French Southern Territories</option>
													<option value="FQ" label="French Southern and Antarctic Territories">French Southern and Antarctic Territories</option>
													<option value="GA" label="Gabon">Gabon</option>
													<option value="GM" label="Gambia">Gambia</option>
													<option value="GE" label="Georgia">Georgia</option>
													<option value="DE" label="Germany">Germany</option>
													<option value="GH" label="Ghana">Ghana</option>
													<option value="GI" label="Gibraltar">Gibraltar</option>
													<option value="GR" label="Greece">Greece</option>
													<option value="GL" label="Greenland">Greenland</option>
													<option value="GD" label="Grenada">Grenada</option>
													<option value="GP" label="Guadeloupe">Guadeloupe</option>
													<option value="GU" label="Guam">Guam</option>
													<option value="GT" label="Guatemala">Guatemala</option>
													<option value="GG" label="Guernsey">Guernsey</option>
													<option value="GN" label="Guinea">Guinea</option>
													<option value="GW" label="Guinea-Bissau">Guinea-Bissau</option>
													<option value="GY" label="Guyana">Guyana</option>
													<option value="HT" label="Haiti">Haiti</option>
													<option value="HM" label="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option>
													<option value="HN" label="Honduras">Honduras</option>
													<option value="HK" label="Hong Kong SAR China">Hong Kong SAR China</option>
													<option value="HU" label="Hungary">Hungary</option>
													<option value="IS" label="Iceland">Iceland</option>
													<option value="IN" label="India">India</option>
													<option value="ID" label="Indonesia">Indonesia</option>
													<option value="IR" label="Iran">Iran</option>
													<option value="IQ" label="Iraq">Iraq</option>
													<option value="IE" label="Ireland">Ireland</option>
													<option value="IM" label="Isle of Man">Isle of Man</option>
													<option value="IL" label="Israel">Israel</option>
													<option value="IT" label="Italy">Italy</option>
													<option value="JM" label="Jamaica">Jamaica</option>
													<option value="JP" label="Japan">Japan</option>
													<option value="JE" label="Jersey">Jersey</option>
													<option value="JT" label="Johnston Island">Johnston Island</option>
													<option value="JO" label="Jordan">Jordan</option>
													<option value="KZ" label="Kazakhstan">Kazakhstan</option>
													<option value="KE" label="Kenya">Kenya</option>
													<option value="KI" label="Kiribati">Kiribati</option>
													<option value="KW" label="Kuwait">Kuwait</option>
													<option value="KG" label="Kyrgyzstan">Kyrgyzstan</option>
													<option value="LA" label="Laos">Laos</option>
													<option value="LV" label="Latvia">Latvia</option>
													<option value="LB" label="Lebanon">Lebanon</option>
													<option value="LS" label="Lesotho">Lesotho</option>
													<option value="LR" label="Liberia">Liberia</option>
													<option value="LY" label="Libya">Libya</option>
													<option value="LI" label="Liechtenstein">Liechtenstein</option>
													<option value="LT" label="Lithuania">Lithuania</option>
													<option value="LU" label="Luxembourg">Luxembourg</option>
													<option value="MO" label="Macau SAR China">Macau SAR China</option>
													<option value="MK" label="Macedonia">Macedonia</option>
													<option value="MG" label="Madagascar">Madagascar</option>
													<option value="MW" label="Malawi">Malawi</option>
													<option value="MY" label="Malaysia">Malaysia</option>
													<option value="MV" label="Maldives">Maldives</option>
													<option value="ML" label="Mali">Mali</option>
													<option value="MT" label="Malta">Malta</option>
													<option value="MH" label="Marshall Islands">Marshall Islands</option>
													<option value="MQ" label="Martinique">Martinique</option>
													<option value="MR" label="Mauritania">Mauritania</option>
													<option value="MU" label="Mauritius">Mauritius</option>
													<option value="YT" label="Mayotte">Mayotte</option>
													<option value="FX" label="Metropolitan France">Metropolitan France</option>
													<option value="MX" label="Mexico">Mexico</option>
													<option value="FM" label="Micronesia">Micronesia</option>
													<option value="MI" label="Midway Islands">Midway Islands</option>
													<option value="MD" label="Moldova">Moldova</option>
													<option value="MC" label="Monaco">Monaco</option>
													<option value="MN" label="Mongolia">Mongolia</option>
													<option value="ME" label="Montenegro">Montenegro</option>
													<option value="MS" label="Montserrat">Montserrat</option>
													<option value="MA" label="Morocco">Morocco</option>
													<option value="MZ" label="Mozambique">Mozambique</option>
													<option value="MM" label="Myanmar [Burma]">Myanmar [Burma]</option>
													<option value="NA" label="Namibia">Namibia</option>
													<option value="NR" label="Nauru">Nauru</option>
													<option value="NP" label="Nepal">Nepal</option>
													<option value="NL" label="Netherlands">Netherlands</option>
													<option value="AN" label="Netherlands Antilles">Netherlands Antilles</option>
													<option value="NT" label="Neutral Zone">Neutral Zone</option>
													<option value="NC" label="New Caledonia">New Caledonia</option>
													<option value="NZ" label="New Zealand">New Zealand</option>
													<option value="NI" label="Nicaragua">Nicaragua</option>
													<option value="NE" label="Niger">Niger</option>
													<option value="NG" label="Nigeria">Nigeria</option>
													<option value="NU" label="Niue">Niue</option>
													<option value="NF" label="Norfolk Island">Norfolk Island</option>
													<option value="KP" label="North Korea">North Korea</option>
													<option value="VD" label="North Vietnam">North Vietnam</option>
													<option value="MP" label="Northern Mariana Islands">Northern Mariana Islands</option>
													<option value="NO" label="Norway">Norway</option>
													<option value="OM" label="Oman">Oman</option>
													<option value="PC" label="Pacific Islands Trust Territory">Pacific Islands Trust Territory</option>
													<option value="PK" label="Pakistan">Pakistan</option>
													<option value="PW" label="Palau">Palau</option>
													<option value="PS" label="Palestinian Territories">Palestinian Territories</option>
													<option value="PA" label="Panama">Panama</option>
													<option value="PZ" label="Panama Canal Zone">Panama Canal Zone</option>
													<option value="PG" label="Papua New Guinea">Papua New Guinea</option>
													<option value="PY" label="Paraguay">Paraguay</option>
													<option value="YD" label="People's Democratic Republic of Yemen">People's Democratic Republic of Yemen</option>
													<option value="PE" label="Peru">Peru</option>
													<option value="PH" label="Philippines">Philippines</option>
													<option value="PN" label="Pitcairn Islands">Pitcairn Islands</option>
													<option value="PL" label="Poland">Poland</option>
													<option value="PT" label="Portugal">Portugal</option>
													<option value="PR" label="Puerto Rico">Puerto Rico</option>
													<option value="QA" label="Qatar">Qatar</option>
													<option value="RO" label="Romania">Romania</option>
													<option value="RU" label="Russia">Russia</option>
													<option value="RW" label="Rwanda">Rwanda</option>
													<option value="RE" label="Réunion">Réunion</option>
													<option value="BL" label="Saint Barthélemy">Saint Barthélemy</option>
													<option value="SH" label="Saint Helena">Saint Helena</option>
													<option value="KN" label="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
													<option value="LC" label="Saint Lucia">Saint Lucia</option>
													<option value="MF" label="Saint Martin">Saint Martin</option>
													<option value="PM" label="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
													<option value="VC" label="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
													<option value="WS" label="Samoa">Samoa</option>
													<option value="SM" label="San Marino">San Marino</option>
													<option value="SA" label="Saudi Arabia">Saudi Arabia</option>
													<option value="SN" label="Senegal">Senegal</option>
													<option value="RS" label="Serbia">Serbia</option>
													<option value="CS" label="Serbia and Montenegro">Serbia and Montenegro</option>
													<option value="SC" label="Seychelles">Seychelles</option>
													<option value="SL" label="Sierra Leone">Sierra Leone</option>
													<option value="SG" label="Singapore">Singapore</option>
													<option value="SK" label="Slovakia">Slovakia</option>
													<option value="SI" label="Slovenia">Slovenia</option>
													<option value="SB" label="Solomon Islands">Solomon Islands</option>
													<option value="SO" label="Somalia">Somalia</option>
													<option value="ZA" label="South Africa">South Africa</option>
													<option value="GS" label="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
													<option value="KR" label="South Korea">South Korea</option>
													<option value="ES" label="Spain">Spain</option>
													<option value="LK" label="Sri Lanka">Sri Lanka</option>
													<option value="SD" label="Sudan">Sudan</option>
													<option value="SR" label="Suriname">Suriname</option>
													<option value="SJ" label="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
													<option value="SZ" label="Swaziland">Swaziland</option>
													<option value="SE" label="Sweden">Sweden</option>
													<option value="CH" label="Switzerland">Switzerland</option>
													<option value="SY" label="Syria">Syria</option>
													<option value="ST" label="São Tomé and Príncipe">São Tomé and Príncipe</option>
													<option value="TW" label="Taiwan">Taiwan</option>
													<option value="TJ" label="Tajikistan">Tajikistan</option>
													<option value="TZ" label="Tanzania">Tanzania</option>
													<option value="TH" label="Thailand">Thailand</option>
													<option value="TL" label="Timor-Leste">Timor-Leste</option>
													<option value="TG" label="Togo">Togo</option>
													<option value="TK" label="Tokelau">Tokelau</option>
													<option value="TO" label="Tonga">Tonga</option>
													<option value="TT" label="Trinidad and Tobago">Trinidad and Tobago</option>
													<option value="TN" label="Tunisia">Tunisia</option>
													<option value="TR" label="Turkey">Turkey</option>
													<option value="TM" label="Turkmenistan">Turkmenistan</option>
													<option value="TC" label="Turks and Caicos Islands">Turks and Caicos Islands</option>
													<option value="TV" label="Tuvalu">Tuvalu</option>
													<option value="UM" label="U.S. Minor Outlying Islands">U.S. Minor Outlying Islands</option>
													<option value="PU" label="U.S. Miscellaneous Pacific Islands">U.S. Miscellaneous Pacific Islands</option>
													<option value="VI" label="U.S. Virgin Islands">U.S. Virgin Islands</option>
													<option value="UG" label="Uganda">Uganda</option>
													<option value="UA" label="Ukraine">Ukraine</option>
													<option value="SU" label="Union of Soviet Socialist Republics">Union of Soviet Socialist Republics</option>
													<option value="AE" label="United Arab Emirates">United Arab Emirates</option>
													<option value="GB" label="United Kingdom">United Kingdom</option>
													<option value="US" label="United States">United States</option>
													<option value="ZZ" label="Unknown or Invalid Region">Unknown or Invalid Region</option>
													<option value="UY" label="Uruguay">Uruguay</option>
													<option value="UZ" label="Uzbekistan">Uzbekistan</option>
													<option value="VU" label="Vanuatu">Vanuatu</option>
													<option value="VA" label="Vatican City">Vatican City</option>
													<option value="VE" label="Venezuela">Venezuela</option>
													<option value="VN" label="Vietnam">Vietnam</option>
													<option value="WK" label="Wake Island">Wake Island</option>
													<option value="WF" label="Wallis and Futuna">Wallis and Futuna</option>
													<option value="EH" label="Western Sahara">Western Sahara</option>
													<option value="YE" label="Yemen">Yemen</option>
													<option value="ZM" label="Zambia">Zambia</option>
													<option value="ZW" label="Zimbabwe">Zimbabwe</option>
													<option value="AX" label="Åland Islands">Åland Islands</option>
												</select>       
											</div>
										</div>

										<!-- Text input-->
										<div class="control-group form-group">
											<label class="control-label col-lg-4" for="fly_since">
												<?= 'Fly since'; ?>
											</label>
											<div class="controls col-lg-8">
												<input id="fly_since" name="fly_since" type="text"
												value="<?= e($currentUser->fly_since); ?>"
												class="input-xlarge form-control">
											</div>
										</div>

										<!-- Radio input-->
										<div class="control-group form-group">
											<label class="control-label col-lg-6" for="contact_form">
												Do you allow pgearth users to contact you via contact form? <br /> (your email will be hidden)
											</label>

											<div class="form-check controls col-lg-6">
												<label class="form-check-label">
													<input class="form-check-input" type="radio" name="contact_form" id="showMailRadios1" value="0"<?php if ($currentUser->contact_form==0) echo " checked";?>>
													No, i don't.
												</label>
											</div>
												
											<div class="form-check controls col-lg-6">
												<label class="form-check-label">
													<input class="form-check-input" type="radio" name="contact_form" id="showMailRadios2" value="1"<?php if ($currentUser->contact_form==1) echo " checked";?>>
													Yes, i do.
												</label>
											</div>								
										</div>

										<!-- Button -->
										<div class="control-group form-group">
											<label class="control-label col-lg-4" for="update_details">
											</label>
											<div class="controls col-lg-8">
												<button id="update_details" name="update_details" class="btn btn-primary">
												<?= trans('update'); ?>
												</button>
												 <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
											</div>
										</div>
									</fieldset>
								</form>
							
							</div> <!-- yourdetails -->



							<div id="changePassword" class="tab-pane fade">
								<form class="form-horizontal no-submit" id="form-changepassword">
									<fieldset>
										<!-- Form Name -->
										<legend><?= trans('change_password') ?></legend>

										<!-- Password input-->
										<div class="control-group form-group">
											<label class="control-label col-lg-4" for="old_password">
												<?= trans('old_password'); ?>
											</label>
											<div class="controls col-lg-8">
												<input id="old_password" name="old_password" type="password" class="input-xlarge form-control">
											</div>
										</div>

										<!-- Password input-->
										<div class="control-group form-group">
											<label class="control-label col-lg-4" for="new_password">
												<?= trans('new_password'); ?>
											</label>
											<div class="controls col-lg-8">
												<input id="new_password" name="new_password" type="password" class="input-xlarge form-control">
											</div>
										</div>

										<!-- Password input-->
										<div class="control-group form-group">
											<label class="control-label col-lg-4" for="new_password_confirm">
												<?= trans('confirm_new_password'); ?>
											</label>
											<div class="controls col-lg-8">
												<input id="new_password_confirm" name="new_password_confirm"
												type="password" class="input-xlarge form-control">
											</div>
										</div>

										<!-- Button -->
										<div class="control-group form-group">
											<label class="control-label col-lg-4" for="change_password"></label>
											<div class="controls col-lg-8">
												<button id="change_password" name="change_password" class="btn btn-primary">
													<?= trans('update'); ?>
												</button>
											</div>
										</div>
									</fieldset>
								</form>
							</div>  <!-- changePassword -->

						</div>  <!-- tabcontent -->

					</div> <!-- profile-detail-wrapper content -->
				</div> <!-- modal-body -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
			</div> <!-- modal-content -->
		</div> <!-- modalDialog -->
	</div>  <!-- loginModal -->


<?php	}
?>

	<!--******* "pros and clubs" modal  *****************-->  
    <div class="modal fade" id="featureModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title text-primary" id="feature-title"></h4>
          </div>
          <div class="modal-body" id="feature-info"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


	<!--******* "attribution" modal  *****************-->  
    <div class="modal fade" id="attributionModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">
              Map attributions :
            </h4>
          </div>
          <div class="modal-body">
             <p id="attribution"> </p>
             <p>Website built with <a href="http://leafletjs.com/" target="_blank">Leaflet</a> mapping software, using a <a href="https://github.com/bmcbride/bootleaf" target="_blank">bootleaf</a> framework basis.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
	<!--******* "404" modal  *****************-->  
    <div class="modal fade" id="404Modal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">
              Oooops, there is nothing here....
            </h4>
          </div>
          <div class="modal-body">
             <p id="404">You are apparently trying to watch a page that doesn't exist on pgearth... Sorry for that :(</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    
 	<!--******* "api" modal  *****************-->  
   <div class="modal fade" id="apiModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">
              PGEarth API
            </h4>
          </div>
          <div class="modal-body">
            <div>The PgEArth API gladly offers you the chance to use the website data.<br />Let us know if you want to access it.</div>
            <div>Some websites/apps that use the API:
				<ul>
					<li> <a href="http://spotair.mobi" target="_blank"> spotair.mobi <i class="fa fa-external-link"></i></a></li>
					<li> <a href="https://www.meteo-parapente.com" target="_blank"> meteo parapente <i class="fa fa-external-link"></i></a></li>
					<li> <a href="http://windy.com" target="_blank"> windyty.com <i class="fa fa-external-link"></i></a></li>
					<li> <a href="http://paraglidingmap.com" target="_blank"> paraglidingmap <i class="fa fa-external-link"></i></a></li>
					<li> <a href="http://www.mobibalises.net/i" target="_blank"> mobibalises <i class="fa fa-external-link"></i></a></li>
					<li> <a href="http://flyskyhy.com" target="_blank"> Flyskyhy <i class="fa fa-external-link"></i></a></li>
					<li>...</li>
					<li> whoever else ? <a href="#" class="openAnotherModal" modalToOpen="contactModal" tab="contact">your app here ?</a></li>
				</ul>
			</div>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


	<!--******* "export" modal  *****************-->  
   <div class="modal fade" id="exportModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">
              Export feature
            </h4>
          </div>
          <div class="modal-body">
			  <h4>GPX format</h4>
			  Export the sites that are in the current viewport (limited to 99 sites max..) :
			  <ul>
				  <li><a href="#" onClick="exportGPX('short');">Simple</a>: only 1 or 2 waypoint(s) per flying site, corresponding to the main takeoff and landing if known</li>
				  <li><a href="#" onClick="exportGPX('detailled');">Detailled</a>: extra waypoints for parkings, alternate takeoffs and landings if any</li>
			  </ul>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

	<!--******* "filter modal  *****************-->  
   <div class="modal fade" id="filtersModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">
              Flying sites filter
            </h4>
          </div><!-- /.modal-header -->
          <div class="modal-body">
			  <div id="filtersModalContent"></div>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <script src="assets/js/localhost/jquery-2.1.4.min.js"></script>
    <script src="assets/js/localhost/bootstrap.min.js"></script>
    <script src="assets/js/localhost/typeahead.bundle.min.js"></script>
    <script src="assets/js/localhost/handlebars.min.js"></script>
    <script src="assets/js/localhost/list.min.js"></script>
    <script src="assets/js/localhost/leaflet.js"></script>   
<!--	<script src="assets/js/leaflet.1.2.js"></script>	-->
	<script src="assets/js/localhost/leaflet.markercluster.js"></script>
    <script src="assets/js/localhost/L.Control.Locate.min.js"></script>
    <script src="assets/leaflet-groupedlayercontrol/leaflet.groupedlayercontrol.js"></script>
    <script src="assets/js/localhost/esri-leaflet.js" type="text/javascript"></script>
    <script src="assets/external-scripts/lightbox/lightbox.min.js"></script>
    <script src="assets/js/xeditable/xeditable.js"></script>
    <script src="https://api4.windy.com/assets/libBoot.js"></script>

    <script src="assets/js/pge_utils_functions.js"></script>
    <script src="assets/js/pge_rewriteUrl.js"></script>

	<?php include('includes/manageGETvariables.php'); ?>

    <script src="assets/js/sitesArray.js"></script>
    <script src="assets/js/pge_news.js"></script>
    <script src="assets/js/plugin-heatLayer.js"></script>
    <script src="assets/js/plugin-leaflet_label.js"></script>
    <script src="assets/js/leaflet.awesome-markers.min.js"></script>
    <script src="assets/js/pge_sites.js"></script>
    <script src="assets/js/pge_siteFlyableTab.js"></script>
    <script src="assets/js/pge_pros.js"></script>
    <script src="assets/js/pge_clubs.js"></script>
    <script src="assets/js/pge_menu.js"></script>
    <script src="assets/js/pge_sidebar.js"></script>
    <script src="assets/js/pge_windyOverlay.js"></script>
    <script src="assets/js/pge_map.js"></script>
    <script src="assets/js/pge_mapEvents.js"></script>
    <script src="assets/js/pge_search.js"></script>
    <script src="assets/js/pge_editable.js"></script>
    <script src="assets/js/pge_reported.js"></script>
    <script src="assets/js/pge_image_overlay.js"></script>
    <script src="assets/js/pge_member.js"></script>
    <script src="assets/js/pge_addNew.js"></script>
  
    
		<script src="member/assets/js/sha512.js" type="text/javascript" charset="utf-8"></script>
		<script src="member/ASLibrary/js/asengine.js" type="text/javascript" charset="utf-8"></script>
		<script src="member/ASLibrary/js/js-bootstrap.php" type="text/javascript" charset="utf-8"></script>
<?php if (! $isMember ) { ?>
        <script src="member/ASLibrary/js/register.js" type="text/javascript" charset="utf-8"></script>
        <script src="member/ASLibrary/js/login.js" type="text/javascript" charset="utf-8"></script>
        <script src="member/ASLibrary/js/passwordreset.js" type="text/javascript" charset="utf-8"></script>
<?php } else { ?>
        <script src="member/ASLibrary/js/profile.js" type="text/javascript" charset="utf-8"></script>
       	<script>
			var iso = '<?php echo $iso;?>';
			$("#country").val(iso.toUpperCase());
		</script>
<?php	} 

require_once('/home/raf/www/owa/owa_php.php');
    
$owa = new owa_php();
// Set the site id you want to track
$owa->setSiteId('4ce625db76e0b66687f0d2b012d337cd');
// Uncomment the next line to set your page title
//$owa->setPageTitle('somepagetitle');
// Set other page properties
//$owa->setProperty('foo', 'bar');
$owa->trackPageView();
?>    

  </body>
</html>
