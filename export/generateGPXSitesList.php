<?php

while ($val=mysqli_fetch_array($result)){
	
	if ($val['name']) {		
		
		/* we store results in an array instead of processing them on the fly
		 * so that we can then sort the array by distance from a given point
		 * and limit to closest results
		 * unfortunately this part of the code is not written here...
		 * (and that makes that array story totaly useless...)
		 */
		 
		// let's get last edit date...
		$qLastEdit = "SELECT date FROM `sites_users` WHERE `item_type` LIKE 'site' AND `item_id` = ".$val['id']." ORDER BY `sites_users`.`date` DESC LIMIT 1 ";
		$rLastEdit = mysqli_query($bdd, $qLastEdit);
		$vLastEdit = mysqli_fetch_array($rLastEdit);
		$last_edit[$count] =  $vLastEdit['date'];
		
		 
		$name[$count] = $val['name'];
		$id_site[$count] = $val['id'];
		$iso[$count] = $val['iso'];

		$lng[$count] = $val['lng'];
		$lat[$count] = $val['lat'];
		$takeoff_altitude[$count] = $val['takeoff_altitude'];

		$landing_name[$count] = $val['landing_name'];
		$landing_lat[$count]  = $val['landing_lat'];
		$landing_lng[$count]  = $val['landing_lng'];
		$landing_altitude[$count] = $val['landing_altitude'];

		$landing_description[$count] = $val['landing'];
		$takeoff_description[$count] = $val['takeoff'];

		$N[$count] = $val['N'];
		$NE[$count] = $val['NE'];
		$E[$count] = $val['E'];
		$SE[$count] = $val['SE'];
		$S[$count] = $val['S'];
		$SW[$count] = $val['SW'];
		$W[$count] = $val['W'];
		$NW[$count] = $val['NW'];

		$hike[$count] = $val['hike'];
		$soaring[$count] = $val['soaring'];
		$thermal[$count] = $val['thermal'];
		$xc[$count] = $val['xc'];
		$flatland[$count] = $val['flatland'];
		$winch[$count] = $val['winch'];
		$hanggliding[$count] = $val['hanggliding'];

		$takeoff_going_there[$count] = $val['access'];
		$takeoff_flight_rules[$count] = $val['rules'];
		$takeoff_contacts[$count] = $val['contact'];
		$takeoff_comments[$count] = $val['comments'];
		$weather[$count] = $val['weather'];

		$web[$count] = $val['web'];


		if (isset($_GET['distance'])) {
			$latRad = 3.14159265 * $val['lat']   / 180;
			$lngRad = 3.14159265 * $val['lng']   / 180;
			$distance[$count] = round(6366 * acos(cos($latRad) * cos($latref) * cos($lngref-$lngRad) + sin($latRad) * sin($latref)),2);
		}

		$count++;
	}
}

$limit= count($name);

if (isset ($_GET['limit'])) {
	$limit = min ($_GET['limit'], count ($name));
}

if (isset($_GET['distance']) and $limit > 0) {
	array_multisort( $distance, SORT_ASC,
		$name, $iso, $id_site, $last_edit,
		$lat, $lng, $takeoff_altitude, 
		$landing_name, $landing_lat, $landing_lng, $landing_altitude, 
		$takeoff_description, $landing_description,
		$hike, $soaring, $thermal, $xc ,$flatland,$winch, $hanggliding,  
		$takeoff_comments, $weather, $takeoff_going_there, $takeoff_flight_rules, $takeoff_contacts,
		$web,
		$N,$NE,$E,$SE,$S,$SW,$W,$NW
	);
}


/*
$xmlElement = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" standalone="no" ?>');

$gpx = $xmlElement->addChild('gpx');
$gpx->addAttribute('version', 1.1);
$gpx->addAttribute('creator');
	$gpx->creator = 'http://paraglidingearth.com';
*/
$gpx = new SimpleXMLElement('<gpx></gpx>');
$gpx->addAttribute('version', 1.1);
$gpx->addAttribute('creator', 'http://paraglidingearth.com');

for ($k=0;$k<$limit;$k++){

	// MAIN TAKEOFF
	$takeoff = $gpx->addChild('wpt');
	$takeoff->addAttribute('lat', $lat[$k]);
	$takeoff->addAttribute('lon', $lng[$k]);
	
	$takeoff->addChild('type', 'takeoff');

	$takeoff->addChild('name'); 
		$takeoff->name = $name[$k];

	$takeoff->addChild('ele', $takeoff_altitude[$k]);

	$takeoff->addChild('link', 'http://www.paraglidingearth.com/?site='.$id_site[$k]);
	

	$takeoff->addChild('desc');
		$takeoff->desc = $takeoff_description[$k];
	// END OF MAIN TAKEOFF	
		
	// MAIN LANDING
	$hasAMainLanding = false;
	if($landing_lng[$k] != 0 and $landing_lat[$k] != 0){
		$hasAMainLanding = true;
		$landing = $gpx->addChild('wpt');
		$landing->addAttribute('lat', $landing_lat[$k]);
		$landing->addAttribute('lon', $landing_lng[$k]);
		
		$landing->addChild('type', 'landing');
		
		$landing->addChild('name');
		if(!$landing_name[$k]) $landing->name = $name[$k].' landing';
		else $landing->name = $landing_name[$k];

		$landing->addChild('ele', $landing_altitude[$k]);
		
		$landing->addChild('desc');
			$landing->desc = $landing_description[$k];
		
		$landing->addChild('cmt');

		$landing->cmt->addChild('landing_countryCode', $iso[$k]);
		$landing->cmt->addChild('landing_pge_site_id', $id_site[$k]);
	}
	// END OF MAIN LANDING


	if ($style=='detailled') {

		$takeoff->addChild('cmt'); 

		if (isset($_GET['distance'])){
			$takeoff->cmt->addChild('distance', $distance[$k]);
		}
		$takeoff->cmt->addChild('countryCode', $iso[$k]);

		$takeoff->cmt->addChild('paragliding', '1');
		$takeoff->cmt->addChild('hanggliding', $hanggliding[$k]);
		$takeoff->cmt->addChild('hike', $hike[$k]);
		$takeoff->cmt->addChild('thermals', $thermal[$k]);
		$takeoff->cmt->addChild('soaring', $soaring[$k]);
		$takeoff->cmt->addChild('xc', $xc[$k]);
		$takeoff->cmt->addChild('flatland', $flatland[$k]);
		$takeoff->cmt->addChild('winch', $winch[$k]);

		$takeoff->cmt->addChild('last_edit', $last_edit[$k]);

		$takeoff->cmt->addChild('orientations');
			$takeoff->cmt->orientations->addChild('N', $N[$k]);
			$takeoff->cmt->orientations->addChild('NE', $NE[$k]);
			$takeoff->cmt->orientations->addChild('E', $E[$k]);
			$takeoff->cmt->orientations->addChild('SE', $SE[$k]);
			$takeoff->cmt->orientations->addChild('S', $S[$k]);
			$takeoff->cmt->orientations->addChild('SW', $SW[$k]);
			$takeoff->cmt->orientations->addChild('W', $W[$k]);
			$takeoff->cmt->orientations->addChild('NW', $NW[$k]);

		
		// TEXT FIELDS
		$takeoff->cmt->addChild('flight_rules');
			$takeoff->cmt->flight_rules = $takeoff_flight_rules[$k];
		$takeoff->cmt->addChild('going_there');
			$takeoff->cmt->going_there = $takeoff_going_there[$k];
		$takeoff->cmt->addChild('comments');
			$takeoff->cmt->comments = $takeoff_comments[$k];
		$takeoff->cmt->addChild('weather');
			$takeoff->cmt->weather = $weather[$k];
		// END OF TEXT FIELDS
			
		// TAKEOFF PARKING WHEN STYLE=DETAILLED 
		$queryTakeoffParkingDetails = "select
						takeoff_parking_lat, takeoff_parking_lng, takeoff_parking_description
						from site where id = ".$id_site[$k] ;
		$resultTakeoffParkingDetails = mysqli_query($bdd, $queryTakeoffParkingDetails);
		$valTakeoffParkingDetails = mysqli_fetch_array($resultTakeoffParkingDetails);

		if ( $valTakeoffParkingDetails['takeoff_parking_lat']!=0 or $valTakeoffParkingDetails['takeoff_parking_lng']!=0 or $valTakeoffParkingDetails['takeoff_parking_description']!="" ) {
			$takeoff_parking = $gpx->addChild('wpt');
			
				$takeoff_parking->addAttribute('lat', $valTakeoffParkingDetails['takeoff_parking_lat']);
				$takeoff_parking->addAttribute('lon', $valTakeoffParkingDetails['takeoff_parking_lng']);
				$takeoff_parking->addChild('type', 'takeoff parking');
				$takeoff_parking->addChild('name', $name[$k] .' takeoff parking');
				$takeoff_parking->addChild('desc');
					$takeoff_parking->desc = $valTakeoffParkingDetails['takeoff_parking_description'];
		}
		// END OF TAKEOFF PARKING

		// ALTERNATE TAKEOFFS
		$queryAlternateTakeoffsDetails = "SELECT
						lat, lng, description, name, altitude
						FROM site_extra_items 
						WHERE type LIKE 'takeoff' AND site = ".$id_site[$k] ;
		$resultAlternateTakeoffsDetails = mysqli_query($bdd, $queryAlternateTakeoffsDetails);

		while ($valAlternateTakeoffsDetails = mysqli_fetch_array($resultAlternateTakeoffsDetails)) {
			if ( $valAlternateTakeoffsDetails['lat']!=0 or $valAlternateTakeoffsDetails['lng']!=0 or $valAlternateTakeoffsDetails['description']!="" ) {
				$alt_takeoff = $gpx->addChild('wpt');
				$alt_takeoff->addChild('type', 'alternate takeoff');
					$alt_takeoff->addChild('name');
						$alt_takeoff->name = $valAlternateTakeoffsDetails['name'];
					$alt_takeoff->addAttribute('lat', $valAlternateTakeoffsDetails['lat']);
					$alt_takeoff->addAttribute('lon', $valAlternateTakeoffsDetails['lng']);
					$alt_takeoff->addChild('ele', $valAlternateTakeoffsDetails['altitude']);
					$alt_takeoff->addChild('desc');
						$alt_takeoff->desc = $valAlternateTakeoffsDetails['description'];
			}
		}
		// END OF ALTERNATE TAKEOFFS


		// LANDING PARKING
		$queryLandingParkingDetails = "select
						landing_parking_lat, landing_parking_lng, landing_parking_description
						from site where id = ".$id_site[$k] ;
		$resultLandingParkingDetails = mysqli_query($bdd, $queryLandingParkingDetails);
		$valLandingParkingDetails = mysqli_fetch_array($resultLandingParkingDetails);
		if ( $valLandingParkingDetails['landing_parking_lat']!=0 or $valLandingParkingDetails['landing_parking_lng']!=0 or $valLandingParkingDetails['landing_parking_description']!="" ) {
			$landing_parking = $gpx->addChild('wpt');
			$landing_parking->addChild('type', 'landing parking');
			$landing_parking->addChild('name', $name[$k].' landing parking');
			$landing_parking->addAttribute('lat', $valLandingParkingDetails['landing_parking_lat']);
			$landing_parking->addAttribute('lon', $valLandingParkingDetails['landing_parking_lng']);
			$landing_parking->addChild('desc');
				$landing_parking->desc = $valLandingParkingDetails['landing_parking_description'];
		}
		// END OF LANDING PARKING
		
		
		// ALTERNATE LANDINGS
		$queryAlternateLandingsDetails = "SELECT
						lat, lng, description, name, altitude
						FROM site_extra_items 
						WHERE type LIKE 'landing' AND site = ".$id_site[$k] ;						
		$resultAlternateLandingsDetails = mysqli_query($bdd, $queryAlternateLandingsDetails);		
		if ($hasAMainLanding) {
			
			while ($valAlternateLandingsDetails = mysqli_fetch_array($resultAlternateLandingsDetails)) {
				if ( $valAlternateLandingsDetails['lat']!=0 or $valAlternateLandingsDetails['lng']!=0 or $valAlternateLandingsDetails['description']!="" ) {
					$alt_landing = $gpx->addChild('wpt');
						$alt_landing->addChild('type', 'alternate landing');
						$alt_landing->addChild('name');
							$alt_landing->name = $valAlternateLandingsDetails['name'];
						$alt_landing->addAttribute('lat', $valAlternateLandingsDetails['lat']);
						$alt_landing->addAttribute('lon', $valAlternateLandingsDetails['lng']);
						$alt_landing->addChild('ele', $valAlternateLandingsDetails['altitude']);
						$alt_landing->addChild('desc');
							$alt_landing->desc = $valAlternateLandingsDetails['description'];
				}
			}
		}
		// END OF ALTERNATE LANDINGS

				
		// ALTERNATE PARKINGS
		// ... to be done....
	} // END OF   "  if (style==detailled)  "
}

Header('Content-type: text/xml');
echo $gpx->asXML();
?>
