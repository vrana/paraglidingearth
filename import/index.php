<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div id="loading">Loading...</div>
		<form action="" method="GET">
			<input id="url"></input>
			<input type="submit"></input>
		</form>

		<table id="pairingTable" border="1">
			<tr>
				<td>property</td>
				<td>pair with</td>
			</tr>
		</table>

		<script src="../assets/js/localhost/jquery-2.1.4.min.js"></script>

		<script>
			var keys = 0;
			var importProps=[];
			var jsonUrl = "../api/geojson/getBoundingBoxSites.php?north=45.307&south=44&east=7&west=5&limit=12&style=detailled";

			function getObject(theObject) {
			    var result = null;

			    if(theObject instanceof Array) {
			        for(var i = 0; i < theObject.length; i++) {
			            result = getObject(theObject[i]);
			        }
			    }
			    else
			    {
			        for(var prop in theObject) {
			            //console.log(prop + ': ' + theObject[prop]);
			            if( $.inArray(prop, importProps ) == -1) {
			                importProps.push(prop);
			                $('#pairingTable tr:last').after('<tr><td>'+prop+'</td><td></td></tr>');
			                console.log('new prop : ' + prop);
			                
			            }
			         	if(theObject[prop] instanceof Object || theObject[prop] instanceof Array)
			                result = getObject(theObject[prop]);
			        }
			    }
			    return result;
			}

			$.getJSON(jsonUrl, function(json){

				getObject(json);



				$("#loading").html("done !");
			});

		</script>

	</body>
</html>