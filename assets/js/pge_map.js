/** ****************   popovers    *********/
$(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
});


/* ***************   pge map scripts   ************** */

var map, featureList, boroughSearch = [], siteSearch = [], proSearch = [], clubSearch = [], showSitesLayer=true, showProsLayer=false, showClubsLayer=false; showOneSite = false;
var addSiteToItem = "";
var displayItemsZoom = 9;

var numberOfSites = 0;
var sitesOnMap = [];
var clubsOnMap = [];
var prosOnMap = [];

//var siteIconOpacity = 1;

var	siteItemsLayer = L.layerGroup(null);	// layer group to show/hide all items (takeoffs, landings, parkings) for one site


var sitesZoomAdvice = false, prosZoomAdvice = false, clubsZoomAdvice = false ; 
function zoomAdvice() {
	if (sitesZoomAdvice || clubsZoomAdvice || prosZoomAdvice){
		$("#zoomAdvice").html("<em>Zoom in to see more</em>");
	} else{
		$("#zoomAdvice").html("");
	}
};

$(window).resize(function() {
  sizeLayerControl();
});

$( document ).ajaxStop(function() {
	$('#loading').css("display", "none");
	$('#loadingWeather').css("display", "none");
});

/* Basemap Layers */
var esriTopo = L.esri.basemapLayer("Topographic");
var esriStreet = L.esri.basemapLayer("Streets");
var esriImagery = L.esri.basemapLayer("Imagery");

/* Overlay Layers */
var highlight = L.geoJson(null);
var highlightStyle = {
  stroke: false,
  fillColor: "#00FFFF",
  fillOpacity: 0.7,
  radius: 10
};

/* Empty layer placeholder to add to layer control for listening when to add/remove sites to markerClusters layer */
var siteLayer = L.geoJson(null);
/* Empty layer placeholder to add to layer control for listening when to add/remove pros to markerClusters layer */
var proLayer = L.geoJson(null);
/* Empty layer placeholder to add to layer control for listening when to add/remove clubs to markerClusters layer */
var clubLayer = L.geoJson(null);




/* ************************************************  Single MARKER CLUSTER layer to hold all clusters */
var markerClusters = new L.MarkerClusterGroup({
  spiderfyOnMaxZoom: true,
  showCoverageOnHover: true,
  zoomToBoundsOnClick: true,
  removeOutsideVisibleBounds: true,
  animate: true,
  disableClusteringAtZoom: displayItemsZoom ,
  iconCreateFunction: function(cluster) {
	var countMarkers = cluster.getChildCount();
	return L.divIcon({
		iconSize: [32,32],
		iconAnchor: [16, 16],
		html: '<div class="pgeMarkerCluster">' + (countMarkers) + '</div>',
		className: 'pgeCluster'
	})
  }	
});

var mapZoom = 2;
var mapCenter = [0, 3];

if (hasFilters) mapZoom=0;

if (zoomOnItem != 'none' ) {
		mapZoom = 12;
		$.ajax({
		  url: "assets/ajax/itemMapCenterJSON.php",
		  dataType: 'json',
		/*async: false,*/
		  data: {id: zoomOnItemId, item: zoomOnItem},
		  success: function(data) {
			mapCenter = [data.lat, data.lng];
			}
		});
	if (zoomOnItem === 'site' ){
		showSitesLayer  = true; 
	 }
	if (zoomOnItem === 'pro' )	{
		showProsLayer  = true;
	}
	if (zoomOnItem === 'club' ) {
		showClubsLayer = true;
	}
}


/* ********************************  define the map *********************** */
map = L.map("map", {
  zoom: mapZoom,
  center: mapCenter,
  maxBounds: [ [90, -180], [-90, 180] ],
  layers: [esriTopo, markerClusters, highlight],
  zoomControl: false,
  attributionControl: true
});
/* ************************  end of  define the map *********************** */

var zoomStart = map.getZoom();
var zoomEnd = map.getZoom();

map.on('popupclose', function(){
	siteItemsLayer.clearLayers();
    map.removeLayer(siteItemsLayer);
//    map.addLayer(sites);
//	showOneSite=false;
//	showSitesLayer=true;
  });

$('#loading').css("display", "none");

if( zoomOnItem === 'bounds' ) {
	//alert(NWlng);
	map.fitBounds([
		[NWlat, NWlng],
		[SElat, SElng]
	]);
	showSitesLayer = true;
	if (showClubs) showClubsLayer = true;
	if (showPros)  showProsLayer  = true;
}

/* scale control */
L.control.scale().addTo(map);

/* **************      add heat map*/
var heat = L.heatLayer( sitesArray, {
		radius: 12,
		blur: 12,
		maxZoom: 7,
});

	
if( zoomEnd < displayItemsZoom ) {
	heat.addTo(map);
	//heat.setOpacity(0.5);
}

/* Attribution control */
function updateAttribution(e) {
  $.each(map._layers, function(index, layer) {
    if (layer.getAttribution) {
      $("#attribution").html((layer.getAttribution()));
    }
  });
}

map.on("layeradd", updateAttribution);
map.on("layerremove", updateAttribution);

/*
var attributionControl = L.control({
  position: "bottomright"
});

attributionControl.onAdd = function (map) {
  var div = L.DomUtil.create("div", "leaflet-control-attribution");
  div.innerHTML = "<span class='hidden-xs'>Developed by <a href='http://bryanmcbride.com'>bryanmcbride.com</a> | </span>";
  div.innerHTML = "<a  class='hidden-xs' href='#' onclick='$(\"#attributionModal\").modal(\"show\"); return false;'>Attribution</a>";
  return div;
};
map.addControl(attributionControl);
*/

var zoomControl = L.control.zoom({
  position: "bottomright"
}).addTo(map);



/* GPS enabled geolocation control set to follow the user's location */
var locateControl = L.control.locate({
  position: "bottomright",
  drawCircle: true,
  follow: true,
  setView: true,
  keepCurrentZoomLevel: true,
  markerStyle: {
    weight: 1,
    opacity: 0.8,
    fillOpacity: 0.8
  },
  circleStyle: {
    weight: 1,
    clickable: false
  },
  icon: "fa fa-crosshairs",
  metric: true,
  strings: {
    title: "My location",
    popup: "You are within {distance} {unit} from this point",
    outsideMapBoundsMsg: "You seem located outside the boundaries of the map"
  },
  locateOptions: {
    maxZoom: 18,
    watch: true,
    enableHighAccuracy: true,
    maximumAge: 10000,
    timeout: 10000
  }
}).addTo(map);



/* display sites at first load */

updateSites();
markerClusters.addLayer(sites);
map.addLayer(siteLayer);  //  add empty layer to the map so that the overlay control is selected... 
if (zoomOnItem === 'club') 
	map.addLayer(clubLayer);   //  add empty layer to the map so that the overlay control is selected... 
if (zoomOnItem === 'pro')
	map.addLayer(proLayer);   //  add empty layer to the map so that the overlay control is selected... 

syncSidebar('sites');




/* Larger screens get expanded layer control and visible sidebar */
if (document.body.clientWidth <= 767) {
  var isCollapsed = true;
} else {
  var isCollapsed = false;
}

var baseLayers = {
  "Topographic": esriTopo,
  "Street Map": esriStreet,
  "Aerial Imagery": esriImagery
};

var groupedOverlays = {
  " ": {
    "<img src='assets/img/site.png' width='24' height='28' /><span id='sitesCount'>&nbsp;Sites</span>": siteLayer,
    "<img src='assets/img/pro.png' width='24' height='28'><span id='prosCount'>&nbsp;Pros</span>": proLayer,
    "<img src='assets/img/club.png' width='24' height='28'><span id='clubsCount'>&nbsp;Clubs</span>": clubLayer
  }
};
/*
var filtersControlContent = 
	{
	"<div><div id='filtersTitle'><i class='fa fa-filter'></i> Filter sites </div><div class='toggleFilters toggle-light'></div></div> <div id='filtersList'></div> <div id='filtersOpenModal'></div>"
	};
*/
var layerControl = L.control.groupedLayers(baseLayers, groupedOverlays, {
   collapsed: isCollapsed
}).addTo(map);

/* Highlight search box text on click */
$("#searchbox").click(function () {
  $(this).select();
});

/* Prevent hitting enter from refreshing the page */
$("#searchbox").keypress(function (e) {
  if (e.which == 13) {
    e.preventDefault();
  }
});

$("#featureModal").on("hidden.bs.modal", function (e) {
  $(document).on("mouseout", ".feature-row", clearHighlight);
});

// Leaflet patch to make layer control scrollable on touch browsers
var container = $(".leaflet-control-layers")[0];
if (!L.Browser.touch) {
  L.DomEvent
  .disableClickPropagation(container)
  .disableScrollPropagation(container);
} else {
  L.DomEvent.disableClickPropagation(container);
}

$(".leaflet-control-layers-list").append("<div id='zoomAdvice'></div><div id='filtersApplied'></div>");

function sharePage(){
	//alert(map.getBounds().getNorthWest().lat);
	var link = "http://paraglidingearth.com/?bounds="+roundNumber(map.getBounds().getNorthWest().lat, 5)+","+roundNumber(map.getBounds().getNorthWest().lng, 5)+","+roundNumber(map.getBounds().getSouthEast().lat, 5)+","+roundNumber(map.getBounds().getSouthEast().lng, 5);
	if (showClubsLayer) link += "&showClubs=true";
	if (showProsLayer) link += "&showPros=true";
	window.prompt("Copy page link to clipboard: Ctrl+C", link);
}

function roundNumber(number, numberOfDecimals) {
	return Math.round(number* Math.pow(10, numberOfDecimals))/Math.pow(10, numberOfDecimals);
}


/*  Sync Sidebar with items on the map */
function syncSidebar( itemType ) {
	
	/* Empty sidebar features */
    $("#feature-list tbody").empty();
    
	if(zoomEnd >= displayItemsZoom) {
		  
		  /* Loop through sites layer and add only features which are in the map bounds */
			if (map.hasLayer(siteLayer) & (itemType == 'sites' || itemType == 'all') ) {
			  sites.eachLayer(function (layer) {
				$("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/site.png"></td><td class="feature-name">' + layer.feature.properties.NAME + ' <img src="assets/img/windrose/26/'+layer.feature.id+'.png" class="windrose26" height="16px" /> <img src="assets/img/flying/25/'+layer.feature.id+'.png" height="22px" /></td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
			  });
			}
		  /* Loop through pros layer and add only features which are in the map bounds */
			if (map.hasLayer(proLayer) & (itemType == 'pros' || itemType == 'all') ) {
			  pros.eachLayer(function (layer) {
			  	$("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/pro.png"></td><td class="feature-name">' + layer.feature.properties.NAME + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
			  });
			}
		  /* Loop through clubs layer and add only features which are in the map bounds */
			if (map.hasLayer(clubLayer) & (itemType == 'clubs' || itemType == 'all') ) {
			  clubs.eachLayer(function (layer) {
				$("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/club.png"></td><td class="feature-name">' + layer.feature.properties.NAME + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
			  });
			}
		  /* Update list.js featureList */
		  featureList = new List("features", {
			valueNames: ["feature-name"]
		  });
		  featureList.sort("feature-name", {
			order: "asc"
		  });
		  
	} else {
		  $("#feature-list tbody").append('<tr class="feature-row"><td></td><td style="vertical-align: middle;">zoom in to get items list</td><td style="vertical-align: middle;"></td></tr>');
	}
}


/* *************   Filters control ************************** */

/********   Toggles from : https://github.com/simontabor/jquery-toggles/  ****/

var filtersControl =  L.Control.extend({
  options: {
    position: 'topright'
  },

  onAdd: function (map) {
    var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-filters');
    
    container.style.backgroundColor = 'white';
   
	container.setAttribute("id", "filtersLeafletControl");
	container.innerHTML = "<div id='flyableTitle'><i class='fa fa-cloud-sun-rain'></i> Flyable ? </div>"
							+ "<div class='toggleFlyable toggle-light disabled'></div>"
							+ "<div id='flyableControlContentZoom'>Zoom in to enable</div>"
							+"<div id='flyableControlContentOffset'><select id='timeOffsetSelect'>"
							+"<option value='0'>now</option>"
							+"<option value='3'>in 3h</option>"
							+"<option value='6'>in 6h</option>"
							+"<option value='9'>in 9h</option>"
							+"<option value='12'>in 12h</option>"
							+"<option value='24'>in 24h</option>"
							+"<option value='48'>in 48h</option>"
							+"</select> </div>";
	container.innerHTML += "<div class='leaflet-control-layers-separator'></div>"
							+" <div><div id='filtersTitle'><i class='fa fa-filter'></i> Filter sites <span id='filtersChevron'></span></div>"
							+ "<div class='toggleFilters toggle-light'></div></div>"
							+ "<div id='filtersInfo'><div id='filtersList'></div>"
							+ "<div id='filtersOpenModal'></div></div>"
	container.innerHTML += "<div class='leaflet-control-layers-separator'></div>"
							+ "<div id='windyTitle'>"
								+ "<a href='https://windy.com'  class='windyLink' target='_blank'><img src='assets/img/windyLogo.png' /></a> Windy <br />"
								+ "<div class='toggleWindy toggle-light'></div>"
								+ "<div id='windyControlContent' style='display:none;'>"
								+ "		<label style='font-weight: normal;' title='toggle pgearth/windy maps actions' >clickable: <input type='checkbox' id='windyClickable'></label>"
								+ "		<br />opacity: <input type='range' max='1' min='0.4' step='0.1' value='0.7' id='windyOpacityRange' />"
								+ "</div>";
							+ "</div>"; 


	container.onclick = function(){
	}

    return container;
  }
});

map.addControl(new filtersControl());

//  initiate filters toggle
//        https://github.com/simontabor/jquery-toggles/

$('.toggleFilters').toggles({
  width: 40, // width used if not set in css
  height: 14, // height if not set in css
  text: {
    on: 'on', // text for the ON position
    off: 'off' // and off
  },
});

var myToggleFilters = $('.toggleFilters').data('toggles');

$('.toggleFilters').on('toggle', function(e, active) {
  if (active) {
 	$("#filtersModal").modal("show");
	openModal = 'filtersModal';
	$(".navbar-collapse.in").collapse("hide");
  } else {
    resetFilters();
  }
});


if (hasFilters){
	updateMapFilterControl();
} else {
	myToggleFilters.toggle(false, false, true);
}

$('#filtersOpenModal').on('click', function() {
 	$("#filtersModal").modal("show");
	openModal = 'filtersModal';
	$(".navbar-collapse.in").collapse("hide");
});

function resetFilters(){
	hasFilters = false;
	hasCountryFilter = false;
	hasWindFilter = false;
	hasKindFilter = false;
	filterHTMLString = "";
	filterCountry = "all";
	filterKinds = [];
	filterWinds = [];
	myToggleFilters.toggle(false, false, true);
	$("#filtersInfo").hide('fast');
	$("#filtersList").html("");
	$("#filtersOpenModal").html("");
	$("#filtersChevron").html("");
	$("#filtersTitle").css('color','black');
	$("#filtersTitle").html("<i class='fa fa-filter'></i> Filter sites <span id='filtersChevron'></span>");
	$("#filtersTitle").css("cursor","auto");
	refreshSites = true;
	updateSites();
}


function updateMapFilterControl() {
	$("#filtersTitle").css('color','#9c0000');
	$("#filtersTitle").html("<i class='fa fa-filter'></i> Filters applied on sites <span id='filtersChevron'></span>");
	$("#filtersTitle").css("cursor","pointer");
	$("#filtersChevron").html("<i class='fa fa-eye-slash'></i>");
	$("#filtersList").html(filterHTMLString);
	$("#filtersOpenModal").html("<span style='color:#0078A8;'><i class='fa fa-edit'></i>edit&nbsp;/&nbsp;<i class='fa fa-plus'></i>add</span>");
	$("#filtersInfo").show('fast');

	myToggleFilters.toggle(true, false, true);
}

$("#filtersTitle").on("click", function () { 
	if (hasFilters) {
		if ( $("#filtersInfo").css("display") == "block" )
			$("#filtersChevron").html("<i class='fa fa-eye'></i>");
		else $("#filtersChevron").html("<i class='fa fa-eye-slash'></i>");
		$("#filtersInfo").toggle("fast") ;
	}
});


	// activate windy toggle
$('.toggleWindy').toggles({
  width: 40, // width used if not set in css
  height: 14, // height if not set in css
  text: {
    on: 'on', // text for the ON position
    off: 'off' // and off
  },
});

var myToggleWindy = $('.toggleWindy').data('toggles');

$('.toggleWindy').on('toggle', function(e, active) {
  if (active) {

	$('.windyContainer').css('visibility','visible');
	$("#windyControlContent").show("fast");

	map.removeLayer(heat);
	showWindyOverlay = true;
	
	$("#windyOpacityRange").on("change mousemove", function() {
		$('.windyContainer').css('opacity', $(this).val() );
	});
  } else {
	$('.windyContainer').css('visibility','hidden');
	$("#windyControlContent").hide("fast")
	
	if (map.getZoom() <= displayItemsZoom-2 ){
		heat.addTo(map);
	}
	
	showWindyOverlay = false;
  }
});

$('#windyClickable').change(function() {
   if($(this).is(":checked")) {
		isWindyMapClickable = true;
		$(".windyContainer").css("pointer-events","auto");
		$(".windyContainer").css("border","solid orange");
		return;
	} else {
		$(".windyContainer").css("pointer-events","none");
		$(".windyClose").css("pointer-events","auto");
		$(".windyContainer").css("border","dashed orange");
		isWindyMapClickable = false;
	}
});

/****************** Follow windy map ********************/
$("body").on('windyMapMoving', function(event, lat, lng, zoom) {
	if ( isWindyMapClickable )
		map.setView( [lat, lng], zoom);
});


var windyAppendDivs = false;


	// activate flyable toggle
$('.toggleFlyable').toggles({
  width: 40, // width used if not set in css
  height: 14, // height if not set in css
  text: {
    on: 'on', // text for the ON position
    off: 'off' // and off
  },
});

var myToggleFlyable = $('.toggleFlyable').data('toggles');

$("#flyableControlContentOffset").css("display","none");

$('.toggleFlyable').on('toggle', function(e, active) {
  if (active) {
	showFlyable = true;
	$("#loadingWeather").css("display","block");
	toggleFlyableIcons(true, 0);
	$("#flyableControlContentOffset").css("display","block");
	$("#timeOffsetSelect").change(function(){
		$( "#timeOffsetSelect option:selected" ).each(function() {
			console.log($( this ).val());
			timeOffset = $( this ).val();
			toggleFlyableIcons(true, $( this ).val());
		});
	}); 
  } else {
	  showFlyable = false;
	  toggleFlyableIcons(false, 0);
	  $("#flyableControlContentOffset").css("display","none");
  }
});


