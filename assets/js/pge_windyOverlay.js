windyOptions = {
	// Required: API key
	key: '3vdBtb72vicQnIeP7pu7xbe0jrTALSPL',
	// Optional: Initial state of the map
	lat: 0,
	lon: 0,
	zoom: 2,
};

// Initialize Windy API
windyInit( windyOptions, windyAPI => {
	
	const { map } = windyAPI;
	// map is instance of Leaflet mapWindy
	map.options.minZoom = 0;
	map.options.maxZoom = 42;
	map.setView( [windyOptions['lat'],windyOptions['lon']], windyOptions['zoom']); 
	
	$("body").on('pgeMapMoving', function(event, lat, lng, zoom) {
		map.setView( [lat, lng], zoom);
	});
	
	//if (isWindyMapClickable) {
		map.on("dragend zoomend", function (e) {
			$("body").trigger("windyMapMoving", [ map.getCenter().lat, map.getCenter().lng, map.getZoom() ]);
			console.log('windy moving to : '+ map.getCenter().lat, map.getCenter().lng, map.getZoom());
		});
	//}
	
});
