/*********** Document Ready !!!  ***/

$(document).ready(function () {
		
	/************************  splash modal ********** */
	//if cookie hasn't been set...
	if (document.cookie.indexOf("splashModalShown=true")<0) {
		$("#splashModal").modal("show");
		// Modal has been shown, now set a cookie so it never comes back if users clicks 'ok...'
		$("#splashModalClose").click(function () {
			document.cookie = "splashModalShown=true; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
			$("#splashModal").modal("hide");
		});
	}
	
	/************************* if "centerMap" <-  we got a latlng from GET variables in url ***********/
	if (centerMap) {
		map.setView( centerLatLng, 15);
		centerLatLng =[0, 0];
		centerMap = false;
	}
	
	/********************  mapBounds stored in a cookie called "mapBounds" on login  *****************/
	$('#btn-login').on('click', function(){
		var bounds = roundNumber(map.getBounds().getNorthWest().lat, 5)+","+roundNumber(map.getBounds().getNorthWest().lng, 5)+","+roundNumber(map.getBounds().getSouthEast().lat, 5)+","+roundNumber(map.getBounds().getSouthEast().lng, 5);
		document.cookie = "mapBounds="+bounds+"; expires=Mon, 12 Dec 2112 12:12:12 GMT; path=/";
	});

	/******************** if mapBounds cookie exists, fitBounds and destroy cookie**********/
	var ca = document.cookie.split(';');   // explode cookies
	for(var i=0;i < ca.length;i++) {       // for all cookies...  
		var c = ca[i];					   // current cookie
		while (c.charAt(0)==' ') c = c.substring(1,c.length); 	// removing starting spaces
		if (c.indexOf("mapBounds=") == 0) {		// mapBounds cookie found
			var coords = c.substring("mapBounds=".length,c.length).split(",");
			map.fitBounds([[coords[0], coords[1]], [coords[2], coords[3]]]);
			document.cookie = "mapBounds=toto; expires=Wed, 12 Dec 2012 12:12:12 GMT; path=/";  // destroy cookie
		} 
	}
	
	/*****************  if Logged in and details empty and memberLoggedIn says "firstTime"  : open login modal  ***********************/

});

/*****************  load form in 'about' modal, in 'contact tab
 * 					was made here because it had to be called after jquery...    */
$('#contactModal').on('shown.bs.modal', function(){
	// console.log('contact modal shown');
	$( "#contact" ).load("assets/captcha/example_form.ajax.php", {async:true}, function() {
		// console.log( "Form loaded" );
		// alert('form loaded');
	});   
});

/**** ajax load of filters modal ****/
$('#filtersModal').on('shown.bs.modal', function(){
	// console.log('hip');
	$( "#filtersModalContent" ).load( "includes/filtersModal.php", function() {
		// console.log( "filters content loaded" );
	});   
});


function reloadCaptcha() {
	$('#siimage').prop('src', './securimage_show.php?sid=' + Math.random());
}
    
function processContactForm()
{
	$.ajax({
		url: 'assets/captcha/mailer.php',
		type: 'POST',
		data: jQuery('#contact_form').serialize(),
		dataType: 'json',
	}).done(function(data) {
		if (data.error === 0) {
			$('#success_message').show();
			$('#contact_form')[0].reset();
			reloadCaptcha();
			setTimeout("jQuery('#success_message').fadeOut()", 12000);
		} else {
			alert("There was an error with your submission.\n\n" + data.message);
			if (data.message.indexOf('Incorrect security code') >= 0) {
				$('#captcha_code').val('');
			}
		}
	});
	return false;
}

/**************** make text urls clickable links in modals ************* */
function textToLink( str ) {
	if (str) {
		var regex = /(https?:\/\/([-\w\.]+)+(:\d+)?(\/([\w\/_\-\.]*(\?\S+)?)?)?)/ig   // Set the regex string
	//	var regex = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/

		var replaced_text = str.replace(regex, "<a href='$1' target='_blank'>$1</a>");  // Replace plain text links by hyperlinks
		var replaced_text = replaced_text.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br />' + '$2');  // nl2br in javascript ;)
	//	console.log ('here again');
		return replaced_text;
	} else {
		return "";
	}
	
}

/**************** add slashes to double quotes "  ***********/
function addslashes( str ) {
	return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}

/***************  draws a country flag in a div/span with class "countryFlag" and attr "iso" ***********/
$(document).bind("ajaxSuccess", function() {
//	console.log('ajaxComplete!');
	$('.countryFlag').each( function() {
		//console.log('found countryFlag'+ $(this).attr('iso') );
		if ($(this).attr('iso') != "") {
			$(this).attr( 'src', "assets/img/flag/"+ $(this).attr('iso') +".png");	
			$(this).attr( 'title', getCountryName( $(this).attr('iso').toUpperCase() ));
		} else {
			$(this).hide();
		}
	});    
});

/*************** switch from modal to another  ***********/
var openModal='';     // should be called openedModal (typo), has the id of the currently opened modal on the page

$('.modal').on('click', '.openAnotherModal', function( ) {
	
		modalToOpen = '#'+$(this).attr('modalToOpen');
		// console.log( 'open '+modalToOpen+' modal 1 ?' );
		
		//  we get what we need from the link attributes as $(this) will not refer to it  in the next function... :( 
		//  what we need are the parameters for the next modal to open
		if (modalToOpen == '#memberModal' )			userName = $(this).attr('member');
		if (modalToOpen == '#siteDetailsModal' )	siteId =  $(this).attr('id');
		if (modalToOpen == '#featureModal' )		{ featureId =  $(this).attr('id'); feature =  $(this).attr('feature') };
		if (modalToOpen == '#reportItemModal' )		{ itemId =  $(this).attr('itemId'); itemType =  $(this).attr('itemType') ; itemName =  $(this).attr('itemName') };

		
		// console.log('here : '+openModal);
		
		$(".modal:visible").one('hidden.bs.modal', function(){
			
		
		// console.log('ther');
		
		map.closePopup(); // show icons on map when leaving an open site modal (and any modal..)
								// we should also close open site popup
		
		// console.log( 'open '+modalToOpen+' modal once ?' );

			if (modalToOpen == '#memberModal' )							{	$(modalToOpen).modal('show'); 	openModal = 'memberModal'; }
			else if (modalToOpen == '#loginModal' )						{	$(modalToOpen).modal('show'); 	openModal = 'loginModal'; }
			else if (modalToOpen == '#siteDetailsModal' )				{	siteModal(siteId); }
			else if (modalToOpen == '#featureModal' && feature == 'club'){	clubModal(featureId); }
			else if (modalToOpen == '#featureModal' && feature == 'pro' ){	proModal(featureId); }
			else if (modalToOpen == '#reportItemModal')					{ 
				$(modalToOpen).modal('show'); 
				openModal='reportItemModal';
				$("#reportItemForm").load("assets/ajax/reportItemForm.php", { "itemId": itemId, "itemName": itemName, "itemType": itemType});
			} 
			else {
				$(modalToOpen).modal('show'); 
			}
		// console.log( modalToOpen+' modal opened ?' );
			
		}).modal('hide');
});

/***********************   Open a Modal (called from GET variables handling *************/
function openAModal(modalName) {
	$(".modal:visible").modal('toggle');
	$("#"+modalName+"Modal").modal('show');
	openModal = modalName+'Modal';
//console.log('toto');
}

/*************** toggle fiances div function : called in the about modal ****************/
function toggleFinances(){
	$("#finances").toggle(600);
}

	
/*************** teleport function : called by the teleport modal form ******************/
function teleportTo(latlng){
	var coords = latlng.split(",");
	map.setView( [ coords[0], coords[1] ] , 15);
	$("#teleportModal").modal("hide");
	var popup = L.popup()
		.setLatLng([ coords[0], coords[1] ] )
		.setContent('latLng: '+coords[0]+','+coords[1])
		.openOn(map);
}

/**************** close bottom navbar *****************************************************/
$("#closeBottomNavbar").click(function(){
	$(".navbar-fixed-bottom ").toggle("slow");
});

/*************** export to gpx : called by the export  modal link ************************/
function exportGPX(style){
	window.open("http://paragliding.earth/export/getBoundingBoxSites.php/?limit=99&style="+style+"&north="+map.getBounds().getNorthWest().lat+"&south="+map.getBounds().getSouthEast().lat+"&east="+map.getBounds().getSouthEast().lng+"&west="+map.getBounds().getNorthWest().lng, '_blank');
}

/**********************  Countries <-> Iso codes table to get country name from iso without querying th db*/
var isoCountries = {
    'AF' : 'Afghanistan',
    'AX' : 'Aland Islands',
    'AL' : 'Albania',
    'DZ' : 'Algeria',
    'AS' : 'American Samoa',
    'AD' : 'Andorra',
    'AO' : 'Angola',
    'AI' : 'Anguilla',
    'AQ' : 'Antarctica',
    'AG' : 'Antigua And Barbuda',
    'AR' : 'Argentina',
    'AM' : 'Armenia',
    'AW' : 'Aruba',
    'AU' : 'Australia',
    'AT' : 'Austria',
    'AZ' : 'Azerbaijan',
    'BS' : 'Bahamas',
    'BH' : 'Bahrain',
    'BD' : 'Bangladesh',
    'BB' : 'Barbados',
    'BY' : 'Belarus',
    'BE' : 'Belgium',
    'BZ' : 'Belize',
    'BJ' : 'Benin',
    'BM' : 'Bermuda',
    'BT' : 'Bhutan',
    'BO' : 'Bolivia',
    'BA' : 'Bosnia And Herzegovina',
    'BW' : 'Botswana',
    'BV' : 'Bouvet Island',
    'BR' : 'Brazil',
    'IO' : 'British Indian Ocean Territory',
    'BN' : 'Brunei Darussalam',
    'BG' : 'Bulgaria',
    'BF' : 'Burkina Faso',
    'BI' : 'Burundi',
    'KH' : 'Cambodia',
    'CM' : 'Cameroon',
    'CA' : 'Canada',
    'CV' : 'Cape Verde',
    'KY' : 'Cayman Islands',
    'CF' : 'Central African Republic',
    'TD' : 'Chad',
    'CL' : 'Chile',
    'CN' : 'China',
    'CX' : 'Christmas Island',
    'CC' : 'Cocos (Keeling) Islands',
    'CO' : 'Colombia',
    'KM' : 'Comoros',
    'CG' : 'Congo',
    'CD' : 'Congo, Democratic Republic',
    'CK' : 'Cook Islands',
    'CR' : 'Costa Rica',
    'CI' : 'Cote D\'Ivoire',
    'HR' : 'Croatia',
    'CU' : 'Cuba',
    'CY' : 'Cyprus',
    'CZ' : 'Czech Republic',
    'DK' : 'Denmark',
    'DJ' : 'Djibouti',
    'DM' : 'Dominica',
    'DO' : 'Dominican Republic',
    'EC' : 'Ecuador',
    'EG' : 'Egypt',
    'SV' : 'El Salvador',
    'GQ' : 'Equatorial Guinea',
    'ER' : 'Eritrea',
    'EE' : 'Estonia',
    'ET' : 'Ethiopia',
    'FK' : 'Falkland Islands (Malvinas)',
    'FO' : 'Faroe Islands',
    'FJ' : 'Fiji',
    'FI' : 'Finland',
    'FR' : 'France',
    'GF' : 'French Guiana',
    'PF' : 'French Polynesia',
    'TF' : 'French Southern Territories',
    'GA' : 'Gabon',
    'GM' : 'Gambia',
    'GE' : 'Georgia',
    'DE' : 'Germany',
    'GH' : 'Ghana',
    'GI' : 'Gibraltar',
    'GR' : 'Greece',
    'GL' : 'Greenland',
    'GD' : 'Grenada',
    'GP' : 'Guadeloupe',
    'GU' : 'Guam',
    'GT' : 'Guatemala',
    'GG' : 'Guernsey',
    'GN' : 'Guinea',
    'GW' : 'Guinea-Bissau',
    'GY' : 'Guyana',
    'HT' : 'Haiti',
    'HM' : 'Heard Island & Mcdonald Islands',
    'VA' : 'Holy See (Vatican City State)',
    'HN' : 'Honduras',
    'HK' : 'Hong Kong',
    'HU' : 'Hungary',
    'IS' : 'Iceland',
    'IN' : 'India',
    'ID' : 'Indonesia',
    'IR' : 'Iran, Islamic Republic Of',
    'IQ' : 'Iraq',
    'IE' : 'Ireland',
    'IM' : 'Isle Of Man',
    'IL' : 'Israel',
    'IT' : 'Italy',
    'JM' : 'Jamaica',
    'JP' : 'Japan',
    'JE' : 'Jersey',
    'JO' : 'Jordan',
    'KZ' : 'Kazakhstan',
    'KE' : 'Kenya',
    'KI' : 'Kiribati',
    'KR' : 'Korea',
    'KW' : 'Kuwait',
    'KG' : 'Kyrgyzstan',
    'LA' : 'Lao People\'s Democratic Republic',
    'LV' : 'Latvia',
    'LB' : 'Lebanon',
    'LS' : 'Lesotho',
    'LR' : 'Liberia',
    'LY' : 'Libyan Arab Jamahiriya',
    'LI' : 'Liechtenstein',
    'LT' : 'Lithuania',
    'LU' : 'Luxembourg',
    'MO' : 'Macao',
    'MK' : 'Macedonia',
    'MG' : 'Madagascar',
    'MW' : 'Malawi',
    'MY' : 'Malaysia',
    'MV' : 'Maldives',
    'ML' : 'Mali',
    'MT' : 'Malta',
    'MH' : 'Marshall Islands',
    'MQ' : 'Martinique',
    'MR' : 'Mauritania',
    'MU' : 'Mauritius',
    'YT' : 'Mayotte',
    'MX' : 'Mexico',
    'FM' : 'Micronesia, Federated States Of',
    'MD' : 'Moldova',
    'MC' : 'Monaco',
    'MN' : 'Mongolia',
    'ME' : 'Montenegro',
    'MS' : 'Montserrat',
    'MA' : 'Morocco',
    'MZ' : 'Mozambique',
    'MM' : 'Myanmar',
    'NA' : 'Namibia',
    'NR' : 'Nauru',
    'NP' : 'Nepal',
    'NL' : 'Netherlands',
    'AN' : 'Netherlands Antilles',
    'NC' : 'New Caledonia',
    'NZ' : 'New Zealand',
    'NI' : 'Nicaragua',
    'NE' : 'Niger',
    'NG' : 'Nigeria',
    'NU' : 'Niue',
    'NF' : 'Norfolk Island',
    'MP' : 'Northern Mariana Islands',
    'NO' : 'Norway',
    'OM' : 'Oman',
    'PK' : 'Pakistan',
    'PW' : 'Palau',
    'PS' : 'Palestinian Territory, Occupied',
    'PA' : 'Panama',
    'PG' : 'Papua New Guinea',
    'PY' : 'Paraguay',
    'PE' : 'Peru',
    'PH' : 'Philippines',
    'PN' : 'Pitcairn',
    'PL' : 'Poland',
    'PT' : 'Portugal',
    'PR' : 'Puerto Rico',
    'QA' : 'Qatar',
    'RE' : 'Reunion',
    'RO' : 'Romania',
    'RU' : 'Russian Federation',
    'RW' : 'Rwanda',
    'BL' : 'Saint Barthelemy',
    'SH' : 'Saint Helena',
    'KN' : 'Saint Kitts And Nevis',
    'LC' : 'Saint Lucia',
    'MF' : 'Saint Martin',
    'PM' : 'Saint Pierre And Miquelon',
    'VC' : 'Saint Vincent And Grenadines',
    'WS' : 'Samoa',
    'SM' : 'San Marino',
    'ST' : 'Sao Tome And Principe',
    'SA' : 'Saudi Arabia',
    'SN' : 'Senegal',
    'RS' : 'Serbia',
    'SC' : 'Seychelles',
    'SL' : 'Sierra Leone',
    'SG' : 'Singapore',
    'SK' : 'Slovakia',
    'SI' : 'Slovenia',
    'SB' : 'Solomon Islands',
    'SO' : 'Somalia',
    'ZA' : 'South Africa',
    'GS' : 'South Georgia And Sandwich Isl.',
    'ES' : 'Spain',
    'LK' : 'Sri Lanka',
    'SD' : 'Sudan',
    'SR' : 'Suriname',
    'SJ' : 'Svalbard And Jan Mayen',
    'SZ' : 'Swaziland',
    'SE' : 'Sweden',
    'CH' : 'Switzerland',
    'SY' : 'Syrian Arab Republic',
    'TW' : 'Taiwan',
    'TJ' : 'Tajikistan',
    'TZ' : 'Tanzania',
    'TH' : 'Thailand',
    'TL' : 'Timor-Leste',
    'TG' : 'Togo',
    'TK' : 'Tokelau',
    'TO' : 'Tonga',
    'TT' : 'Trinidad And Tobago',
    'TN' : 'Tunisia',
    'TR' : 'Turkey',
    'TM' : 'Turkmenistan',
    'TC' : 'Turks And Caicos Islands',
    'TV' : 'Tuvalu',
    'UG' : 'Uganda',
    'UA' : 'Ukraine',
    'AE' : 'United Arab Emirates',
    'GB' : 'United Kingdom',
    'US' : 'United States',
    'UM' : 'United States Outlying Islands',
    'UY' : 'Uruguay',
    'UZ' : 'Uzbekistan',
    'VU' : 'Vanuatu',
    'VE' : 'Venezuela',
    'VN' : 'Viet Nam',
    'VG' : 'Virgin Islands, British',
    'VI' : 'Virgin Islands, U.S.',
    'WF' : 'Wallis And Futuna',
    'EH' : 'Western Sahara',
    'YE' : 'Yemen',
    'ZM' : 'Zambia',
    'ZW' : 'Zimbabwe'
};

function getCountryName (countryCode) {
    if (isoCountries.hasOwnProperty(countryCode)) {
        return isoCountries[countryCode];
    } else {
        return countryCode;
    }
}
