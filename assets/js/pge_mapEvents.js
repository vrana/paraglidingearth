/* Layer control listeners that allow for a single markerClusters layer */
map.on("overlayadd", function(e) {
  if (e.layer === siteLayer /* && zoomEnd >= displayItemsZoom*/) {
    //updateSites(); 
    markerClusters.addLayer(sites);
    showSitesLayer = true;
    syncSidebar('sites');
  }
  if (e.layer === proLayer) {
	updatePros();   
    markerClusters.addLayer(pros);
    showProsLayer = true;
    syncSidebar('pros');
  }
  if (e.layer === clubLayer) {
	updateClubs(); 
    markerClusters.addLayer(clubs);
    showClubsLayer = true;
    syncSidebar('clubs');
  }
});

map.on("overlayremove", function(e) {
  if (e.layer === siteLayer) {
    markerClusters.removeLayer(sites);
    showSitesLayer = false;
    $("#sitesCount").html(" Sites");
    syncSidebar('sites');
  }
  if (e.layer === proLayer) {
    markerClusters.removeLayer(pros);
    showProsLayer = false;
    $("#prosCount").html(" Pros");
    syncSidebar('pros');
  }
  if (e.layer === clubLayer) {
    markerClusters.removeLayer(clubs);
    showClubsLayer = false;
    $("#clubsCount").html(" Clubs");
    syncSidebar('clubs');
  }
});


/* Move end :
 *  updateItems on map
 * 	Filter  sidebar feature list to only show features in current map bounds */

map.on("moveend", function (e) {

    console.log('moveend !');
    
    $('#loading').css("display", "block");
		
	if (showSitesLayer) { updateSites(); }
	if (showProsLayer ) { updatePros();  }
	if (showClubsLayer) { updateClubs(); }
  //  $(".siteIcon").css( "opacity" , siteIconOpacity);
  //  $(".siteLabel").css( "opacity" , siteIconOpacity);
	syncSidebar('all');

    if (hasFilters) {
      sites.eachLayer(function (layer) {
        if (layer.feature.properties.filters < 1) {
          layer.setOpacity(0.3, true);
        }
      });
     // map.removeLayer(heat);
    }
    
	$("body").trigger("pgeMapMoving", [ map.getCenter().lat, map.getCenter().lng, map.getZoom() ]);
   // if (showSitesLayer && hasFilters) { filterSites(); }
});


map.on("zoomstart", function (e) {
	zoomStart = map.getZoom();
});

map.on("zoomend", function (e) {

	zoomEnd = map.getZoom();
	if ( zoomEnd > displayItemsZoom-2 && map.hasLayer(heat) ) {
		map.removeLayer(heat);
	}
	if ( zoomEnd <= displayItemsZoom-2 && !map.hasLayer(heat) && ! showWindyOverlay ) {
		heat.addTo(map);
	}
	
	if (zoomEnd > 8) {
		$(".toggleFlyable").toggleClass("disabled", false);
		$("#flyableControlContentZoom").css("display","none");
	} else {
		$(".toggleFlyable").toggleClass("disabled", true);
		$("#flyableControlContentZoom").css("display","block");
		$("#flyableControlContentOffset").css("display","none");
		
		if (showFlyable) {
			myToggleFlyable.toggle(false, false, false); // (state, noAnimate, noEvent) 
		}
		showFlyable = false;
	}
	//$("body").trigger("pgeMapMoving", [ map.getCenter().lat, map.getCenter().lng, map.getZoom() ]);
});



/* Clear feature highlight when map is clicked */
map.on("click", function(e) {
  highlight.clearLayers();
  //console.log('map clicked');
  siteItemsLayer.clearLayers();
  // siteIconOpacity = 1;
  // $(".siteIcon").css( "opacity" , siteIconOpacity);
  // $(".siteLabel").css( "opacity" , siteIconOpacity);
});


map.on('popupclose', function(e) {
  $(".-1siteIcon").css( "opacity" , 0.3);
  $(".0siteIcon").css( "opacity" , 0.3);
  $(".1siteIcon").css( "opacity" , 1);
  $(".-1siteLabel").css( "opacity" , 0.3);
  $(".0siteLabel").css( "opacity" , 0.3);
  $(".1siteLabel").css( "opacity" , 1);
});
map.on('popupopen', function(e) {
  $(".siteIcon").css( "opacity" , 0.1);
  $(".siteLabel").css( "opacity" , 0.1);
});

/*
if (showWindyOverlay) {
	$("body").trigger("pgeMapMoving", [ map.getCenter().lat, map.getCenter().lng, map.getZoom() ]);
//}
map.on('move', function(e) {
//	if (showWindyOverlay) {
		$("body").trigger("pgeMapMoving", [ map.getCenter().lat, map.getCenter().lng, map.getZoom() ]);
//	}

})
*/
