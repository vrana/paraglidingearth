$(document).ready(function(){
	
	/**     when get variable in url ******/
	if (showMember){
		$("#memberModal").modal('show');	
		openModal = 'memberModal';

	}

});


/********  ajax modal load call ******/
$("#memberModal").on("shown.bs.modal", function () {

	$.post("assets/ajax/memberModalDetails.php",{"userName": userName} ,  function (data){
		$("#memberModalDetails").html(data);
	});

	$.post("assets/ajax/memberModalActivity.php",{"userName": userName} ,  function (data){
		//data = countryFlag(data);
		$("#memberModalActivity").html(data);
	});
	
	$.post("assets/ajax/memberModalPictures.php",{"userName": userName} ,  function (data){
		$("#memberModalPictures").html(data);
	});
	
	$.post("assets/ajax/memberModalContact.php",{"userName": userName} ,  function (data){
		$("#memberModalContact").html(data);
	});
	
});
