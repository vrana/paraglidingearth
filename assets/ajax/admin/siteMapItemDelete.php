<?php
include "_isAdmin.php";

$id = $_POST['id'];
$type = str_replace(" ", "_", $_POST['type']);
$reportId = $_POST['reportId'];

if (stripos($type, "alternate") !== FALSE) $alternate = true;
else $alternate = false;

/****** delete DB entry **********/
if ( $alternate ) {										// if is an alternate item : erase db entry
	$qDelete = "DELETE FROM site_extra_items WHERE id = ".$id;
} else {															// if is a built-in item : edit site entry
	if ( $type == "landing") $description_field = "";  // landing doesn't have a "landing_description" field
	else $description_field = ", ".$type."_description = '' ";
	$qDelete = "UPDATE site 
				SET ".$type."_lat = 0, ".$type."_lng = 0, ".$type."_altitude = 0, ".$type."_name = '' ".$description_field."
				WHERE id = ".$id;
}

if ($rDelete = mysqli_query($bdd, $qDelete)) echo 'db entry deleted';
else echo "db entry not deleted : ".$qDelete;



// update entry in "reported_items" table
if (mysqli_query($bdd, "update reported_items set done = '1' where id = ".$reportId) ) 
	echo " - reported_items entry ok";
?>
