    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
						<div  id="returnmessage"></div>
                <form class="form-horizontal" id="formDelete">
                    <fieldset>
						
						
						<input id="idRequestDelete" name="id" type="hidden" placeholder="Id" class="form-control" value="<?php echo $_POST['id'];?>" >
						<input id="siteNameRequestDelete" name="sitename" type="hidden" placeholder="Id" class="form-control" value="<?php echo $_POST['sitename'];?>" >
 
                        <div class="form-group">
                            <span class="col-md-1 text-center"><i class="fa fa-user bigicon"></i> *</span>
                            <div class="col-md-12">
                                <input id="nameRequestDelete" name="name" type="text" placeholder="* Name" class="form-control">
                            </div>
                        </div>
 
                        <div class="form-group">
                            <span class="col-md-1 text-center"><i class="fa fa-envelope-o bigicon"></i> *</span>
                            <div class="col-md-12">
                                <input id="emailRequestDelete" name="email" type="text" placeholder="* Email Address" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 text-center"><i class="fa fa-pencil-square-o bigicon"></i> *</span>
                            <div class="col-md-12">
                                <textarea class="form-control" id="messageRequestDelete" name="message" placeholder="* Please let us know why this site should be erased." rows="7"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <input type="button" id="submitDelete"  class="btn btn-primary btn-lg" value="Send"/>
                               <input type="button" id="cancelDelete"  class="btn" value="Cancel"/>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>

<script>
	
$(document).ready(function() {
	$("#cancelDelete").click(function() {
		$("#formDelete")[0].reset(); 
		$("#deleteSiteForm").toggle("slow");
	});
	$("#submitDelete").click(function() {
		var name = $("#nameRequestDelete").val();
		var email = $("#emailRequestDelete").val();
		var message = $("#messageRequestDelete").val();
		var id = $("#idRequestDelete").val();
		var sitename = $("#siteNameRequestDelete").val();
		
		$("#returnmessage").empty(); // To empty previous error/success message.
		
		// Checking for blank fields.
		if (name == '' || email == '' || message == '') {
			alert("Please Fill Required Fields");
		} else {
			// Returns successful data submission message when the entered information is stored in database.
			$.post("assets/ajax/deleteSiteRequestMail.php", {
				name1: name,
				email1: email,
				message1: message,
				sitename: sitename,
				id: id,
				}, function(data) {
						$("#returnmessage").append(data); // Append returned message to message paragraph.
						if (data == "Request was sent, thx for your participation.") {
							$("#formDelete")[0].reset(); // To reset form fields on success.
					        $("#formDelete").hide();
						}
					});
		}
	});
});
</script>

