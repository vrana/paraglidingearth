<div class="alert-warning">
  
	<a href="#" class="pop" data-trigger="hover" data-toggle="popover"  data-html="true" data-content="This vote is totally subjective, the idea is to help people who don't know nothing about the area sites to make choices between the differents places around.">
	Rank this site (?)</a>


	<div class="radio">
	  <label>
		<input type="radio" name="vote" id="vote0" value="0">
		<span class="pop" data-trigger="hover" data-toggle="popover"  data-html="true" data-content="rather uninteresting, are you sure you can't go to a better site today?">
			<img src="assets/img/ranking/0.png" /> 0 (?)
		</span>
	  </label>
	</div>	
	
	<div class="radio">
	  <label>
		<input type="radio" name="vote" id="vote1" value="1">
		<span class="pop" data-trigger="hover" data-toggle="popover"  data-html="true" data-content="well, you can obviously fly here, but you won't keep an eternal souvenir of it">
			<img src="assets/img/ranking/1.png" /> 1 (?)
		</span>
	  </label>
	</div>	
	
	<div class="radio">
	  <label>
		<input type="radio" name="vote" id="vote2" value="2">
		<span class="pop" data-trigger="hover" data-toggle="popover"  data-html="true" data-content="rather cool site, pleasant but not fantastic">
			<img src="assets/img/ranking/2.png" /> 2 (?)
		</span>
	  </label>
	</div>	
	
	<div class="radio">
	  <label>
		<input type="radio" name="vote" id="vote3" value="3">
		<span class="pop" data-trigger="hover" data-toggle="popover"  data-html="true" data-content="this site is really okay, you will enjoy flying here">
			<img src="assets/img/ranking/3.png" /> 3 (?)
		</span>
	  </label>
	</div>	
	
	<div class="radio">
	  <label>
		<input type="radio" name="vote" id="vote4" value="4">
		<span class="pop" data-trigger="hover" data-toggle="popover"  data-html="true" data-content="very cool site, it has to be visited, you won't regret">
			<img src="assets/img/ranking/4.png" /> 4 (?)
		</span>
	  </label>
	</div>	

	<div class="radio">
	  <label>
		<input type="radio" name="vote" id="vote5" value="5">
		<span class="pop" data-trigger="hover" data-toggle="popover"  data-html="true" data-content="exceptionnal, unique, absolutely terrific : it would be a sin not to come fly here, you'll keep it in mind it for years">
			<img src="assets/img/ranking/5.png" /> 5 (?)
		</span>
	  </label>
	</div>	
	
	<input type="hidden" name="site" value="<?=$_REQUEST['id']?>">
	
	<button id="submitRanking" class="btn btn-primary btn-sm">submit</button>
	<button id="cancelRanking" class="btn btn-secondary btn-sm">cancel</button>

</div>



<script>
	$('[data-toggle="popover"]').popover();
 
 
	$("#cancelRanking").on("click", function(){
			$("#rankingForm").toggle("slow");
		});
	
	$("#submitRanking").on("click", function(){

		if ($('input[type=radio][name=vote]:checked').length == 1) {
			// console.log($('input[type=radio][name=vote]:checked').attr('value'));
			$.post("assets/ajax/updateItem/siteRankingSave.php",{"id":<?=$_REQUEST['id']?> ,"vote": $('input[type=radio][name=vote]:checked').attr('value')}, function(data){
				//console.log(data.ranking);
				console.log(data.number_of_votes);
				$("#rankingImg").attr({"src": "assets/img/ranking/"+data.ranking_roundedHalf+".png", "title": data.ranking +"/5, based on "+data.number_of_votes+" votes"});
				$("#rankingForm").toggle("slow");
			});	
		} else {
			console.log('select a value or cancel');
		}
	});	

 </script>
