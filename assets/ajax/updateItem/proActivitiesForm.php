<form id="activitiesForm" action="javascript:void(0);">
	<input type="hidden" id="idForm" name="idForm" value="<?php echo $_POST['id'];?>" />
	 <div class="checkbox">
		<label><input type="checkbox" id="tandem" name="activity[]" value="tandem" <?php if($_POST['tandem']==1) echo "checked";?> /> <img src="assets/img/icons/tandem.png" /> tandem first flights</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" id="school" name="activity[]" value="school" <?php if($_POST['school']==1) echo "checked";?> /> <img src="assets/img/icons/school.png" /> free flight school</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" id="shop" name="activity[]" value="shop" <?php if($_POST['shop']==1) echo "checked";?> /> <img src="assets/img/icons/shop.png" /> gear shop</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" id="repair" name="activity[]" value="repair" <?php if($_POST['repair']==1) echo "checked";?> /> <img src="assets/img/icons/repair.png" /> test and repair</label>
	</div>
	<div>
		<button id="submitActivitiesForm"  class="btn btn-primary btn-sm">submit</button>
		<button id="cancelActivitiesForm"  class="btn btn-secondary btn-sm">cancel</button>
	</div>
</form>

<script>
	
	$("#cancelActivitiesForm").on("click", function(){
		$("#activitiesForm").toggle("slow");
		$("#activitiesDiv").toggle("slow");
	});
	
	$("#submitActivitiesForm").on("click", function(){
		var listHtml = '';		
		var dataForm = {'id': $('#idForm').val(), 'tandem':0, 'school':0, 'shop':0, 'repair':0 };
		if( $('input[id=tandem]').is(':checked') ) {
			dataForm['tandem'] = 1;
			listHtml += '<li> <img src="assets/img/icons/tandem.png" /> tandem</li>';
		}
		if( $('input[id=school]').is(':checked') ) {
			dataForm['school'] = 1;
			listHtml += '<li> <img src="assets/img/icons/school.png" /> free flight school</li>';
		}
		if( $('input[id=shop]').is(':checked') ) {
			dataForm['shop'] = 1;
			listHtml += '<li> <img src="assets/img/icons/shop.png" /> gear shop</li>';
		}
		if( $('input[id=repair]').is(':checked') ) {
			dataForm['repair'] = 1;
			listHtml += '<li> <img src="assets/img/icons/repair.png" /> testing and/or repair</li>';
		}
		
	//	console.log (dataForm);
			
		$.post("assets/ajax/updateItem/proActivitiesSave.php", dataForm, function( data ) {
		//	console.log( "Data Loaded: " + data );
		});

		$("#activitiesDiv").html(listHtml);
		$("#activitiesForm").toggle("slow");
		$("#activitiesDiv").toggle("slow");
	});
	
</script>
