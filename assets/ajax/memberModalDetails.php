<?php
include '../../member/ASEngine/AS.php';

$db = app('db');
$result = $db->select(
    "SELECT * FROM `as_user_details` LEFT JOIN as_users ON as_user_details.user_id = as_users.user_id WHERE `username` = :username",
    array ("username" => $_POST['userName'])
);

$userDetails = $result[0];

$currentUserId = ASSession::get('user_id');

//print_r($userDetails);

?>

<legend>Profile</legend>

<p>
	First name: <?php echo ($userDetails['first_name']=="") ? "?" : $userDetails['first_name'];?>
</p>

<p>
	Last name: <?php echo ($userDetails['last_name']=="") ? "?" : $userDetails['last_name'];?>
</p>

<p>
	Country: <?php echo ($userDetails['country']=="") ? "?" : $userDetails['country'];?> <img class="countryFlag" iso="<?php echo $userDetails['country'];?>" />
</p>

<p>
	Fly since: <?php echo ($userDetails['fly_since']=="") ? "?" : $userDetails['fly_since'];?>
</p>



<?php if ($userDetails['user_id'] == $currentUserId) {
?>
<div class="alert-info" style="text-align:center; padding:7px"> 
<input type="button" class="btn btn-primary"  onclick="location.assign('member/profile.php');" value="Hey, that's me ! Edit my profile" />
</div>
<?php }
?>
