<!--
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <title>Securimage Example Form</title>
    <style type="text/css">
    <!--
        #success_message { border: 1px solid #000; width: 550px; text-align: left; padding: 10px 7px; background: #33ff33; color: #000; font-weight; bold; font-size: 1.2em; border-radius: 4px; -moz-border-radius: 4px; -webkit-border-radius: 4px; }
        fieldset { width: 90%; }
        legend { font-size: 24px; }
        .note { font-size: 18px; }
        form label { display: block; font-weight: bold; }
    ->
    </style>
    </head>
<body>
-->
<fieldset>
	<legend>Contact Form</legend>
	<!--
	<p class="note">
	  This is an example PHP form that processes user information, checks for errors, and validates the captcha code.<br />
	  This example form also demonstrates how to submit a form to itself to display error messages.
	</p>
	-->
	<div id="success_message" class="alert-info" style="display: none">
		<p>	Your message has been sent!<br />
			We will contact you back if needed as soon as possible.
		</p>
	</div>

	<form method="post" action="" id="contact_form" onsubmit="return processContactForm()">
	  <input type="hidden" name="do" value="contact" />
	  <div class="well well-sm">				  
		<div class="row">
		  <div class="col-md-4">
			<div class="form-group">
			  <label for="ct_name">Name:</label>
			  <input type="text" name="ct_name"   class="form-control" value="" />
			</div>
			<div class="form-group">
			  <label for="ct_email">Email:</label>
			  <input type="text" name="ct_email"  class="form-control" value="" />
			</div>
		  </div>
		  <div class="col-md-8">
			<div class="form-group">
			  <label for="ct_email">Subject:</label>
			  <input type="text" name="ct_subject"  class="form-control" value="" />
			</div>
			<label for="ct_message">Message:</label>
			<textarea name="ct_message" rows="5"  class="form-control"></textarea>
		 </div>
		</div>
		<div class="row">
		  <div class="col-md-12">
			<label for="captcha_code">Be a human, make the maths:</label><br />
			<?php require_once 'securimage.php'; echo Securimage::getCaptchaHtml(array('input_name' => 'ct_captcha')); ?>
		 </div>
		</div>
		<div class="row">
		  <div class="col-md-12">
			<input type="submit" value="Submit Message" class="btn btn-primary pull-right" />
		  </div>
		</div>
	  </div>
	</form>
</fieldset>

<script type="text/javascript">
</script>
<!--
</body>
</html>
-->
