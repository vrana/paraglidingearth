<?php
//session_start(); // this MUST be called prior to any output including whitespaces and line breaks!

$GLOBALS['ct_recipient']   = 'raphael@disroot.org'; // Change to your email address!
$GLOBALS['ct_msg_subject'] = 'PgEarth Contact';

$GLOBALS['DEBUG_MODE'] = 0;
// CHANGE TO 0 TO TURN OFF DEBUG MODE
// IN DEBUG MODE, ONLY THE CAPTCHA CODE IS VALIDATED, AND NO EMAIL IS SENT


// Process the form, if it was submitted
process_si_contact_form();

?>

<?php

// The form processor PHP code
function process_si_contact_form()
{
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && @$_POST['do'] == 'contact') {
        // if the form has been submitted

        foreach($_POST as $key => $value) {
            if (!is_array($key)) {
                // sanitize the input data
                if ($key != 'ct_message') $value = strip_tags($value);
                $_POST[$key] = htmlspecialchars(stripslashes(trim($value)));
            }
        }

        $nameCT    = @$_POST['ct_name'];    // name from the form
        $email   = @$_POST['ct_email'];   // email from the form
        $URL     = @$_POST['ct_URL'];     // url from the form
        $subject = @$_POST['ct_subject']; // the subject from the form
        $message = @$_POST['ct_message']; // the message from the form
        $captcha = @$_POST['ct_captcha']; // the user's entry for the captcha code
        $nameCT    = substr($nameCT, 0, 64);  // limit name to 64 characters
	//	$nameCT    = substr($nameCT, -1, 18);  // limit name to 64 characters

        $errors = array();  // initialize empty error array

        if (isset($GLOBALS['DEBUG_MODE']) && $GLOBALS['DEBUG_MODE'] == false) {
            // only check for errors if the form is not in debug mode

            if (strlen($nameCT) < 3) {
                // name too short, add error
                $errors['name_error'] = 'Your name is required';
            }

            if (strlen($email) == 0) {
                // no email address given
                $errors['email_error'] = 'Email address is required';
            } else if ( !preg_match('/^(?:[\w\d-]+\.?)+@(?:(?:[\w\d]\-?)+\.)+\w{2,4}$/i', $email)) {
                // invalid email format
                $errors['email_error'] = 'Email address entered is invalid';
            }

            if (strlen($message) < 20) {
                // message length too short
                $errors['message_error'] = 'Please enter a longer message';
            }
        }

        // Only try to validate the captcha if the form has no errors
        // This is especially important for ajax calls
        if (sizeof($errors) == 0) {
            require_once dirname(__FILE__) . '/securimage.php';
            $securimage = new Securimage();

            if ($securimage->check($captcha) == false) {
                $errors['captcha_error'] = 'Incorrect security code entered';
            }
        }

        if (sizeof($errors) == 0) {
            // no errors, send the form
            $time       = date('r');
            $GLOBALS['ct_msg_subject'] .= " : ".$subject;
            $message = "A message was submitted from the contact form.  The following information was provided.<br /><br />"
                     . "Name: $nameCT<br />"
                     . "Email: $email<br />"
                     . "URL: $URL<br />"
                     . "Message:<br />"
                     . "<pre>$message</pre>"
                     . "<br /><br />IP Address: {$_SERVER['REMOTE_ADDR']}<br />"
                     . "Time: $time<br />"
                     . "Browser: {$_SERVER['HTTP_USER_AGENT']}<br />";

            if (isset($GLOBALS['DEBUG_MODE']) && $GLOBALS['DEBUG_MODE'] == false) {
                // send the message with mail()
                mail(	$GLOBALS['ct_recipient'],
						$GLOBALS['ct_msg_subject'],
						$message,
						"From: {$nameCT}\r\n
						Reply-To: {$email}\r\n
						Content-type: text/html; charset=UTF-8\r\n
						MIME-Version: 1.0"
					);
            }

            $return = array('error' => 0, 'message' => 'OK');
            die(json_encode($return));
        } else {
            $errmsg = '';
            foreach($errors as $key => $error) {
                // set up error messages to display with each field
                $errmsg .= " - {$error}\n";
            }

            $return = array('error' => 1, 'message' => $errmsg);
            die(json_encode($return));
        }
    } // POST
} // function process_si_contact_form()
?>
